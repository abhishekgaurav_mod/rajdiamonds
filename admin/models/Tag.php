<?php
class Tag extends DBSource
{
  public function __construct()
  { parent::__construct();  }

    public function __destruct()
  { parent::__destruct(); }

  public function addTag($obj_tag)
  {
    try
    {
      $sql = 'INSERT INTO tags
          (
            tag_name
            , tag_description
            , tag_slug
            , tag_thumb
            , desktop_banner
            , tab_banner
            , mobile_banner
            , featured_status
            , created_on
            , updated_on
          )
          VALUES
          ( "'.$this->mysqlEscapeString($obj_tag->tag_name).'"
            , "'.$this->mysqlEscapeString($obj_tag->tag_description).'"
            , "'.$this->mysqlEscapeString($obj_tag->tag_slug).'"
            , "'.$this->mysqlEscapeString($obj_tag->tag_thumb).'"
            , "'.$this->mysqlEscapeString($obj_tag->desktop_banner).'"
            , "'.$this->mysqlEscapeString($obj_tag->tab_banner).'"
            , "'.$this->mysqlEscapeString($obj_tag->mobile_banner).'"
            , "'.$this->mysqlEscapeString($obj_tag->featured_status).'"
            , CURRENT_TIMESTAMP
            , CURRENT_TIMESTAMP);';
        $this->db_query($sql);
      return $this->mysqlInsertId();
      }
      catch(Exception $e)
      { throw $e; }
  }

  public function updateTag($obj_tag)
  {
    try
    {
        $sql  ="  UPDATE tags SET
              tag_name     = '".$this->mysqlEscapeString($obj_tag->tag_name)."'
              , tag_description = '".$this->mysqlEscapeString($obj_tag->tag_description)."'
              , tag_slug = '".$this->mysqlEscapeString($obj_tag->tag_slug)."'
              , tag_thumb =  '".$this->mysqlEscapeString($obj_tag->tag_thumb)."'
              , desktop_banner =  '".$this->mysqlEscapeString($obj_tag->desktop_banner)."'
              , tab_banner = '".$this->mysqlEscapeString($obj_tag->tab_banner)."'
              , mobile_banner = '".$this->mysqlEscapeString($obj_tag->mobile_banner)."'
              , featured_status = '".$this->mysqlEscapeString($obj_tag->featured_status)."'
            , updated_on    = CURRENT_TIMESTAMP
              WHERE tag_id = '".$this->mysqlEscapeString($obj_tag->tag_id)."'";
      return ($this->db_query($sql))?true:false;
    }
      catch(Exception $e)
      { throw $e; }
  }

  public function deleteTag ($tagId, $enabled)
  {
    try
    {
      $sql  =    "  UPDATE  tags
              SET   enabled   =  '".$this->mysqlEscapeString($enabled)."'
              WHERE   tag_id = '".$this->mysqlEscapeString($tagId)."'";
      return ($this->db_query($sql))?true:false;
    }
    catch(Exception $e)
    { throw $e; }
  }

    public function getTagfilesById($id)
    {
      try {
        $sql = "SELECT  tag_thumb, desktop_banner, tab_banner, mobile_banner 
            FROM tags
            WHERE tag_id = '".$this->mysqlEscapeString($id)."'";

        $res=$this->db_query($sql);
        if($this->db_num_rows($res)==0) {
          $this->db_free_results($res);
          return 0;
        } else {
          $row    =   $this->db_fetch_object($res);
          $this->db_free_results($res);
          return $row;
        }
      }
      catch(Exception $e)
      { throw $e; }
    }

    public function getTagById($id)
    {
      try
      {
        $sql = "SELECT  * FROM tags
            WHERE   tag_id = '".$this->mysqlEscapeString($id)."'";

        $res=$this->db_query($sql);
        if($this->db_num_rows($res)==0)
        {
          $this->db_free_results($res);
          return 0;
        }
        else
        {
          $row    =   $this->db_fetch_object($res);
          $this->db_free_results($res);
          return $row;
        }
      }
      catch(Exception $e)
      { throw $e; }
    }

    public function checkTagSlug($tag_slug) {
        try {
      $sql="SELECT tag_id , tag_name 
      FROM tags WHERE tag_slug = '".$this->mysqlEscapeString($tag_slug)."'";

      $res=$this->db_query($sql);
      if($this->db_num_rows($res)==0) {
        $this->db_free_results($res);
        return false;
      } else {
        $this->db_free_results($res);
        return true;
      }
        }catch (Exception $e){ $this->db_error($e); }
    }

    public function checkOtherSimilarTagSlug($tag_id, $tag_slug) {
        try {
      $sql="SELECT tag_id , tag_name 
      FROM tags WHERE tag_slug = '".$this->mysqlEscapeString($tag_slug)."' AND tag_id != '".$this->mysqlEscapeString($tag_id)."' ";

      $res=$this->db_query($sql);
      if($this->db_num_rows($res)==0) {
        $this->db_free_results($res);
        return false;
      } else {
        $this->db_free_results($res);
        return true;
      }
        }catch (Exception $e){ $this->db_error($e); }
    }

    public function getTagsWithfilter($query, $ids = null) {
      try
      {
        $sql =  sprintf("SELECT tag_id,  tag_name FROM tags WHERE  tag_name LIKE '%%%s%%'",$this->mysqlEscapeString($query));
        $sql.=  ($ids!==null)?'AND tag_id NOT IN('.$this->mysqlEscapeString($ids).')':'';
        $sql.=  ' ORDER BY tag_name ASC LIMIT 10';

        $res=$this->db_query($sql);
        if($this->db_num_rows($res)==0)
        {
          $this->db_free_results($res);
          return 0;
        }
        else
        {
          for($i=0;$row=$this->db_fetch_object($res);$i++)
          {
          $arr[$i] = array('id'=>$row->tag_id, 'name'=>$row->tag_name);
        }
        $this->db_free_results($res);
        return $arr;
        }
      }catch (Exception $e){ $this->db_error($e); }
    }
}
?>
