<?php
class Product extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addProduct($obj_product)
	{
		try
		{
			$sql = 'INSERT INTO products
					(
						product_name
						, category_id
						, product_code
						, bar_code
						, description
						, product_order
						, front_visible
						, product_slug
						, product_thumb
						, tags_string
						, created_on
						, updated_on
					)
					VALUES
					(	"'.$this->mysqlEscapeString($obj_product->product_name).'"
						, "'.$this->mysqlEscapeString($obj_product->category_id).'"
						, "'.$this->mysqlEscapeString($obj_product->product_code).'"
						, "'.$this->mysqlEscapeString($obj_product->bar_code).'"
						, "'.$this->mysqlEscapeString($obj_product->description).'"
						, "'.$this->mysqlEscapeString($obj_product->product_order).'"
						, "'.$this->mysqlEscapeString($obj_product->front_visible).'"
						, "'.$this->mysqlEscapeString($obj_product->product_slug).'"
						, "'.$this->mysqlEscapeString($obj_product->product_thumb).'"
						, "'.$this->mysqlEscapeString($obj_product->tags_string).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function updateProduct($obj_product)
	{
		try
		{
   			$sql	="	UPDATE products SET
   						product_name		=	'".$this->mysqlEscapeString($obj_product->product_name)."'
						, product_code		=	'".$this->mysqlEscapeString($obj_product->product_code)."'
						, bar_code			=	'".$this->mysqlEscapeString($obj_product->bar_code)."'
						, description		=	'".$this->mysqlEscapeString($obj_product->description)."'
						, product_order		=	'".$this->mysqlEscapeString($obj_product->product_order)."'
						, front_visible		=	'".$this->mysqlEscapeString($obj_product->front_visible)."'
						, product_slug		=	'".$this->mysqlEscapeString($obj_product->product_slug)."'
						, product_thumb		=	'".$this->mysqlEscapeString($obj_product->product_thumb)."'
						, tags_string		=	'".$this->mysqlEscapeString($obj_product->tags_string)."'
						, updated_on		=	CURRENT_TIMESTAMP
   						WHERE product_id	=	'".$this->mysqlEscapeString($obj_product->product_id)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function deleteProduct ($productId, $enabled)
	{
		try
		{
			$sql	=    "	UPDATE 	products
							SET 	enabled 	=  '".$this->mysqlEscapeString($enabled)."'
							WHERE 	product_id	=	'".$this->mysqlEscapeString($productId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}

    public function getProductfilesById($id)
    {
    	try
    	{
    		$sql = "SELECT	product_thumb 
    				FROM products
    				WHERE product_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }

    public function getProductTagStringById($id)
    {
    	try
    	{
    		$sql = "SELECT	tags_string 
    				FROM products
    				WHERE product_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }

    public function getProductById($id)
    {
    	try
    	{
    		$sql = "SELECT	r.category_name
    						, r.category_id
    						, r.category_order
    		                , c.product_name
    		                , c.product_code
    		                , c.bar_code
    		                , c.description
    		                , c.product_order
    		                , c.front_visible
    		                , c.product_slug
    		                , c.product_thumb
    		                , c.tags_string
    		                , c.product_id
    		                , c.created_on
    		                , c.updated_on
    		                , c.enabled
    				FROM categories AS r
    				LEFT JOIN products AS c ON c.category_id = r.category_id
    				WHERE c.product_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
    public function getProductsByCategoryId($categoryId)
	{
		try
		{
			$sql = 'SELECT 	c.product_id
							, c.category_id
							, c.product_name
							, c.product_code
							, c.bar_code
							, c.description
							, c.product_order
							, c.front_visible
							, c.product_slug
							, c.product_thumb
							, c.tags_string
					FROM products AS c
					WHERE c.category_id	=	"'.$this->mysqlEscapeString($categoryId).'"';

			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0)
			{
				$this->db_free_results($res);
				return 0;
			}
			else
			{
    			for($i=0;$row=$this->db_fetch_object($res);$i++)
    			{	$arr[$i] = $row;	}
    			$this->db_free_results($res);
    			return $arr;
			}
		}
		catch(Exception $e)
		{	throw $e;	}
	}

  	public function getProductTagMappingByProductId($product_id) {
		try {
			$sql = "SELECT ptm.tag_id, t.tag_name 
					FROM product_tag_mapping AS ptm
					INNER JOIN tags AS t ON ptm.tag_id = t.tag_id
					WHERE ptm.product_id = ".$this->mysqlEscapeString($product_id);

			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0) {
				$this->db_free_results($res);
				return 0;
			} else {
				$arr	= 	array();
				for($i=0;$row=$this->db_fetch_object($res);$i++)
				{	$arr[$i]	=	$row;	}
				$this->db_free_results($res);
				return $arr;
			}
		}catch (Exception $e){ $this->db_error($e); }
	}

	public function addProductTagMapping($obj_mapping) {
		try
		{
			$sql = 'INSERT INTO product_tag_mapping
					(
						product_id
						, tag_id
						, created_on
					)
					VALUES
					(	"'.$this->mysqlEscapeString($obj_mapping->product_id).'"
						, "'.$this->mysqlEscapeString($obj_mapping->tag_id).'"
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function deleteProductTagMappingByProductId($product_id) {
		try
		{
			$sql = 'DELETE FROM product_tag_mapping 
					WHERE product_id 	= 	'.$this->mysqlEscapeString($product_id);
			return ($this->db_query($sql))?true:false;
		}catch (Exception $e){ $this->db_error($e); }
	}

	public function getProductsAndCategories()
	{
		try
		{
			$sql = 'SELECT 	c.product_id
							, c.category_id
							, c.product_name
							, r.category_name
					FROM products AS c
					LEFT JOIN categories AS r ON r.category_id = c.category_id
					WHERE c.enabled	=	"Y"';

			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0)
			{
				$this->db_free_results($res);
				return 0;
			}
			else
			{
    			for($i=0;$row=$this->db_fetch_object($res);$i++)
    			{	$arr[$i] = $row;	}
    			$this->db_free_results($res);
    			return $arr;
			}
		}
		catch(Exception $e)
		{	throw $e;	}
	}
}
?>
