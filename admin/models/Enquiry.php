<?php 
class Enquiry extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}


	public function deleteEnquiry($enquiryId, $enabled)
	{
		try
		{
			$sql	=    "	UPDATE 	enquiries 
							SET 	enabled 	=  	'".$this->mysqlEscapeString($enabled)."' 
							WHERE 	enquiry_id	=	'".$this->mysqlEscapeString($enquiryId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}

	public function getEnquiryById($enquiryId)
    {
    	try
    	{
    		$sql = "SELECT 	enquiry_id, first_name, phone, organisation, interest, details, email, created_on, updated_on, enabled	
    				FROM enquiries
    				WHERE enquiry_id	= '".$this->mysqlEscapeString($enquiryId)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
}
?>
