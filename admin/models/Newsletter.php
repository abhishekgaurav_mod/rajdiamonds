<?php 
class Newsletter extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addNewsletter($object)
	{
		try
		{
			$sql = 'INSERT INTO newsletters
					(	
						subject
                        , imagepath
						, created_on
                        , updated_on
					)
					VALUES
					(	
						"'.$this->mysqlEscapeString($object->subject).'"
                        , "'.$this->mysqlEscapeString($object->imagepath).'"
                        , CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

    public function updateNewsletter ($object)
    {
        try
        {
            $sql    ="  UPDATE newsletters SET
                        subject         =   '".$this->mysqlEscapeString($object->subject)."',
                        imagepath       =   '".$this->mysqlEscapeString($object->imagepath)."',
                        updated_on  	=   CURRENT_TIMESTAMP
                        WHERE newsletter_id  =   '".$this->mysqlEscapeString($object->newsletter_id)."'";
            return ($this->db_query($sql))?true:false;
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function updateSendNewsletter ($object)
    {
        try
        {
            $sql    ="  UPDATE newsletters SET
                        send_date           =   '".$this->mysqlEscapeString($object->send_date)."',
                        subscriber_count    =   '".$this->mysqlEscapeString($object->subscriber_count)."',
                        send_count          =   '".$this->mysqlEscapeString($object->send_count)."',
                        updated_on          =   CURRENT_TIMESTAMP
                        WHERE newsletter_id =   '".$this->mysqlEscapeString($object->newsletter_id)."'";
            return ($this->db_query($sql))?true:false;
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getNewsletterfilesById($id)
    {
        try {
            $sql = "SELECT  imagepath 
                    FROM newsletters
                    WHERE newsletter_id    = '".$this->mysqlEscapeString($id)."'";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                $row    =   $this->db_fetch_object($res);
                $this->db_free_results($res);
                return $row;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }
	
	public function deleteNewsletter($newsletterId, $enabled)
	{
		try
		{
			$sql	=    "	UPDATE  newsletters
                            SET     enabled     =  '".$this->mysqlEscapeString($enabled)."'
                            WHERE   newsletter_id  =   '".$this->mysqlEscapeString($newsletterId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}
	
    public function getNewsletterById($Id)
    {
    	try
    	{
    		$sql = "SELECT	newsletter_id, subject, imagepath, created_on, updated_on, enabled 
    		 		FROM newsletters 
    				WHERE newsletter_id = '".$this->mysqlEscapeString($Id)."' ";
    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
	
}
?>
