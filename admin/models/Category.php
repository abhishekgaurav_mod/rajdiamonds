<?php
class Category extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addCategory($obj_category)
	{
		try
		{
			$sql = 'INSERT INTO categories
					(
						category_name
						, category_description
						, category_slug
						, category_thumb
						, desktop_banner
						, tab_banner
						, mobile_banner
						, featured_status
						, category_order
						, created_on
						, updated_on
					)
					VALUES
					(	"'.$this->mysqlEscapeString($obj_category->category_name).'"
						, "'.$this->mysqlEscapeString($obj_category->category_description).'"
						, "'.$this->mysqlEscapeString($obj_category->category_slug).'"
						, "'.$this->mysqlEscapeString($obj_category->category_thumb).'"
						, "'.$this->mysqlEscapeString($obj_category->desktop_banner).'"
						, "'.$this->mysqlEscapeString($obj_category->tab_banner).'"
						, "'.$this->mysqlEscapeString($obj_category->mobile_banner).'"
						, "'.$this->mysqlEscapeString($obj_category->featured_status).'"
						, "'.$this->mysqlEscapeString($obj_category->category_order).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function updateCategory($obj_category)
	{
		try
		{
   			$sql	="	UPDATE categories SET
   						category_name	    =	'".$this->mysqlEscapeString($obj_category->category_name)."'
   						, category_description =	'".$this->mysqlEscapeString($obj_category->category_description)."'
   						, category_slug =	'".$this->mysqlEscapeString($obj_category->category_slug)."'
   						, category_thumb =	'".$this->mysqlEscapeString($obj_category->category_thumb)."'
   						, desktop_banner =	'".$this->mysqlEscapeString($obj_category->desktop_banner)."'
   						, tab_banner =	'".$this->mysqlEscapeString($obj_category->tab_banner)."'
   						, mobile_banner =	'".$this->mysqlEscapeString($obj_category->mobile_banner)."'
   						, featured_status =	'".$this->mysqlEscapeString($obj_category->featured_status)."'
   						, category_order    =	'".$this->mysqlEscapeString($obj_category->category_order)."'
						, updated_on		=	CURRENT_TIMESTAMP
   						WHERE category_id	=	'".$this->mysqlEscapeString($obj_category->category_id)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function deleteCategory ($categoryId, $enabled)
	{
		try
		{
			$sql	=    "	UPDATE 	categories
							SET 	enabled 	=  '".$this->mysqlEscapeString($enabled)."'
							WHERE 	category_id	=	'".$this->mysqlEscapeString($categoryId)."'";
			return ($this->db_query($sql))?true:false;
		}
		catch(Exception $e)
		{	throw $e;	}
	}

    public function getCategoryfilesById($id)
    {
    	try {
    		$sql = "SELECT	category_thumb, desktop_banner, tab_banner, mobile_banner 
    				FROM categories
    				WHERE category_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0) {
    			$this->db_free_results($res);
    			return 0;
    		} else {
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }

    public function getCategoryById($id)
    {
    	try
    	{
    		$sql = "SELECT	* FROM categories
    				WHERE 	category_id	= '".$this->mysqlEscapeString($id)."'";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
	public function getCategoriesAndSubCategories()
	{
		try
		{
			$sql = 'SELECT 	* FROM categories WHERE enabled = "Y" ';

			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0)
			{
				$this->db_free_results($res);
				return 0;
			}
			else
			{
				$subcategory	=	new Subcategory();
				for($i=0;$row=$this->db_fetch_object($res);$i++)
				{
					$object		=	(object)'';
					$object->category_id		=	$row->category_id;
					$object->category_name		=	$row->category_name;
					$object->category_slug		=	$row->category_slug;
					$object->featured_status	=	$row->featured_status;
					$object->created_on			=	$row->created_on;
					$object->updated_on			=	$row->updated_on;
					$object->enabled			=	$row->enabled;
					$obj						=	$subcategory->getSubCategoriesByCategoryId($object->category_id);
					if(!empty($obj))
					{
						$object->subcategories	=	$obj;
						$arr[$i] = $object;
					}
				}
				$this->db_free_results($res);
				return $arr;
			}
		}
		catch(Exception $e)
		{	throw $e;	}
	}

   	public function checkCategorySlug($category_slug) {
      	try {
			$sql="SELECT category_id , category_name 
			FROM categories WHERE category_slug = '".$this->mysqlEscapeString($category_slug)."'";

			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0) {
				$this->db_free_results($res);
				return false;
			} else {
				$this->db_free_results($res);
				return true;
			}
      	}catch (Exception $e){ $this->db_error($e); }
   	}

   	public function checkOtherSimilarCategorySlug($category_id, $category_slug) {
      	try {
			$sql="SELECT category_id , category_name 
			FROM categories WHERE category_slug = '".$this->mysqlEscapeString($category_slug)."' AND category_id != '".$this->mysqlEscapeString($category_id)."' ";

			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0) {
				$this->db_free_results($res);
				return false;
			} else {
				$this->db_free_results($res);
				return true;
			}
      	}catch (Exception $e){ $this->db_error($e); }
   	}
}
?>
