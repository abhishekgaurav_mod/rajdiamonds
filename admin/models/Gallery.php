<?php
class Gallery extends DBSource
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	 public function addImage($obj_newsevent)
	{
		try
		{
	 		$sql = 'INSERT INTO `galleries`
							(
								`imagepath`
								, `title`
								, `created_on`
								, `updated_on`
							)
							VALUES
							(
								"'.$this->mysqlEscapeString($obj_newsevent->imagepath).'"
								, "'.$this->mysqlEscapeString($obj_newsevent->title).'"
								, CURRENT_TIMESTAMP
								, CURRENT_TIMESTAMP);';
			$this->db_query($sql);
			return $this->mysqlInsertId();
		}
		catch(Exception $e)
		{	throw $e;	}
	}

	public function getImageById($imageId)
	{
		try
		{
			$sql="	SELECT * FROM galleries
					WHERE image_id = '".$this->mysqlEscapeString($imageId)."'";
			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0)
			{
				$this->db_free_results($res);
				return 0;
			}
			else
			{
				$row    =   $this->db_fetch_object($res);
				$this->db_free_results($res);
				return $row;
			}
		}
		catch(Exception $e)
		{	throw $e;	}
	}

	public function getImages($start,$end)
	{
		try
		{
			$sql=  "  SELECT *	FROM  galleries
					  WHERE enabled  = 'Y' ORDER BY image_id DESC
					  LIMIT ".$this->mysqlEscapeString($start).", ".$this->mysqlEscapeString($end);


			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0)
			{
				$this->db_free_results($res);
				return 0;
			}
			else
			{
				for($i=0;$row=$this->db_fetch_object($res);$i++)
				{	$arr[$i] = $row;	}
				$this->db_free_results($res);
				return $arr;
			}
		}
		catch(Exception $e)
		{	throw $e;	}
	}
	public function deleteImage ($imageId, $status)
	{
		try
		{
   			$sql	=  "	DELETE FROM galleries
							WHERE image_id	 =	'".$this->mysqlEscapeString($imageId)."'";

   			return ($this->db_query($sql))?TRUE:FALSE;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

	public function getImagesCount()
	{
		try
		{
			$sql = "SELECT COUNT(image_id) As cnt	FROM galleries WHERE enabled = 'Y' ";
			$res=$this->db_query($sql);
			if($this->db_num_rows($res)==0)
			{
				$this->db_free_results($res);
				return 0;
			}
			else
			{
				$row    =   $this->db_fetch_object($res);
				$this->db_free_results($res);
				return $row->cnt;
			}
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

}
?>
