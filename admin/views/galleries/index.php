<script type="text/javascript">
	$(document).ready(function() {
	  	 getimages(1);
	});
</script>

<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>
	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>galleries/add" class="btn btn-primary">Add New Image</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>
		<div class="row">
		  	<div class="col-md-12">
			    <div class="x_panel">
			      	<div class="x_title">
				        <h2>Media Gallery</h2>
				        <div class="clearfix"></div>
			      	</div>
			      	<div class="x_content">
				        <div class="row">

							<div class="datalist galleries" id="image"> </div>

				        </div>
			      	</div>
			    </div>
		  	</div>
		</div>
	</div>
</div>

<script type="text/javascript">
var getimages	=	function (page){
	$.ajax({
		url: _URL+'galleries/paginate',
		data: "type=image&page="+page,
		type: 'GET',
		success: function(data) 
		{	$('#image').html(data);	},
		error: function() {},
		complete: function() 
		{	$('a[rel*=imagebox]').facebox();}
	});
};
</script>