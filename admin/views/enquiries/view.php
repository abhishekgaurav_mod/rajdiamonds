<div class="mainbody">
	<div class="datalist-head">
    	<h1><?=ucfirst($this->pagetitle)?></h1>
        <ul>
        	<li><a href="<?=_URL?>enquiries/index">Enquiries</a></li>
   		</ul>
	</div>
	<div class="datalist">
		<?php if (!empty($obj_enquiry)): ?>
		<div class="forms brdform floatleft pad10bottom">
			<div class="formhead"><?=ucfirst($obj_enquiry->first_name)?>
			</div>
			<div class="fieldbox pad10top enquiryreply">
				<div class="grid-5050">
					<div class="grid-5050-a pad10right">
						<div class="tickets"><div class="heading">Enquiry Info</div></div>
						<div class="fieldbox pad10top borderbox">
							<table width="100%" border="0">
							<tr>
								<td align="left" valign="top" width="50%" class="padzero">
									<table width="100%" border="0">
									<tr>
										<td class="label" width="150">Name</td>
										<td>: <?=ucfirst($obj_enquiry->first_name)?> </td>
									</tr>
									<tr>
										<td class="label">Contact Number</td>
										<td>: <?=$obj_enquiry->phone?></td>
									</tr>
									<tr>
										<td class="label">Email</td>
										<td>: <a href="mailto:<?=$obj_enquiry->email?>"><?=$obj_enquiry->email?></a></td>
									</tr>
									<tr>
										<td class="label" width="150">Organisation</td>
										<td>: <?=ucfirst($obj_enquiry->organisation)?></td>
									</tr>
									<tr>
										<td class="label" width="150">Interest</td>
										<td>: <?=ucfirst($obj_enquiry->interest)?></td>
									</tr>
									
									<tr>
										<td class="label">Created On</td>
										<td>: <?=date('jS F, Y g:s A',strtotime($obj_enquiry->created_on))?></td>
									</tr>
									</table>
								</td>
							</tr>
							</table>
						</div>
						
						<div class="tickets pad10top">
							<div class="heading"><span class="text">Message</span><span class="sap">:</span><span class="headtext"> <?=str_replace(PHP_EOL, '<br />', $obj_enquiry->details)?></span></div>
						</div>
					</div>
					<!--
					<div class="grid-5050-b pad10left bordersolidleft">
						<form name="frmenquiryreply" id="frmenquiryreply" action="<?=_URL?>enquiries/sendmail" method="post">
							<input type="hidden" name="action" value="enquiry" />
							<input type="hidden" name="enquiryId" value="<?=$obj_enquiry->enquiry_id?>" />
							<input type="hidden" name="email" value="<?=$obj_enquiry->email?>" />
							<input type="hidden" name="name" value="<?=$obj_enquiry->name?>" />
							<div class="tickets">
								<div class="pad10bottom"><div class="heading">Respond to this enquiry email</div></div>
							</div>
							<div class="field"><textarea tabindex="4" name="response" id="response" rows="4" cols="50" placeholder="Send response.."></textarea></div>
							<div class="field textalignright pad10top pad10bottom">
								<span class="loading">Please wait...</span>
								<button type="submit" name="save" value="Save" class="inline" >Send Mail</button>
							</div>
						</form>
					</div>
					-->
				</div>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>