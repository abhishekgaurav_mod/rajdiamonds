<div class="mainbody">
	<div class="datalist-head">
    	<h1><?=ucfirst($this->pagetitle)?></h1>
        <ul>
        	<li><a href="<?=_URL?>enquiries/index<?=(($status=='Y')?'?status=n':'')?>"><?=(($status=='Y')?'Disabled ':'Enabled ').'Enquiries'?></a></li>
		</ul>
	</div>
	<div class="datalist">
    	<table class="display" id="example">
        <thead>
        	<tr><th width="80">Enquiry Id</th>
        		<th  width="160">Name</th>
        		<th  width="160">Phone No</th>
                <th>Email </th>
                <th>Interest </th>
                <th width="110">Created On</th>
                <th width="110">Updated On</th>
                <th>Enabled</th>
                <th width="90">Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="5" class="dataTables_empty">Loading data from server</td>
			</tr>
		</tbody>
        </table>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	var page	=	0;
    oTable = $('#example').dataTable( {
    	"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<?=$ajaxSource?>",
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({ "name": "status", "value": "<?=$status?>" });
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			}).done(function() {});
		},
		"fnDrawCallback": function () { page	=	this.fnPagingInfo().iPage; },
        "sPaginationType": "full_numbers",
        "iDisplayLength": <?=_DATA_LIMIT?>,
        "bProcessing": true,
        "sDom": 'CRTfrtip',
    	"aoColumns": [null,null,null,null,null,null,null,{ "bVisible": false},
						{	"sName": "id",
							"bSearchable": false,
							"bSortable": false,
							"fnRender": function (oObj) {
								var html	=	'<a href="<?=_URL?>enquiries/view?enquiryId='+oObj.aData[0]+'">View</a>';
									html	+=	'<span class="separator">|</span><a onclick="javascript:deleters(\'Enquiry\', \''+oObj.aData[0]+'\',\''+((oObj.aData[7]=='Y')?'N':'Y')+'\',\''+((oObj.aData[7]=='Y')?'Disable':'Enable')+'\')">'+((oObj.aData[7]=='Y')?'Disable':'Enable')+'</a>';							
								return html;
							}
						}
    	],
        "aaSorting": [[ 0, "desc" ]],
    });
});
</script>