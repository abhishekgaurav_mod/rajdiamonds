<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		            	<a href="<?=_URL?>newsletters" class="btn btn-primary">Newsletters</a>
		            	<a href="<?=_URL?>newsletters/add" class="btn btn-primary">Add New Newsletter</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addnewsletter">

							<form name="frmaddnewsletter" id="frmaddnewsletter" action="<?=_URL?>newsletters/add" accept-charset="utf-8"  method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<div class="fieldbox pad10top">
									<input type="hidden" name="action" value="<?=$action?>" />
									<?php if ($action=='edit' && !empty($obj_newsletter)): ?>
										<input type="hidden" name="newsletterId" value="<?=$id?>" />
										<input type="hidden" name="imagename" value="<?=(empty($obj_newsletter->imagepath))?null:$obj_newsletter->imagepath?>" />
									<?php endif;?>

									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label for="subject">Newsletter Subject : </label>
										<input name="subject" type="text" id="subject" placeholder="Subject" value="<?=(!empty($obj_newsletter->subject)?$obj_newsletter->subject:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
	                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
									</div>

									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label for="imagepath">Upload Image</label>
										<input type="file" name="imagepath" id="imagepath" />
										<?=(!empty($obj_newsletter->imagepath)?'<a href="'._NEWSLETTER_ORG_URL.$obj_newsletter->imagepath.'" rel="facebox">'.(!empty($obj_newsletter->imagepath)?'<img src="'._NEWSLETTER_ORG_URL.$obj_newsletter->imagepath.'" alt="" class="img-responsive iconView">':'Not uploaded yet !').'</a>':'')?>
									</div>

									<div class="clearfix"></div>
			                      	<div class="ln_solid"></div>
			                      	<div class="form-group">
										<div class="field col-md-9 col-sm-9 col-xs-12">
											<span class="loading">Please wait...</span>
											<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
											<button type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
										</div>
			                      	</div>
								</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>
