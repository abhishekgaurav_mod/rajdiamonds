<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
						<a href="<?=_URL?>newsletters/index<?=(($status=='Y')?'?status=n':'')?>" class="btn btn-primary"><?=(($status=='Y')?'Disabled ':'Enabled ').'Newsletter'?></a>
			            <a href="<?=_URL?>newsletters/add" class="btn btn-primary">Add New Newsletter</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_content">
						
						<div class="datalist">
					    	<table class="display" id="example">
					        <thead>
					        	<tr>
					        		<th width="120">Newsletter Id</th>
					                <th>Subject</th>
					                <th width="170">Send Date</th>
					                <th width="140">Subscriber Count</th>
					                <th width="120">Send Count</th>
					                <th width="170">Created On</th>
					                <th>Enabled</th>
					                <th width="120">Actions</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="6" class="dataTables_empty">Loading data from server</td>
								</tr>
							</tbody>
					        </table>
						</div>
		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>

<script type="text/javascript">
$(document).ready(function() {
	var page	=	0;
    oTable = $('#example').dataTable( {
    	"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "<?=$ajaxSource?>",
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			aoData.push({ "name": "status", "value": "<?=$status?>" });
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": fnCallback
			}).done(function() {});
		},
		"fnDrawCallback": function () { page	=	this.fnPagingInfo().iPage; },
        "sPaginationType": "full_numbers",
        "iDisplayLength": <?=_DATA_LIMIT?>,
        "bProcessing": true,
        "sDom": 'CRTfrtip',
    	"aoColumns": [null,null,null,null,null,null,{ "bVisible": false},
						{	"sName": "id",
							"bSearchable": false,
							"bSortable": false,
							"fnRender": function (oObj) {
								var html	=	'<a href="<?=_URL?>newsletters/view?id='+oObj.aData[0]+'">View</a><span class="separator">|</span>';
								html	+=	'<a href="<?=_URL?>newsletters/add?action=edit&id='+oObj.aData[0]+'">Edit</a><span class="separator">|</span>';
								html	+=	'<a onclick="javascript:deleters(\'Newsletter\', \''+oObj.aData[0]+'\',\''+((oObj.aData[6]=='Y')?'N':'Y')+'\',\''+((oObj.aData[6]=='Y')?'Disable':'Enable')+'\')">'+((oObj.aData[6]=='Y')?'Disable':'Enable')+'</a>';
								return html;
							}
						}
    	],
        "aaSorting": [[ 0, "desc" ]],
    });
});
</script>