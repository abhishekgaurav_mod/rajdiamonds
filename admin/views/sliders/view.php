<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		            	<a href="<?=_URL?>sliders" class="btn btn-primary">Sliders</a>
		            	<a href="<?=_URL?>sliders/add" class="btn btn-primary">Add New Slider</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($obj_slider)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>sliders/add?action=edit&id=<?=$id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
				                    	<table class="table" width="100%" border="0">
											<tr>
												<td><b>Slider Id</b></td>
												<td>: <?=$obj_slider->slider_id?></td>
											</tr>
											<tr>
												<td><b>Slider Order</b></td>
												<td>: <?=$obj_slider->slider_order?></td>
											</tr>
											<tr>
												<td width="1"><b>Desktop Banner</b></td>
												<td width="220">: <?=(!empty($obj_slider->desktop_banner)?'<img src="'._SLIDER_ORG_URL.'desktop-banner/'.$obj_slider->desktop_banner.'" alt="'.$obj_slider->category_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td width="1"><b>Tablet Banner</b></td>
												<td width="220">: <?=(!empty($obj_slider->tab_banner)?'<img src="'._SLIDER_ORG_URL.'tab-banner/'.$obj_slider->tab_banner.'" alt="'.$obj_slider->category_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td width="1"><b>Mobile Banner</b></td>
												<td width="220">: <?=(!empty($obj_slider->mobile_banner)?'<img src="'._SLIDER_ORG_URL.'mobile-banner/'.$obj_slider->mobile_banner.'" alt="'.$obj_slider->category_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td><b>Created On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_slider->created_on))?></td>
											</tr>
											<tr>
												<td><b>Updated On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_slider->updated_on))?></td>
											</tr>
										</table>
				                    </div>
		      					</div>
		      				</div>
	      					<div class="col-md-6 col-xs-12">
	      						<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
										<h3>Description :- </h3>
				                      	<div><?=(!empty($obj_slider->description)?ucfirst($obj_slider->description):'Sorry, No description available')?></div><br><br>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>