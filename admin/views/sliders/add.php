<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		            	<a href="<?=_URL?>sliders" class="btn btn-primary">Sliders</a>
		            	<a href="<?=_URL?>sliders/add" class="btn btn-primary">Add New Slider</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">

							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>sliders/add" accept-charset="utf-8"  method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<div class="fieldbox pad10top">
									<input type="hidden" name="action" value="<?=$action?>" />
									<?php if ($action=='edit' && !empty($obj_slider)): ?>
										<input type="hidden" name="sliderId" value="<?=$id?>" />
										<input type="hidden" name="desktop_bannerId" value="<?=(empty($obj_slider->desktop_banner))?null:$obj_slider->desktop_banner?>" />
										<input type="hidden" name="tab_bannerId" value="<?=(empty($obj_slider->tab_banner))?null:$obj_slider->tab_banner?>" />
										<input type="hidden" name="mobile_bannerId" value="<?=(empty($obj_slider->mobile_banner))?null:$obj_slider->mobile_banner?>" />
									<?php endif;?>

								    <div class="row">
								      	<div class="col-md-4 col-xs-12">
								      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
												<label>Desktop Banner :  </label>
												<input type="file" name="desktop_banner" /><?=(!empty($obj_slider->desktop_banner)?'<a href="'._SLIDER_ORG_URL.'desktop-banner/'.$obj_slider->desktop_banner.'" rel="facebox">'.(!empty($obj_slider->desktop_banner)?'<img src="'._SLIDER_ORG_URL.'desktop-banner/'.$obj_slider->desktop_banner.'" alt="Desktop Slider '.$id.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
											</div>
								      	</div>
								      	<div class="col-md-4 col-xs-12">
								      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
												<label>Tablet Banner :  </label>
												<input type="file" name="tab_banner" />
												<?=(!empty($obj_slider->tab_banner)?'<a href="'._SLIDER_ORG_URL.'tab-banner/'.$obj_slider->tab_banner.'" rel="facebox">'.(!empty($obj_slider->tab_banner)?'<img src="'._SLIDER_ORG_URL.'tab-banner/'.$obj_slider->tab_banner.'" alt="Tablet Slider '.$id.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
											</div>
								      	</div>
								      	<div class="col-md-4 col-xs-12">
								      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
												<label>Mobile Banner :  </label>
												<input type="file" name="mobile_banner" /><?=(!empty($obj_slider->mobile_banner)?'<a href="'._SLIDER_ORG_URL.'mobile-banner/'.$obj_slider->mobile_banner.'" rel="facebox">'.(!empty($obj_slider->mobile_banner)?'<img src="'._SLIDER_ORG_URL.'mobile-banner/'.$obj_slider->mobile_banner.'" alt="Mobile Slider '.$id.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
											</div>
								      	</div>
								    </div>
								
									<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label for="slider_order">Slider Order: </label>
										<input name="slider_order" type="text" id="slider_order" placeholder="Slider Order" value="<?=(!empty($obj_slider->slider_order)?$obj_slider->slider_order:'')?>" maxlength="4" class="form-control has-feedback-left" />
		                        		<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
									</div>

									<!-- <div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
										<label for="imagepath">Upload Image</label>
										<input type="file" name="imagepath" id="imagepath" />
										<?=(!empty($obj_slider->imagepath)?'<a href="'._SLIDER_ORG_URL.$obj_slider->imagepath.'" rel="facebox">'.(!empty($obj_slider->imagepath)?'<img src="'._SLIDER_ORG_URL.$obj_slider->imagepath.'" alt="" class="img-responsive iconView">':'Not uploaded yet !').'</a>':'')?>
									</div> -->

									<div class="field col-md-12 col-sm-12 col-xs-12">
										<label>Description</label>
										<textarea name="description" id="description" rows="7" cols="50" placeholder="Slider text..."><?=(!empty($obj_slider->description)?$obj_slider->description:'')?></textarea>
									</div>

									<div class="clearfix"></div>
			                      	<div class="ln_solid"></div>
			                      	<div class="form-group">
										<div class="field col-md-9 col-sm-9 col-xs-12">
											<span class="loading">Please wait...</span>
											<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
											<button type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
										</div>
			                      	</div>
								</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>

<script type="text/javascript">
/*CKEDITOR.replace('description',{allowedContent:true,resize_enabled:'false',resize_maxHeight:'280',resize_minHeight: '280',toolbar:[{name:'document',items:['Source','-','NewPage','Preview','-','Templates']},{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },{name:'basicstyles',items:['Bold','Italic']}]});*/

// CK EDITOR
var ckConfig = {
		allowedContent: true, resize_enabled:'false',width: '100%', height: '350px', resize_maxHeight:'320', resize_minHeight: '600',
		toolbar:[
			{ name:'clipboard', items:['Cut', 'Copy', 'Paste', '-', 'Undo', 'Redo'] },
			{ name:'document', items:['source'] },
			{ name:'paragraph', /*groups: ['list', 'indent', 'blocks', 'align', 'bidi'],*/ items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
			{ name:'basicstyles', items:['Bold','Italic','Underline','Strike', 'Scayt'] },
			'/',
	       	{ name:'insert', items: ['HorizontalRule', 'SpecialChar', 'PageBreak' , 'Image', /*'Link', 'Unlink',*/ 'Maximize', 'Table', 'Source'] },
			{ name:'styles', items : ['Styles', 'Format','FontSize'] }
		]
	};
CKEDITOR.config.forcePasteAsPlainText 		= true;
/*CKEDITOR.config.filebrowserImageBrowseUrl 	= '<?=_URL?>fileuploader/index?type=articlecontentimages';*/
CKEDITOR.replace('description',ckConfig);


</script>
