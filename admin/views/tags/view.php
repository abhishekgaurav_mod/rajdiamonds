<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>tags/index" class="btn btn-primary">Tags</a>
						<a href="<?=_URL?>tags/add" class="btn btn-primary">Add New Tag</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($object)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>tags/add?action=edit&id=<?=$id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
				                    	<table class="table" width="100%" border="0">
											<tr>
												<td><b>Tag Id</b></td>
												<td>: <?=$object->tag_id?></td>
											</tr>
											<tr>
												<td><b>Tag Title</b></td>
												<td>: <?=$object->tag_name?></td>
											</tr>
											<tr>
												<td><b>Tag Description</b></td>
												<td>: <?=$object->tag_description?></td>
											</tr>
											<tr>
												<td><b>Featured Status</b></td>
												<td>: <?=($object->featured_status=='Y')?'Yes':'No'?></td>
											</tr>
											<tr>
												<td width="1"><b>Tag Thumb</b></td>
												<td width="220">: <?=(!empty($object->tag_thumb)?'<img src="'._TAG_ORG_URL.'thumb/'.$object->tag_thumb.'" alt="'.$object->tag_name.'" class="img-responsive thumbView">':'Not Available')?></td>
											</tr>
											<tr>
												<td width="1"><b>Desktop Banner</b></td>
												<td width="220">: <?=(!empty($object->desktop_banner)?'<img src="'._TAG_ORG_URL.'desktop-banner/'.$object->desktop_banner.'" alt="'.$object->tag_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td width="1"><b>Tab Banner</b></td>
												<td width="220">: <?=(!empty($object->tab_banner)?'<img src="'._TAG_ORG_URL.'tab-banner/'.$object->tab_banner.'" alt="'.$object->tag_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td width="1"><b>Mobile Banner</b></td>
												<td width="220">: <?=(!empty($object->mobile_banner)?'<img src="'._TAG_ORG_URL.'mobile-banner/'.$object->mobile_banner.'" alt="'.$object->tag_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td><b>Enabled</b></td>
												<td>: <?=($object->enabled=='Y')?'Yes':'No'?></td>
											</tr>
											<tr>
												<td><b>Created On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($object->created_on))?></td>
											</tr>
											<tr>
												<td><b>Updated On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($object->updated_on))?></td>
											</tr>
										</table>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>