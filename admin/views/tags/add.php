<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>tags/index" class="btn btn-primary">Tags</a>
						<a href="<?=_URL?>tags/add" class="btn btn-primary">Add New Tag</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-6 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">
							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>tags/add" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<input type="hidden" name="action" value="<?=$action?>" />
								<?php if ($action=='edit' && !empty($obj_tag)): ?>
								<input type="hidden" name="tagId" value="<?=$id?>" />
								<input type="hidden" name="tag_thumbId" value="<?=(empty($obj_tag->tag_thumb))?null:$obj_tag->tag_thumb?>" />
								<input type="hidden" name="desktop_bannerId" value="<?=(empty($obj_tag->desktop_banner))?null:$obj_tag->desktop_banner?>" />
								<input type="hidden" name="tab_bannerId" value="<?=(empty($obj_tag->tab_banner))?null:$obj_tag->tab_banner?>" />
								<input type="hidden" name="mobile_bannerId" value="<?=(empty($obj_tag->mobile_banner))?null:$obj_tag->mobile_banner?>" />
								<?php endif;?>

								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="title">Tag Title: </label>
									<input name="title" type="text" id="title" placeholder="Tag Title" value="<?=(!empty($obj_tag->tag_name)?$obj_tag->tag_name:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
	                        		<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>

								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="tag_description">Tag Description: </label>
									<input name="tag_description" type="text" id="tag_description" placeholder="Tag Description" value="<?=(!empty($obj_tag->tag_description)?$obj_tag->tag_description:'')?>" autocomplete="off" class="form-control has-feedback-left" />
	                        		<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label>Featured Status: </label>
									<select id="featured_status" name="featured_status" class="form-control has-feedback-left">
										<option value="">-Select Featured Status-</option>
										<option value="Y" <?=((!empty($obj_tag->featured_status) && $obj_tag->featured_status == 'Y')?'selected':'')?> >Yes</option>
										<option value="N" <?=((!empty($obj_tag->featured_status) && $obj_tag->featured_status == 'N')?'selected':'')?> >No</option>
									</select>
                    				<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>

							    <div class="row">
							      	<div class="col-md-6 col-xs-12">
							      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Tag Thumb :  </label>
											<input type="file" name="tag_thumb" /><?=(!empty($obj_tag->tag_thumb)?'<a href="'._TAG_ORG_URL.'thumb/'.$obj_tag->tag_thumb.'" rel="facebox">'.(!empty($obj_tag->tag_thumb)?'<img src="'._TAG_ORG_URL.'thumb/'.$obj_tag->tag_thumb.'" alt="'.$obj_tag->tag_name.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
										</div>
							      	</div>
							    </div>

							    <div class="row">
							      	<div class="col-md-4 col-xs-12">
							      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Desktop Banner :  </label>
											<input type="file" name="desktop_banner" /><?=(!empty($obj_tag->desktop_banner)?'<a href="'._TAG_ORG_URL.'desktop-banner/'.$obj_tag->desktop_banner.'" rel="facebox">'.(!empty($obj_tag->desktop_banner)?'<img src="'._TAG_ORG_URL.'desktop-banner/'.$obj_tag->desktop_banner.'" alt="'.$obj_tag->tag_name.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
										</div>
							      	</div>
							      	<div class="col-md-4 col-xs-12">
							      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Tablet Banner :  </label>
											<input type="file" name="tab_banner" /><?=(!empty($obj_tag->tab_banner)?'<a href="'._TAG_ORG_URL.'tab-banner/'.$obj_tag->tab_banner.'" rel="facebox">'.(!empty($obj_tag->tab_banner)?'<img src="'._TAG_ORG_URL.'tab-banner/'.$obj_tag->tab_banner.'" alt="'.$obj_tag->tag_name.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
										</div>
							      	</div>
							      	<div class="col-md-4 col-xs-12">
							      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Mobile Banner :  </label>
											<input type="file" name="mobile_banner" /><?=(!empty($obj_tag->mobile_banner)?'<a href="'._TAG_ORG_URL.'mobile-banner/'.$obj_tag->mobile_banner.'" rel="facebox">'.(!empty($obj_tag->mobile_banner)?'<img src="'._TAG_ORG_URL.'mobile-banner/'.$obj_tag->mobile_banner.'" alt="'.$obj_tag->tag_name.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
										</div>
							      	</div>
							    </div>

								<div class="clearfix"></div>
		                      	<div class="ln_solid"></div>
		                      	<div class="form-group">
									<div class="field col-md-9 col-sm-9 col-xs-12">
										<span class="loading">Please wait...</span>
										<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
										<button type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
									</div>
		                      	</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>