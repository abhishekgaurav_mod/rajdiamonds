<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-6 col-sm-6 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>categories" class="btn btn-primary">Categories</a>
						<a href="<?=_URL?>products/index?id=<?=$categoryId?>" class="btn btn-primary">All <?=$obj_category->category_name?></a>
						<a href="<?=_URL?>products/add?categoryId=<?=$categoryId?>" class="btn btn-primary">Add New <?=$obj_category->category_name?></a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">

							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>products/add" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<input type="hidden" name="action" value="<?=$action?>" />
								<?php if ($action=='edit' && !empty($obj_product)): ?>
								<input type="hidden" name="productId" value="<?=$productId?>" />
								<input type="hidden" name="product_thumbId" value="<?=(empty($obj_product->product_thumb))?null:$obj_product->product_thumb?>" />
								<?php endif;?>
								<input type="hidden" name="categoryId" value="<?=$categoryId?>" />

							    <div class="row">
							      	<div class="col-md-6 col-xs-12">
										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label for="title">Product Name : </label>
											<input name="title" type="text" id="title" placeholder="Product Name" value="<?=(!empty($obj_product->product_name)?$obj_product->product_name:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
		                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>

										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label for="product_code">Product Code : </label>
											<input name="product_code" type="text" id="product_code" placeholder="Product Code" value="<?=(!empty($obj_product->product_code)?$obj_product->product_code:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
		                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>

										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label for="bar_code">Bar Code : </label>
											<input name="bar_code" type="text" id="bar_code" placeholder="Bar Code" value="<?=(!empty($obj_product->bar_code)?$obj_product->bar_code:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
		                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>

								      	<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									        <label>Tags:</label>
									        <input name="tags_string" type="text" id="tag_ids" class="ele-field ele-required" value="<?=(!empty($obj_product->tags_string)?$obj_product->tags_string:'')?>" placeholder="Tags" />
							      	  	</div>

										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label for="product_order">Product Order : </label>
											<input name="product_order" type="text" id="product_order" placeholder="Product Code" value="<?=(!empty($obj_product->product_order)?$obj_product->product_order:'')?>" maxlength="50" autocomplete="off" class="form-control has-feedback-left" />
		                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>
								
										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Visible on Front: </label>
											<select id="front_visible" name="front_visible" class="form-control has-feedback-left">
												<option value="">-Select Front Visible Status-</option>
												<option value="Y" <?=((!empty($obj_product->front_visible) && $obj_product->front_visible == 'Y')?'selected':'')?> >Yes</option>
												<option value="N" <?=((!empty($obj_product->front_visible) && $obj_product->front_visible == 'N')?'selected':'')?> >No</option>
											</select>
		                    				<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
										</div>
							      	  	
										<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Product Thumb :  </label>
											<input type="file" name="product_thumb" />
											<?=(!empty($obj_product->product_thumb)?'<a href="'._PRODUCT_ORG_URL.$obj_product->product_thumb.'" rel="facebox">'.(!empty($obj_product->product_thumb)?'<img src="'._PRODUCT_ORG_URL.$obj_product->product_thumb.'" alt="'.$obj_product->product_name.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
										</div>
				                    </div>

							      	<div class="col-md-6 col-xs-12">
										<div class="field padzero">
											<label>Description : </label>
											<textarea name="description" id="description" rows="7" cols="50" placeholder="Testimonial Text..."><?=(!empty($obj_product->description)?stripcslashes($obj_product->description):'')?></textarea>
										</div>

										<div class="clearfix"></div>
				                      	<div class="ln_solid"></div>
				                      	<div class="form-group">
											<div class="field col-md-9 col-sm-9 col-xs-12">
												<span class="loading">Please wait...</span>
												<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
												<button type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
											</div>
				                      	</div>
							      	</div>
							      	
								</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>

  	</div>
</div>
<div class="clearfix"></div>

<script type="text/javascript">
/*CKEDITOR.replace('description',{allowedContent:true,resize_enabled:'false',resize_maxHeight:'280',resize_minHeight: '280',toolbar:[{name:'document',items:['Source','-','NewPage','Preview','-','Templates']},{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },{name:'basicstyles',items:['Bold','Italic']}]});*/
var ckConfig = {
		allowedContent: true, resize_enabled:'false',width: '100%', height: '350px', resize_maxHeight:'320', resize_minHeight: '600',
		toolbar:[
			{ name:'document', items:['source'] },
			{ name:'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
			{ name:'basicstyles', items:['Bold','Italic','Underline'] },
			'/',
	       	{ name:'insert', items: ['SpecialChar', 'Maximize', 'Source'] },
			{ name:'styles', items : ['Styles', 'Format','FontSize'] }
		]
	};
CKEDITOR.config.forcePasteAsPlainText 		= true;
CKEDITOR.replace('description',ckConfig);
</script>
