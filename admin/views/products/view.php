<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle.' ('.$categoryName.')')?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-6 col-sm-6 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>categories" class="btn btn-primary">Categories</a>
						<a href="<?=_URL?>products/index?id=<?=$categoryId?>" class="btn btn-primary">All <?=$obj_product->category_name?></a>
						<a href="<?=_URL?>products/add?categoryId=<?=$categoryId?>" class="btn btn-primary">Add New <?=$obj_product->category_name?></a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($obj_product)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>products/add?action=edit&productId=<?=$id?>&categoryId=<?=$obj_product->category_id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
				                    <table class="table" width="100%" border="0">
											<tr>
												<td><b>Product Id</b></td>
												<td>: <?=$obj_product->product_id?></td>
											</tr>
											<tr>
												<td><b>Product Name</b></td>
												<td>: <?=$obj_product->product_name?></td>
											</tr>
											<tr>
												<td><b>Product Code</b></td>
												<td>: <?=$obj_product->product_code?></td>
											</tr>
											<tr>
												<td><b>Bar Code</b></td>
												<td>: <?=$obj_product->bar_code?></td>
											</tr>
											<tr>
												<td><b>Description</b></td>
												<td>: <?=$obj_product->description?></td>
											</tr>
											<tr>
												<td width="1"><b>Product Image</b></td>
												<td width="220">: <?=(!empty($obj_product->product_thumb)?'<img src="'._PRODUCT_ORG_URL.$obj_product->product_thumb.'" alt="'.$obj_product->product_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td><b>Category Title</b></td>
												<td>: <?=$obj_product->category_name?></td>
											</tr>
											<tr>
												<td><b>Product Tags</b></td>
												<td>: <?=implode(', ', $preTags)?></td>
											</tr>
											<tr>
												<td><b>Product Order</b></td>
												<td>: <?=(!empty($obj_product->product_order)?$obj_product->product_order:'- Not Available -')?></td>
											</tr>
											<tr>
												<td><b>Visible on front</b></td>
												<td>: <?=($obj_product->front_visible=='Y')?'Yes':'No'?></td>
											</tr>
											<tr>
												<td><b>Enabled</b></td>
												<td>: <?=($obj_product->enabled=='Y')?'Yes':'No'?></td>
											</tr>
											<tr>
												<td><b>Created On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_product->created_on))?></td>
											</tr>
											<tr>
												<td><b>Updated On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_product->updated_on))?></td>
											</tr>
										</table>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>