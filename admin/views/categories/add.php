<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>categories/index" class="btn btn-primary">Categories</a>
						<a href="<?=_URL?>categories/add" class="btn btn-primary">Add New Category</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-6 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2><?=ucfirst($action).' '.ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addslider">
							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>categories/add" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<input type="hidden" name="action" value="<?=$action?>" />
								<?php if ($action=='edit' && !empty($obj_category)): ?>
								<input type="hidden" name="categoryId" value="<?=$id?>" />
								<input type="hidden" name="category_thumbId" value="<?=(empty($obj_category->category_thumb))?null:$obj_category->category_thumb?>" />
								<input type="hidden" name="desktop_bannerId" value="<?=(empty($obj_category->desktop_banner))?null:$obj_category->desktop_banner?>" />
								<input type="hidden" name="tab_bannerId" value="<?=(empty($obj_category->tab_banner))?null:$obj_category->tab_banner?>" />
								<input type="hidden" name="mobile_bannerId" value="<?=(empty($obj_category->mobile_banner))?null:$obj_category->mobile_banner?>" />
								<?php endif;?>

								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="title">Category Title: </label>
									<input name="title" type="text" id="title" placeholder="Category Title" value="<?=(!empty($obj_category->category_name)?$obj_category->category_name:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
	                        		<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>

								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="category_description">Category Description: </label>
									<input name="category_description" type="text" id="category_description" placeholder="Category Description" value="<?=(!empty($obj_category->category_description)?$obj_category->category_description:'')?>" autocomplete="off" class="form-control has-feedback-left" />
	                        		<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label>Featured Status: </label>
									<select id="featured_status" name="featured_status" class="form-control has-feedback-left">
										<option value="">-Select Featured Status-</option>
										<option value="Y" <?=((!empty($obj_category->featured_status) && $obj_category->featured_status == 'Y')?'selected':'')?> >Yes</option>
										<option value="N" <?=((!empty($obj_category->featured_status) && $obj_category->featured_status == 'N')?'selected':'')?> >No</option>
									</select>
                    				<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="category_order">Category Order: </label>
									<input name="category_order" type="text" id="category_order" placeholder="Category Order" value="<?=(!empty($obj_category->category_order)?$obj_category->category_order:'')?>" maxlength="4" class="form-control has-feedback-left" />
	                        		<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>

							    <div class="row">
							      	<div class="col-md-6 col-xs-12">
							      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Category Thumb :  </label>
											<input type="file" name="category_thumb" /><?=(!empty($obj_category->category_thumb)?'<a href="'._CATEGORY_ORG_URL.'thumb/'.$obj_category->category_thumb.'" rel="facebox">'.(!empty($obj_category->category_thumb)?'<img src="'._CATEGORY_ORG_URL.'thumb/'.$obj_category->category_thumb.'" alt="'.$obj_category->category_name.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
										</div>
							      	</div>
							    </div>

							    <div class="row">
							      	<div class="col-md-4 col-xs-12">
							      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Desktop Banner :  </label>
											<input type="file" name="desktop_banner" /><?=(!empty($obj_category->desktop_banner)?'<a href="'._CATEGORY_ORG_URL.'desktop-banner/'.$obj_category->desktop_banner.'" rel="facebox">'.(!empty($obj_category->desktop_banner)?'<img src="'._CATEGORY_ORG_URL.'desktop-banner/'.$obj_category->desktop_banner.'" alt="'.$obj_category->category_name.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
										</div>
							      	</div>
							      	<div class="col-md-4 col-xs-12">
							      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Tablet Banner :  </label>
											<input type="file" name="tab_banner" />
											<?=(!empty($obj_category->tab_banner)?'<a href="'._CATEGORY_ORG_URL.'tab-banner/'.$obj_category->tab_banner.'" rel="facebox">'.(!empty($obj_category->tab_banner)?'<img src="'._CATEGORY_ORG_URL.'tab-banner/'.$obj_category->tab_banner.'" alt="'.$obj_category->category_name.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
										</div>
							      	</div>
							      	<div class="col-md-4 col-xs-12">
							      		<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
											<label>Mobile Banner :  </label>
											<input type="file" name="mobile_banner" /><?=(!empty($obj_category->mobile_banner)?'<a href="'._CATEGORY_ORG_URL.'mobile-banner/'.$obj_category->mobile_banner.'" rel="facebox">'.(!empty($obj_category->mobile_banner)?'<img src="'._CATEGORY_ORG_URL.'mobile-banner/'.$obj_category->mobile_banner.'" alt="'.$obj_category->category_name.'" class="img-responsive iconView">':'').'</a>':'<br><h4><b>Not uploaded yet !</b></h4>')?>
										</div>
							      	</div>
							    </div>

								<div class="clearfix"></div>
		                      	<div class="ln_solid"></div>
		                      	<div class="form-group">
									<div class="field col-md-9 col-sm-9 col-xs-12">
										<span class="loading">Please wait...</span>
										<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
										<button type="reset" name="reset" value="Clear" class="inline btn btn-success" >Clear</button>
									</div>
		                      	</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>