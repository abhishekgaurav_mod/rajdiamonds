<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<div class="title_right pull-right">
		        <div class="col-md-5 col-sm-5 col-xs-12 pull-right top_search">
		          	<div class="pull-right">
		          		<a href="<?=_URL?>categories/index" class="btn btn-primary">Categories</a>
						<a href="<?=_URL?>categories/add" class="btn btn-primary">Add New Category</a>
		          	</div>
		        </div>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($obj_category)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>categories/add?action=edit&id=<?=$id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
				                    	<table class="table" width="100%" border="0">
											<tr>
												<td><b>Category Id</b></td>
												<td>: <?=$obj_category->category_id?></td>
											</tr>
											<tr>
												<td><b>Category Title</b></td>
												<td>: <?=$obj_category->category_name?></td>
											</tr>
											<tr>
												<td><b>Category Description</b></td>
												<td>: <?=$obj_category->category_description?></td>
											</tr>
											<tr>
												<td><b>Featured Status</b></td>
												<td>: <?=($obj_category->featured_status=='Y')?'Yes':'No'?></td>
											</tr>
											<tr>
												<td><b>Category Order</b></td>
												<td>: <?=$obj_category->category_order?></td>
											</tr>
											<tr>
												<td width="1"><b>Category Thumb</b></td>
												<td width="220">: <?=(!empty($obj_category->category_thumb)?'<img src="'._CATEGORY_ORG_URL.'thumb/'.$obj_category->category_thumb.'" alt="'.$obj_category->category_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td width="1"><b>Desktop Banner</b></td>
												<td width="220">: <?=(!empty($obj_category->desktop_banner)?'<img src="'._CATEGORY_ORG_URL.'desktop-banner/'.$obj_category->desktop_banner.'" alt="'.$obj_category->category_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td width="1"><b>Tablet Banner</b></td>
												<td width="220">: <?=(!empty($obj_category->tab_banner)?'<img src="'._CATEGORY_ORG_URL.'tab-banner/'.$obj_category->tab_banner.'" alt="'.$obj_category->category_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td width="1"><b>Mobile Banner</b></td>
												<td width="220">: <?=(!empty($obj_category->mobile_banner)?'<img src="'._CATEGORY_ORG_URL.'mobile-banner/'.$obj_category->mobile_banner.'" alt="'.$obj_category->category_name.'" class="img-responsive thumbView">':'- Not Available -')?></td>
											</tr>
											<tr>
												<td><b>Enabled</b></td>
												<td>: <?=($obj_category->enabled=='Y')?'Yes':'No'?></td>
											</tr>
											<tr>
												<td><b>Created On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_category->created_on))?></td>
											</tr>
											<tr>
												<td><b>Updated On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_category->updated_on))?></td>
											</tr>
										</table>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>