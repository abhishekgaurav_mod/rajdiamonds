<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left pull-left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

		<?php if (!empty($obj_admin)): ?>
	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<ul class="nav navbar-right panel_toolbox">
							<a href="<?=_URL?>admins/add?action=edit&id=<?=$id?>" class="btn btn-primary">Update</a>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="row">
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
					                    <table class="table" width="100%" border="0">
					                    	<tr>
												<td><b>Name</b></td>
												<td>: <?=ucfirst($obj_admin->name)?></td>
											</tr>
											<tr>
												<td><b>Email</b></td>
												<td>: <a href="mailto:<?=$obj_admin->email?>"><?=$obj_admin->email?></a></td>
											</tr>
											<tr>
												<td><b>Last Sign-in IP</b></td>
												<td>: <?=$obj_admin->current_sign_in_ip?></td>
											</tr>
											<tr>
												<td><b>Last Sign-in At</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_admin->current_sign_in_at))?></td>
											</tr>
					                    </table>
				                    </div>
		      					</div>
		      				</div>
	      					<div class="col-md-6 col-xs-12">
								<div class="bs-example" data-example-id="simple-jumbotron">
				                    <div class="jumbotron">
					                    <table class="table" width="100%" border="0">
					                    	<tr>
												<td><b>Sign In Count</b></td>
												<td>: <?=$obj_admin->sign_in_count?></td>
											</tr>
											<tr>
												<td><b>Enabled</b></td>
												<td colspan="2">: <?=($obj_admin->enabled=='Y')?'Yes':'No'?></td>
											</tr>
											<tr>
												<td><b>Created On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_admin->created_on))?></td>
											</tr>
											<tr>
												<td><b>Updated On</b></td>
												<td>: <?=date('jS F, Y g:s A',strtotime($obj_admin->updated_on))?></td>
											</tr>
					                    </table>
				                    </div>
		      					</div>
		      				</div>
						</div>
	                </div>
	                
		        </div>
	      	</div>
	    </div>
		<?php endif; ?>
  	</div>
</div>
<div class="clearfix"></div>

