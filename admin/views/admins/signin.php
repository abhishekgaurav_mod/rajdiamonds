<div>
	<a class="hiddenanchor" id="signup"></a>
	<a class="hiddenanchor" id="signin"></a>

  	<div class="login_wrapper">
	    <div class="animate form login_form">
	    	<div class="loginFromLogo"></div>
	      	<section class="forms addslider login_content">
		        <form name="frmaddslider" id="frmaddslider" action="<?=_URL?>admins/signin" method="post">
					<h1>Admin Login</h1>
					<div>
						<input name="useremail" type="text" id="useremail" placeholder="Email" maxlength="50" class="form-control" />
					</div>
					<div>
						<input name="userpassword" type="password" id="userpassword" placeholder="Password" maxlength="20" class="form-control" />
					</div>
					<div class="field">
						<span class="loading">Please wait...</span>
						<button type="submit" name="signin" value="Sign In" class="btn btn-default submit" >Sign In</button>
					</div>

					<div class="clearfix"></div>

					<div class="separator">
						<div>
							<h1><i class="fa fa-home"></i> Raj Diamonds </h1>
							<p>&copy; <?=date('Y')?> All Rights Reserved. Raj Diamonds Admin Panel<br>Powered by <a href="http://www.motherofdesign.in" target="_blank">Mother of Design</a></p>
						</div>
					</div>
		        </form>
	      	</section>
	    </div>

  	</div>
</div>
