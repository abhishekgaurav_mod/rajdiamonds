<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>

	      	<!-- <div class="title_right">
		        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
		          	<div class="input-group">
			            <input type="text" class="form-control" placeholder="Search for...">
			            <span class="input-group-btn">
			            	<button class="btn btn-default" type="button">Go!</button>
			            </span>
		          	</div>
		        </div>
	      	</div> --> 
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-12 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2>Edit <?=ucfirst($this->formhead)?></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<div class="forms addcmscontent">

							<form name="frmaddcmscontent" id="frmaddcmscontent" data-parsley-validate action="<?=_URL?>cmscontents/save" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left input_mask">
								<input type="hidden" name="type" value="<?=$type?>" />

								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="title">Title:</label>
									<input name="title" type="text" id="title" placeholder="Title" value="<?=(!empty($object->title)?$object->title:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
		                        	<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="subtitle">Sub Title</label>
									<input name="subtitle" type="text" id="subtitle" placeholder="Sub Title" value="<?=(!empty($object->subtitle)?$object->subtitle:'')?>" maxlength="255" autocomplete="off" class="form-control has-feedback-left" />
		                        	<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								<div class="field col-md-12 col-sm-12 col-xs-12">
									<label>Description</label>
									<textarea name="description" id="description" rows="7" cols="50" placeholder="Research Text..."><?=(!empty($object->description)?$object->description:'')?></textarea>
								</div>
								<div class="clearfix"></div>
		                      	<div class="ln_solid"></div>
		                      	<div class="form-group">
									<div class="field col-md-9 col-sm-9 col-xs-12">
										<span class="loading">Please wait...</span>
										<button type="submit" name="save" value="Save" class="btn btn-success">Submit</button>
									</div>
		                      	</div>

		                    </form>

						</div>
		            
		          	</div>
		        </div>
	      	</div>



	      	<!-- <div class="col-md-6 col-xs-12">
		        <div class="x_panel">
					<div class="x_title">
						<h2>Form Basic Elements <small>different form elements</small></h2>                    
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
		            	<br />
		            
		          	</div>
		        </div>
	      	</div> -->

	    </div>
  	</div>
</div>
<div class="clearfix"></div>


<script type="text/javascript">

// CK EDITOR
var ckConfig = {
		allowedContent: true, resize_enabled:'false',width: '100%', height: '350px', resize_maxHeight:'320', resize_minHeight: '600',
		toolbar:[
			{ name:'clipboard', items:['Cut', 'Copy', 'Paste', '-', 'Undo', 'Redo'] },
			{ name:'document', items:['source'] },
			{ name:'paragraph', /*groups: ['list', 'indent', 'blocks', 'align', 'bidi'],*/ items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
			{ name:'basicstyles', items:['Bold','Italic','Underline','Strike', 'Scayt'] },
			'/',
	       	{ name:'insert', items: ['HorizontalRule', 'SpecialChar', 'PageBreak' , 'Image', /*'Link', 'Unlink',*/ 'Maximize', 'Table', 'Source'] },
			{ name:'styles', items : ['Styles', 'Format','FontSize'] }
		]
	};
CKEDITOR.config.forcePasteAsPlainText 		= true;
/*CKEDITOR.config.filebrowserImageBrowseUrl 	= '<?=_URL?>fileuploader/index?type=articlecontentimages';*/
CKEDITOR.replace('description',ckConfig);

</script>
