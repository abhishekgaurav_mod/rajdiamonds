<?php
	$functional	=	array(	'_TIME_ZONE' => 'Asia/Calcutta',
							'_TWITTER_CONSUMER_KEY'=>'',
							'_TWITTER_CONSUMER_SECRET'=>'',
							'_TWITTER_HANDLE' => 'RajDiamonds',
							'_FB_PAGE'=>'https://www.facebook.com/',
							'_FB_PAGE_NAME'=>'RajDiamonds',
							'_FACEBOOK_APP_ID'=>'',
							'_FACEBOOK_APP_SECRET'=>'',
							'_ERROR_REPORTING' => true,
							'_TITLE' => 'Raj Diamonds',
							'_FOOTER' => '&copy; 2017 Raj Diamonds.',
							'_IMAGE_LIMIT' => '200',
							'_DATA_LIMIT' => '15');

	$smtpsetting=	array(	'_SMTP_SERVER' => 'smtp.gmail.com',
							'_EMAIL_LOGIN' => 'abhishek@motherofdesign.in',
							'_EMAIL_PASSWORD' => '********',
							'_EMAIL_FROM_NAME' => 'Raj Diamonds',
							'_FROM_EMAIL' => 'abhishek@motherofdesign.in',
							'_EMAIL_REPLY_TO' => 'abhishek@motherofdesign.in',
							'_EMAIL_SMTPSECURE' => 'ssl',
							'_EMAIL_PORT' => '465');

	setdefined(array_merge($database, $userdefind, $functional, $smtpsetting));

	// ROOT  Settings
	define('_CONTROLLERS_ROOT', _ROOT.'controllers/');
	define('_LIBS_ROOT', _ROOT.'libs/');
	define('_MODEL_ROOT', _ROOT.'models/');
	define('_VIEWS_ROOT', _ROOT.'views/');
	define('_WEBROOT_ROOT',_ROOT.'webroot/');
	define('_PARTIAL_ROOT', _VIEWS_ROOT.'partial/');
	define('_EMAIL_TEMPLATE',_VIEWS_ROOT.'email_templates/');

	// URL Settings
	define('_WEBROOT',_URL.'webroot/');
	define('_IMAGES',_WEBROOT.'images/');
	define("_CSS",_WEBROOT.'css/');
	define("_JS",_WEBROOT.'js/');
	define('_PLUGINS',_WEBROOT.'plugins/');
	define('_THEME',_WEBROOT.'theme/');

	define('_ALLWOED_IMAGE_TYPES',serialize( array('image/jpeg','image/jpg','image/png','image/gif')));
	define('_ALLWOED_FILE_TYPES',serialize( array('application/pdf','application/msword')));

	//GALLERIES-IMAGES
	define('_GALLERY_THUMB_SIZE', serialize(array(127)));
    define('_GALLERY_ORGS',_MEDIA_ROOT.'galleries/');
	define('_GALLERY_THUMBS_ORGS',_MEDIA_ROOT.'galleries/thumb/');
    define('_GALLERY_ORG_URL',_MEDIA_URL.'galleries/');
	define('_GALLERY_THUMBS_ORG_URL',_MEDIA_URL.'galleries/thumb/');

	// SLIDER-IMAGE
    define('_SLIDER_ORGS',_MEDIA_ROOT.'sliders/');
    define('_SLIDER_ORG_URL',_MEDIA_URL.'sliders/');

	// PRODUCT-IMAGE
    define('_PRODUCT_ORGS',_MEDIA_ROOT.'products/');
    define('_PRODUCT_ORG_URL',_MEDIA_URL.'products/');

	// CATEGORY-IMAGE
    define('_CATEGORY_ORGS',_MEDIA_ROOT.'categories/');
    define('_CATEGORY_ORG_URL',_MEDIA_URL.'categories/');

	// TAG-IMAGE
    define('_TAG_ORGS',_MEDIA_ROOT.'tags/');
    define('_TAG_ORG_URL',_MEDIA_URL.'tags/');

	// SLIDER-IMAGE
    define('_NEWSLETTER_ORGS',_MEDIA_ROOT.'newsletters/');
    define('_NEWSLETTER_ORG_URL',_MEDIA_URL.'newsletters/');


	function setdefined($defines){
		if (!empty($defines))
		{
			foreach ($defines as $key=>$val)
			{	define($key, $val);	}
		}
	}
?>
