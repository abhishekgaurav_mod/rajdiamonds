<?php
class Admins extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}
    	
    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'administrators';
    	$this->pagetitle=	'administrators';
    	 
    	$ajaxSource	=	_URL.'admins/getadmins';
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'admins/index.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function getadmins()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}
		
		if (!empty($status))
		{
			$sTable			=	'admins';
			$sIndexColumn	=	'admin_id';
			$aColumns 		= 	array(	'admin_id','email','name','sign_in_count','current_sign_in_at','current_sign_in_ip',
										'created_on','updated_on','enabled','admin_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'').' AND admin_id!='.$_SESSION['adminId'];
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current	=	'administrators';
    	$this->pagetitle=	'administrators';
		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}
	
		if (!empty($id))
		{
			$admin		=	new Admin();
			$obj_admin	=	$admin->getAdminById($id);
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'admins/view.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function add()
	{
		$this->islogged();
		$admin		=	new Admin();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}
			
			$err        =   NULL;
			switch($action)	
			{
				case 'add'	:  	if ($err==NULL && !filter_var($email, FILTER_VALIDATE_EMAIL))
								{	$err    =   'Please enter valid email.';	}
								if ($err==NULL && empty($password))
								{	$err    =   'Please enter user password.';	}
								if ($err==NULL && empty($retypepassword))
								{	$err    =   'Please confirm user password.';	}
								if ($err==NULL && ($password!=$retypepassword))
								{	$err    =   'Password mismatch.';	}
								if ($err==NULL && empty($name))
								{	$err    =   'Please enter name.';	}
								if ($err==NULL && $admin->checkAdminEmail($email))
								{	$err    =   'Email already in use.';	}
								
								if ($err==NULL)
								{
									$obj_admin   =   (Object)'';
									$obj_admin->name		=   $name;
									$obj_admin->email  	 	=   $email;
									$obj_admin->salt_string	=   str_shuffle(uniqid().time());
									$obj_admin->password	=   $password;
									if ($err==NULL)
									{	
										$adminId     =   $admin->addAdmin($obj_admin);
										if (!empty($adminId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'admins/view?id='.$adminId)));	}
									}
									$err    =   ($err==NULL)?'Unable to add administrator.':$err;
								}
				break;
				case 'edit'	:	$obj_admin  =   (Object)'';
								if ($err==NULL && !filter_var($email, FILTER_VALIDATE_EMAIL))
								{	$err    =   'Please enter valid email.';	}
								if ($err==NULL && ($password!='') && ($retypepassword!=''))
								{
									if ($password!=$retypepassword)
									{	$err    =   'Password mismatch.';	}
									else
									{   
										$obj_admin->password   =    $password;
										$obj_admin->salt_string	=   str_shuffle(uniqid().time());
									}
								}
								if ($err==NULL && empty($name))
								{	$err    =   'Please enter name.';	}
								
                                if ($err==NULL)
                                {
                                	$obj    =   $admin->getAdminByEmail($email);
                                    if (!empty($obj))
                                    {
                                    	if ($obj->admin_id!=$adminId)
                                        {   $err    =   'Email already in use.';    }
									}        
								}
								if ($err==NULL)
                                {
                                	$obj_admin->admin_id  	=   $adminId;
                                	$obj_admin->name		=   $name;
                                	$obj_admin->email  	 	=   $email;
                                	
                                	if ($err==NULL)
                                	{ 
	                                    if ($admin->updateAdmin($obj_admin))
	                                    {
	                                    	if ($adminId==$_SESSION['adminId'])
	                                    	{
	                                    		$_SESSION['name']	=	$obj_admin->name;
	                                    		$_SESSION['email']	=	$obj_admin->email;
	                                    	}
		                                    exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'admins/view?id='.$adminId)));
	                                    }
                                	}
                                	$err    =   ($err==NULL)?'Unable to update administrator.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';
			$this->current		=	'administrators';
    		$this->pagetitle	=	'administrators';
    		$this->formhead		=	'administrator';
			if (!empty($id))
			{	$obj_admin   =   $admin->getAdminById($id);	}
			else
			{   $action     =   'add';	}
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'admins/add.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{	
			$admin	=	new Admin();
			if ($admin->deleteAdmin($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Admin '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.'))); 
	}
		
	public function signin()
	{
		$this->notlogged();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}
			
			$err		=	NULL;
			if (empty($useremail))
			{	$err		=	'Please enter email.';	}
			if ($err==NULL && !filter_var($useremail, FILTER_VALIDATE_EMAIL))
            {   $err    =   'Please enter valid email.';        }
			if ($err==NULL && empty($userpassword))
			{	$err		=	'Please enter password.';	}
			if ($err==NULL)
			{
				$admin		=	new Admin();
				$common		=	new Common();
					
				$obj_admin	=	$admin->getAdminByEmail($useremail);
				if (!empty($obj_admin))
				{
					$obj_admin	=	$admin->authenticateAdmin($useremail,$obj_admin->salt_string.$userpassword);
        			if (!empty($obj_admin))
        			{
        				$objAdmin =   (object) '';
                        $objAdmin->admin_id     		=  $obj_admin->admin_id;
                        $objAdmin->sign_in_count     	=  $obj_admin->sign_in_count+1;
                        $objAdmin->last_sign_in_at   	=  $obj_admin->current_sign_in_at;
                        $objAdmin->current_sign_in_ip	=  $common->get_client_ip();
                        $objAdmin->last_sign_in_ip   	=  $obj_admin->current_sign_in_ip;
                                        
        				$admin->updateAdminSignParam ($objAdmin);
        				$_SESSION['adminId']	=	$obj_admin->admin_id;
        				$_SESSION['name']		=	$obj_admin->name;
        				$_SESSION['email']		=	$obj_admin->email;
        				
        				exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL)));
        			}
				}
				$err    =   'Incorrect username or password.';
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'admins/signin.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function signout()
	{
		session_destroy();
    	header('Location: '._URL.'admins/signin');
	}
	
	public function changepassword()
	{
		$this->islogged();
		$this->current		=	'Change Password';
		$this->pagetitle	=	'Change Password';
		$this->formhead		=	'Change Password';
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= 	trim(strip_tags($val));	}
			
			$err	=	NULL;
			if (empty($newpassword))
			{	$err	=	'Please enter your new password.';	}
			if ($err==NULL && ($newpassword!=$confirmpassword))
			{	$err	=	'Password mis-match.';	}

			if ($err==NULL)
			{
				$admin		=	new Admin();
				if ($admin->changePassword(str_shuffle(uniqid().time()), $confirmpassword, $_SESSION['adminId']))
				{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Password updated successfully!!!')));	}
				else
				{	$err	=	'Error: while updating password.';	}
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'admins/change_password.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function sendmail(){
		$this->islogged(true);
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= $val;	}
			
			if (!empty($id)){
				$common		=	new Common();
				$admin		=	new Admin();
				$password	=	$common->randomPassword();

				$obj_admin	=	$admin->getAdminById($id);
				if (!empty($obj_admin)){
					if ($admin->changePassword(str_shuffle(uniqid().time()), $password, $obj_admin->admin_id))
					{
						$mailer		=	new Mailer();
						$mailBody	=	file_get_contents(_EMAIL_TEMPLATE.'create_user.inc');
						$mailBody	=	str_replace('{name}',$obj_admin->name,$mailBody);
						$mailBody	=	str_replace('{email}',$obj_admin->email,$mailBody);
						$mailBody	=	str_replace('{password}',$password,$mailBody);
						$mailBody	=	str_replace('{target}',_URL,$mailBody);
						$mailBody	=	str_replace('{footer}',_TITLE,$mailBody);
						$mailer->SendHTMLEMail($obj_admin->email, _CREATE_ACCOUNT, $mailBody);
						exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Login details being sent to '.$name.' on there registered email address.')));
					}
				}
			}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while sending login details!!!')));
	}
} 
?>