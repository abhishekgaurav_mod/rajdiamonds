<?php
class Tags extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}
    	
    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'tags';
    	$this->pagetitle=	'Tags';
    	 
    	$ajaxSource	=	_URL.'tags/gettags';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'tags/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function gettags()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}
		
		if (!empty($status))
		{
			$sTable			=	'tags';
			$sIndexColumn	=	'tag_id';
			$aColumns 		= 	array('tag_id','tag_name','featured_status','created_on','updated_on','enabled','tag_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}
	
	public function view()
	{
		$this->islogged();
		$this->current	=	'tags';
		$this->pagetitle=	'Tag';
		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}
	
		if (!empty($id))
		{
			$tag			=	new Tag();		
			$object		=	$tag->getTagById($id);
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'tags/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function add()
	{
		$this->islogged();
		$tag		=	new Tag();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{
				if(gettype($val)=='string')
				{	$$key	= trim(strip_tags($val));	}	
				else
				{	$$key	= $val;		}
			}
			
			$err        =   NULL;
			switch($action)	
			{
				case 'add'	:  	if ($err==NULL && empty($title))
								{	$err    =   'Please enter tag name.';	}
								if ($err==NULL && $tag->checkTagSlug(Common::slugify($title)) )
								{ 	$err    =   'Tag already in use.'; 	}
								if ($err==NULL && empty($featured_status))
								{	$err    =   'Please select featured status.';	}
								if ($err==NULL && (empty($_FILES['tag_thumb'])))
								{	$err	=	'Please upload tag thumbnail.'; }
								
								if ($err==NULL)
								{
									$obj_tag   =   (Object)'';
									$obj_tag->tag_name  		=   $title;
									$obj_tag->tag_description  	=   $tag_description;
									$obj_tag->featured_status  	=   $featured_status;
                  					$obj_tag->tag_slug        =   Common::slugify($title);
									$obj_tag->tag_thumb 		=   null;
									$obj_tag->desktop_banner 		=   null;
									$obj_tag->tab_banner 		=   null;
									$obj_tag->mobile_banner 		=   null;

									if (!empty($_FILES['tag_thumb'])){
										if (empty($_FILES['tag_thumb']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['tag_thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['tag_thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$tag_thumbName	=	$images->uploadimage($_FILES['tag_thumb'], $fileName, _TAG_ORGS.'thumb/');
											}
											if ($tag_thumbName!=NULL)
											{	$obj_tag->tag_thumb	=	$tag_thumbName;	}
											else
											{	$err	=	'Error: uploading tag thumb.';		}
										}
									}

									if (!empty($_FILES['desktop_banner'])){
										if (empty($_FILES['desktop_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['desktop_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['desktop_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$desktop_bannerName	=	$images->uploadimage($_FILES['desktop_banner'], $fileName, _TAG_ORGS.'desktop-banner/');
											}
											if ($desktop_bannerName!=NULL)
											{	$obj_tag->desktop_banner	=	$desktop_bannerName;	}
											else
											{	$err	=	'Error: uploading desktop banner.';		}
										}
									}

									if (!empty($_FILES['tab_banner'])){
										if (empty($_FILES['tab_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['tab_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['tab_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$tab_bannerName	=	$images->uploadimage($_FILES['tab_banner'], $fileName, _TAG_ORGS.'tab-banner/');
											}
											if ($tab_bannerName!=NULL)
											{	$obj_tag->tab_banner	=	$tab_bannerName;	}
											else
											{	$err	=	'Error: uploading tablet banner.';		}
										}
									}

									if (!empty($_FILES['mobile_banner'])){
										if (empty($_FILES['mobile_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['mobile_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['mobile_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$mobile_bannerName	=	$images->uploadimage($_FILES['mobile_banner'], $fileName, _TAG_ORGS.'mobile-banner/');
											}
											if ($mobile_bannerName!=NULL)
											{	$obj_tag->mobile_banner	=	$mobile_bannerName;	}
											else
											{	$err	=	'Error: uploading mobile banner.';		}
										}
									}
									
									if ($err==NULL)
									{	
										$tagId     =   $tag->addTag($obj_tag);
										if (!empty($tagId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'tags/view?id='.$tagId)));	}
									}
									$err    =   ($err==NULL)?'Unable to add tag.':$err;
								}
				break;
				case 'edit'	:	$obj_tag  =   (Object)'';
								if ($err==NULL && empty($title))
								{	$err    =   'Please enter tag name.';	}
								if ($err==NULL && $tag->checkOtherSimilarTagSlug($tagId, Common::slugify($title)) )
								{ 	$err    =   'Tag already in use.'; 	}
								if ($err==NULL && empty($featured_status))
								{	$err    =   'Please select featured status.';	}

								if (!empty($tagId)) {
									$obj_tagfile   =   $tag->getTagfilesById($tagId);
								}
								
								if ($err==NULL)
                                {
									$obj_tag   					=   (Object)'';
                                	$obj_tag->tag_id  		=   $tagId;
									$obj_tag->tag_name  		=   $title;
									$obj_tag->tag_description  	=   $tag_description;
									$obj_tag->featured_status  	=   $featured_status;
                  					$obj_tag->tag_slug   		=   Common::slugify($title);
									$obj_tag->tag_thumb 		=   $tag_thumbId;
									$obj_tag->desktop_banner 	=   $desktop_bannerId;
									$obj_tag->tab_banner 		=   $tab_bannerId;
									$obj_tag->mobile_banner 	=   $mobile_bannerId;

									if (!empty($_FILES['tag_thumb'])){
										if (empty($_FILES['tag_thumb']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['tag_thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['tag_thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$tag_thumbName	=	$images->uploadimage($_FILES['tag_thumb'], $fileName, _TAG_ORGS.'thumb/');
											}
											if ($tag_thumbName!=NULL) {
												$obj_tag->tag_thumb	=	$tag_thumbName;
												if(file_exists(_TAG_ORGS.'thumb/'.$obj_tagfile->tag_thumb)){
													unlink(_TAG_ORGS.'thumb/'.$obj_tagfile->tag_thumb);
												}
											}
											else
											{	$err	=	'Error: uploading tag thumb.';		}
										}
									}

									if (!empty($_FILES['desktop_banner'])){
										if (empty($_FILES['desktop_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['desktop_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['desktop_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$desktop_bannerName	=	$images->uploadimage($_FILES['desktop_banner'], $fileName, _TAG_ORGS.'desktop-banner/');
											}
											if ($desktop_bannerName!=NULL) {
												$obj_tag->desktop_banner	=	$desktop_bannerName;
												if(file_exists(_TAG_ORGS.'desktop-banner/'.$obj_tagfile->desktop_banner)){
													unlink(_TAG_ORGS.'desktop-banner/'.$obj_tagfile->desktop_banner);
												}
											}
											else
											{	$err	=	'Error: uploading desktop banner.';		}
										}
									}

									if (!empty($_FILES['tab_banner'])){
										if (empty($_FILES['tab_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['tab_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['tab_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$tab_bannerName	=	$images->uploadimage($_FILES['tab_banner'], $fileName, _TAG_ORGS.'tab-banner/');
											}
											if ($tab_bannerName!=NULL) {
												$obj_tag->tab_banner	=	$tab_bannerName;
												if(file_exists(_TAG_ORGS.'mobile-banner/'.$obj_tagfile->tab_banner)){
													unlink(_TAG_ORGS.'mobile-banner/'.$obj_tagfile->tab_banner);
												}
											}
											else
											{	$err	=	'Error: uploading tablet banner.';		}
										}
									}

									if (!empty($_FILES['mobile_banner'])){
										if (empty($_FILES['mobile_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['mobile_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['mobile_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$mobile_bannerName	=	$images->uploadimage($_FILES['mobile_banner'], $fileName, _TAG_ORGS.'mobile-banner/');
											}
											if ($mobile_bannerName!=NULL) {
												$obj_tag->mobile_banner	=	$mobile_bannerName;
												if(file_exists(_TAG_ORGS.'mobile-banner/'.$obj_tagfile->mobile_banner)){
													unlink(_TAG_ORGS.'mobile-banner/'.$obj_tagfile->mobile_banner);
												}
											}
											else
											{	$err	=	'Error: uploading mobile banner.';		}
										}
									}
									
                                	if ($err==NULL)
                                	{ 
	                                    if ($tag->updateTag($obj_tag))
	                                    {   exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'tags/view?id='.$tagId)));	}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update tag.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';
			$this->current		=	'tags';
			$this->pagetitle	=	'Tag';
			$this->formhead		=	'Tag';
			if (!empty($id))
			{	$obj_tag   =   $tag->getTagById($id);	}
			else
			{   $action     =   'add';	}
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'tags/add.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{	
			$tag	=	new Tag();
			if ($tag->deleteTag($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Tag '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.'))); 
	}

	public function getTagsList() {
	   	try{
		   	$this->islogged();
		   	foreach($_GET as $key=>$val)
		   	{ $$key   = $val; }

		   	$res    =   array();
		   	if (!empty($q)) {
		   		$tag		=	new Tag();
		   		$res     	=   $tag->getTagsWithfilter($q);
		   	}
		   	exit(json_encode($res));
	   	}catch (Exception $e){ $this->error_log($e); }
   	}
} 
?>