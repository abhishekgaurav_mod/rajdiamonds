<?php
class Base extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
		$this->current	=	'dashboard';
		$this->pagetitle=	'dashboard';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
} 
?>