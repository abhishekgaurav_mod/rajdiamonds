<?php
class Sliders extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}

    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'sliders';
    	$this->pagetitle=	'sliders';

    	$ajaxSource	=	_URL.'sliders/getsliders';
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'sliders/index.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}

	public function getsliders()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($status))
		{
			$sTable			=	'sliders';
			$sIndexColumn	=	'slider_id';
			$aColumns 		= 	array(	'slider_id','description','slider_order','created_on','updated_on','enabled','slider_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current	=	'sliders';
    	$this->pagetitle=	'sliders';
		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($id))
		{
			$slider		 =	new Slider();
			$obj_slider   =   $slider->getSliderById($id);;
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'sliders/view.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}

	public function add()
	{
		$this->islogged();
		$slider		=	new Slider();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= $val;		}

			$err        =   NULL;
			switch($action)
			{
				case 'add'	:
								if ($err==NULL && empty($_FILES['desktop_banner']))
								{	$err    =   'Please Upload desktop banner.';	}
								if ($err==NULL && empty($_FILES['tab_banner']))
								{	$err    =   'Please Upload tablet banner.';	}
								if ($err==NULL && empty($_FILES['mobile_banner']))
								{	$err    =   'Please Upload mobile banner.';	}

								if ($err==NULL)
								{
									$obj_slider   =   new stdClass();
									/*$obj_slider->imagepath		=	null;*/
									$obj_slider->desktop_banner 	=   null;
									$obj_slider->tab_banner 		=   null;
									$obj_slider->mobile_banner 		=   null;
									$obj_slider->slider_order		=   $slider_order;
									$obj_slider->description		=   stripslashes($description);


									/*if (!empty($_FILES['imagepath'])){
										if (empty($_FILES['imagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['imagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['imagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}

											$fileName	=	$images->uploadimage($_FILES['imagepath'], time().uniqid(), _SLIDER_ORGS);
											if ($fileName!=NULL)
											{	$obj_slider->imagepath	=	$fileName;		}
											else
											{	$err	=	'Error: uploading Image.';			}
										}
									}*/


									if (!empty($_FILES['desktop_banner'])){
										if (empty($_FILES['desktop_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['desktop_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['desktop_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = 'raj-diamonds-'.uniqid();
												$desktop_bannerName	=	$images->uploadimage($_FILES['desktop_banner'], $fileName, _SLIDER_ORGS.'desktop-banner/');
											}
											if ($desktop_bannerName!=NULL)
											{	$obj_slider->desktop_banner	=	$desktop_bannerName;	}
											else
											{	$err	=	'Error: uploading desktop banner.';		}
										}
									}

									if (!empty($_FILES['tab_banner'])){
										if (empty($_FILES['tab_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['tab_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['tab_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = 'raj-diamonds-'.uniqid();
												$tab_bannerName	=	$images->uploadimage($_FILES['tab_banner'], $fileName, _SLIDER_ORGS.'tab-banner/');
											}
											if ($tab_bannerName!=NULL)
											{	$obj_slider->tab_banner	=	$tab_bannerName;	}
											else
											{	$err	=	'Error: uploading tablet banner.';		}
										}
									}

									if (!empty($_FILES['mobile_banner'])){
										if (empty($_FILES['mobile_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['mobile_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['mobile_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = 'raj-diamonds-'.uniqid();
												$mobile_bannerName	=	$images->uploadimage($_FILES['mobile_banner'], $fileName, _SLIDER_ORGS.'mobile-banner/');
											}
											if ($mobile_bannerName!=NULL)
											{	$obj_slider->mobile_banner	=	$mobile_bannerName;	}
											else
											{	$err	=	'Error: uploading mobile banner.';		}
										}
									}

									if ($err==NULL)
									{
										$sliderId     =   $slider->addSlider($obj_slider);
										if (!empty($sliderId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'sliders/view?id='.$sliderId)));	}
									}
									$err    =   ($err==NULL)?'Unable to add Slider.':$err;
								}
				break;
				case 'edit'	:	$obj_admin  =   (Object)'';
								if (!empty($sliderId)) {
									$obj_sliderfile   =   $slider->getSliderfilesById($sliderId);
								}

								if ($err==NULL)
                                {
                                	$obj_slider   =   new stdClass();
									$obj_slider->slider_id	=   $sliderId;
									/*$obj_slider->imagepath		=	$imagename;*/
									$obj_slider->desktop_banner 	=   $desktop_bannerId;
									$obj_slider->tab_banner 		=   $tab_bannerId;
									$obj_slider->mobile_banner 		=   $mobile_bannerId;
									$obj_slider->slider_order		=   $slider_order;
									$obj_slider->description		=   stripslashes($description);


                                	/*if (!empty($_FILES['imagepath'])){
										if (empty($_FILES['imagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['imagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['imagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}

											$fileName	=	$images->uploadimage($_FILES['imagepath'], time().uniqid(), _SLIDER_ORGS);
											if ($fileName!=NULL)
											{	$obj_slider->imagepath	=	$fileName;		}
											else
											{	$err	=	'Error: uploading Image.';			}
										}
									}*/


									if (!empty($_FILES['desktop_banner'])){
										if (empty($_FILES['desktop_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['desktop_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['desktop_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = 'raj-diamonds-'.uniqid();
												$desktop_bannerName	=	$images->uploadimage($_FILES['desktop_banner'], $fileName, _SLIDER_ORGS.'desktop-banner/');
											}
											if ($desktop_bannerName!=NULL) {
												$obj_slider->desktop_banner	=	$desktop_bannerName;
												if(file_exists(_SLIDER_ORGS.'desktop-banner/'.$obj_sliderfile->desktop_banner)){
													unlink(_SLIDER_ORGS.'desktop-banner/'.$obj_sliderfile->desktop_banner);
												}
											}
											else
											{	$err	=	'Error: uploading desktop banner.';		}
										}
									}

									if (!empty($_FILES['tab_banner'])){
										if (empty($_FILES['tab_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['tab_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['tab_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = 'raj-diamonds-'.uniqid();
												$tab_bannerName	=	$images->uploadimage($_FILES['tab_banner'], $fileName, _SLIDER_ORGS.'tab-banner/');
											}
											if ($tab_bannerName!=NULL) {
												$obj_slider->tab_banner	=	$tab_bannerName;
												if(file_exists(_SLIDER_ORGS.'tab-banner/'.$obj_sliderfile->tab_banner)){
													unlink(_SLIDER_ORGS.'tab-banner/'.$obj_sliderfile->tab_banner);
												}
											}
											else
											{	$err	=	'Error: uploading tablet banner.';		}
										}
									}

									if (!empty($_FILES['mobile_banner'])){
										if (empty($_FILES['mobile_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['mobile_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['mobile_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = 'raj-diamonds-'.uniqid();
												$mobile_bannerName	=	$images->uploadimage($_FILES['mobile_banner'], $fileName, _SLIDER_ORGS.'mobile-banner/');
											}
											if ($mobile_bannerName!=NULL) {
												$obj_slider->mobile_banner	=	$mobile_bannerName;
												if(file_exists(_SLIDER_ORGS.'mobile-banner/'.$obj_sliderfile->mobile_banner)){
													unlink(_SLIDER_ORGS.'mobile-banner/'.$obj_sliderfile->mobile_banner);
												}
											}
											else
											{	$err	=	'Error: uploading mobile banner.';		}
										}
									}

                                	if ($err==NULL)
                                	{
	                                    if ($slider->updateSlider($obj_slider))
	                                    {  exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'sliders/view?id='.$sliderId)));	}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update Slider.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';
			$this->current		=	'sliders';
    		$this->pagetitle	=	'sliders';
    		$this->formhead		=	'slider';
			if (!empty($id))
			{	$obj_slider   =   $slider->getSliderById($id);	}
			else
			{   $action     =   'add';	}
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'sliders/add.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}

	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{
			$slider		=	new Slider();
			if ($slider->deleteSlider($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Slider '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.')));
	}

}
?>
