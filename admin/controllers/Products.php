<?php
class Products extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}

		$category	 	=	New Category();

    	if(empty($id))
		{	header('Location:'._URL.'categories/');	}

    	$status     	=   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
		$categoryId		=	$id;
		$obj_category   =   $category->getCategoryById($id);

    	$this->current	=	'categories';
    	$this->pagetitle=	'Products ('.$obj_category->category_name.')';

    	$ajaxSource	=	_URL.'products/getproducts';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'products/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function getproducts()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($status))
		{
			$sTable			=	'products';
			$sIndexColumn	=	'product_id';
			$aColumns 		= 	array('product_id','product_name','product_code','product_order','front_visible','created_on','updated_on','enabled','category_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'')." AND category_id = '".$categoryId."'";
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current	=	'categories';
		$this->pagetitle=	'Products';

		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($id)) {
			$product			=	new Product();
			$obj_product		=	$product->getProductById($id);
			$arr_tags 	   		=   $product->getProductTagMappingByProductId($id);
			if (!empty($arr_tags)) {
				$preTags   =   array();
				foreach ($arr_tags AS $key=>$obj_tag){
					array_push($preTags, $obj_tag->tag_name);
				}
			}
		}
		$categoryId					= (!empty($obj_product))?$obj_product->category_id:null;
		$categoryName				= (!empty($obj_product))?$obj_product->category_name:null;
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'products/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function add()
	{
		$this->islogged();

		$product			=	new Product();
		$category	 		=	New Category();

		$ajaxTagSource  	=   _URL.'tags/getTagsList';

		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val) {
				if(gettype($val)=='string')
				{	$$key	= trim(strip_tags($val));	}
				else
				{	$$key	= $val;		}
			}

			$description = 	($_POST['description']);

			$err        =   NULL;
			switch($action) {
				case 'add'	:  	if ($err==NULL && empty($title))
								{	$err    =   'Please enter product.';	}
								if ($err==NULL && empty($product_code))
								{	$err    =   'Please select product code.';	}
								if ($err==NULL && empty($tags_string))
								{	$err    =   'Please select product tags.';	}

								if ($err==NULL)
								{
									$obj_product   =   (Object)'';
									$obj_product->category_id  	=   $categoryId;
									$obj_product->product_name  =   $title;
									$obj_product->product_code  =   $product_code;
									$obj_product->bar_code  	=   $bar_code;
									$obj_product->description  	=   $description;
									$obj_product->product_order =   $product_order;
									$obj_product->front_visible =   $front_visible;
									$obj_product->product_slug  =   Common::slugify($product_code).'-'.Common::slugify($title);
									$obj_product->tags_string  	=   $tags_string;
									$obj_product->product_thumb =   null;

									if (!empty($_FILES['product_thumb'])){
										if (empty($_FILES['product_thumb']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['product_thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['product_thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$product_thumbName	=	$images->uploadimage($_FILES['product_thumb'], $fileName, _PRODUCT_ORGS);
											}
											if ($product_thumbName!=NULL)
											{	$obj_product->product_thumb	=	$product_thumbName;	}
											else
											{	$err	=	'Error: uploading product thumb.';		}
										}
									}

									if ($err==NULL)
									{
										$productId     =   $product->addProduct($obj_product);
										if ($err==NULL) {
											if(!empty($tags_string)){
												$tag_id_array = explode(',',$tags_string);
												$obj_producttagmapping = new stdClass();
												$obj_producttagmapping->product_id = $productId;
												foreach($tag_id_array AS $key=>$tag_id){
													$obj_producttagmapping->tag_id = $tag_id;
													if(!$product->addProductTagMapping($obj_producttagmapping)){
														exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Adding product tag mapping was not successful')));
													}
												}
											}
										}
										if (!empty($categoryId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'products/view?id='.$productId)));	}
									}
									$err    =   ($err==NULL)?'Error: Unable to add Product.':$err;
								}
				break;
				case 'edit'	:	$obj_product  =   (Object)'';
								if ($err==NULL && empty($title))
								{	$err    =   'Please enter product.';	}
								if ($err==NULL && empty($product_code))
								{	$err    =   'Please select product code.';	}
								if ($err==NULL && empty($tags_string))
								{	$err    =   'Please select product tags.';	}

								if (!empty($productId)) {
									$obj_productfile   =   $product->getProductfilesById($productId);
									$obj_producttagstring   =   $product->getProductTagStringById($productId);
								}

								if ($err==NULL)
                                {
									$obj_product   =   (Object)'';
                                	$obj_product->product_id 	=   $productId;
									$obj_product->product_name  =   $title;
									$obj_product->product_code  =   $product_code;
									$obj_product->bar_code  	=   $bar_code;
									$obj_product->description  	=   $description;
									$obj_product->product_order  =   $product_order;
									$obj_product->front_visible =   $front_visible;
									$obj_product->product_slug  =   Common::slugify($product_code).'-'.Common::slugify($title);
									$obj_product->tags_string  	=   $tags_string;
									$obj_product->product_thumb =   $product_thumbId;

									if (!empty($_FILES['product_thumb'])){
										if (empty($_FILES['product_thumb']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['product_thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['product_thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$product_thumbName	=	$images->uploadimage($_FILES['product_thumb'], $fileName, _PRODUCT_ORGS);
											}
											if ($product_thumbName!=NULL) {
												$obj_product->product_thumb	=	$product_thumbName;
												if(file_exists(_PRODUCT_ORGS.$obj_productfile->product_thumb)){
													unlink(_PRODUCT_ORGS.$obj_productfile->product_thumb);
												}
											}
											else
											{	$err	=	'Error: uploading product thumb.';		}
										}
									}

                                	if ($err==NULL)
                                	{
	                                    if ($product->updateProduct($obj_product)) {
											if ($err==NULL && ($tags_string != $obj_producttagstring->tags_string)) {
												$product->deleteProductTagMappingByProductId($productId);

												if(!empty($tags_string)){
													$tag_id_array = explode(',',$tags_string);
													$obj_producttagmapping = new stdClass();
													$obj_producttagmapping->product_id = $productId;
													foreach($tag_id_array AS $key=>$tag_id){
														$obj_producttagmapping->tag_id = $tag_id;
														if(!$product->addProductTagMapping($obj_producttagmapping)){
															exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: Adding product tag mapping was not successful')));
														}
													}
												}
											}										
	                                    	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'products/view?id='.$productId)));
	                                    }
                                	}
                                	$err    =   ($err==NULL)?'Unable to update Product.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';

			if(empty($categoryId))
			{	header('Location:'._URL.'categories/');	}

			$obj_category   =   $category->getCategoryById($categoryId);

			$this->current		=	'categories';
			$this->pagetitle	=	'Product of ('.$obj_category->category_name.')';
			$this->formhead		=	'Products';

			if (!empty($productId)) {
				$obj_product   =   $product->getProductById($productId);
				$arr_tags 	   =   $product->getProductTagMappingByProductId($productId);
				if (!empty($arr_tags)) {
					$preTags   =   array();
					foreach ($arr_tags AS $key=>$obj_tag){
						array_push($preTags, array('id'=>$obj_tag->tag_id,'name'=>$obj_tag->tag_name));
					}
				}
			}
			else
			{   $action     =   'add';	}
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'products/add.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{
			$product		=	new Product();
			if ($product->deleteProduct($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Product '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.')));
	}
}
?>
