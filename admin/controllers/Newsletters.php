<?php
class Newsletters extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}

    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'newsletters';
    	$this->pagetitle=	'newsletters';

    	$ajaxSource	=	_URL.'newsletters/getnewsletters';
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'newsletters/index.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}

	public function getnewsletters()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($status))
		{
			$sTable			=	'newsletters';
			$sIndexColumn	=	'newsletter_id';
			$aColumns 		= 	array('newsletter_id','subject','send_date','subscriber_count','send_count','created_on','enabled','newsletter_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current	=	'newsletters';
    	$this->pagetitle=	'newsletters';
		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}

		if (!empty($id))
		{
			$newsletter		 =	new Newsletter();
			$obj_newsletter   =   $newsletter->getNewsletterById($id);;
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'newsletters/view.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}

	public function add()
	{
		$this->islogged();
		$newsletter		=	new Newsletter();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= $val;		}

			$err        =   NULL;
			switch($action)
			{
				case 'add'	:
								if ($err==NULL && empty($subject))
								{	$err    =   'Please enter Newsletter Subject.';	}
								if ($err==NULL && empty($_FILES['imagepath']))
								{	$err    =   'Please Upload Image.';	}

								if ($err==NULL)
								{
									$obj_newsletter   =   new stdClass();
									$obj_newsletter->subject		=	$subject;
									$obj_newsletter->imagepath		=	null;


									if (!empty($_FILES['imagepath'])){
										if (empty($_FILES['imagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['imagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['imagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}

											if ($err==NULL) {
												$fileName 		= 	Common::slugify($subject).'-'.uniqid();
												$obj_newsletter	=	$images->uploadimage($_FILES['imagepath'], $fileName, _NEWSLETTER_ORGS);
											}
											else
											{	$err	=	'Error: uploading Image.';	}
										}
									}

									if ($err==NULL)
									{
										$newsletterId     =   $newsletter->addNewsletter($obj_newsletter);
										if (!empty($newsletterId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'newsletters/view?id='.$newsletterId)));	}
									}
									$err    =   ($err==NULL)?'Unable to add Newsletter.':$err;
								}
				break;
				case 'edit'	:	$obj_admin  =   (Object)'';

								if ($err==NULL && empty($subject))
								{	$err    =   'Please enter Newsletter Subject.';	}


								if ($err==NULL)
                                {
                                	$obj_newsletter   =   new stdClass();
									$obj_newsletter->subject		=	$subject;
									$obj_newsletter->newsletter_id	=   $newsletterId;
									$obj_newsletter->imagepath		=	$imagename;

									if (!empty($newsletterId)) {
										$obj_newsletterfile   =   $newsletter->getNewsletterfilesById($newsletterId);
									}

                                	if (!empty($_FILES['imagepath'])){
										if (empty($_FILES['imagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['imagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['imagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}

											if ($err==NULL) {
												$fileName 				= Common::slugify($subject).'-'.uniqid();
												$newsletter_imageName	=	$images->uploadimage($_FILES['imagepath'], $fileName, _NEWSLETTER_ORGS);
											}
											if ($newsletter_imageName!=NULL) {
												$obj_newsletter->imagepath	=	$newsletter_imageName;
												if(file_exists(_NEWSLETTER_ORGS.$obj_newsletterfile->imagepath)){
													unlink(_NEWSLETTER_ORGS.$obj_newsletterfile->imagepath);
												}
											}
											else
											{	$err	=	'Error: uploading Image.';	}

										}
									}
                                	if ($err==NULL)
                                	{
	                                    if ($newsletter->updateNewsletter($obj_newsletter))
	                                    {  exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'newsletters/view?id='.$newsletterId)));	}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update Newsletter.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';
			$this->current		=	'newsletters';
    		$this->pagetitle	=	'newsletters';
    		$this->formhead		=	'newsletter';
			if (!empty($id))
			{	$obj_newsletter   =   $newsletter->getNewsletterById($id);	}
			else
			{   $action     =   'add';	}
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'newsletters/add.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}

	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{
			$newsletter		=	new Newsletter();
			if ($newsletter->deleteNewsletter($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Newsletter '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.')));
	}

}
?>
