<?php
class Cmscontents extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{
		$this->islogged();
		foreach($_GET as $key=>$val)
		{	$$key	= $val;		}
		$cms	=	new Cmscontent();

		if (!empty($type))
		{
			switch (strtolower($type))
			{

				case 'home'					:	$this->formhead		=	'Home Page';
												$this->pagetitle	=	'Home Page';
												$this->current		=	'home';
				break;
				case 'contact'				:	$this->formhead		=	'Office Address';
												$this->pagetitle	=	'Office Address';
												$this->current		=	'contact';
				break;
				case 'aboutus'				:	$this->formhead		=	'About Us';
												$this->pagetitle	=	'About Us';
												$this->current		=	'aboutus';
				break;
				case 'stagesofiasexam'		:	$this->formhead		=	'Stages of IAS Exam';
												$this->pagetitle	=	'Stages of IAS Exam';
												$this->current		=	'stagesofiasexam';
				break;
				case 'generalstudiesprelims':	$this->formhead		=	'General Studies Prelims';
												$this->pagetitle	=	'General Studies Prelims';
												$this->current		=	'generalstudiesprelims';
				break;
				case 'generalstudiesmains'	:	$this->formhead		=	'General Studies Mains';
												$this->pagetitle	=	'General Studies Mains';
												$this->current		=	'generalstudiesmains';
				break;
				case 'iasexameligibility'	:	$this->formhead		=	'IAS Exam Eligibility';
												$this->pagetitle	=	'IAS Exam Eligibility';
												$this->current		=	'iasexameligibility';
				break;
				case 'iasgeneralinstructions':	$this->formhead		=	'IAS General Instructions';
												$this->pagetitle	=	'IAS General Instructions';
												$this->current		=	'iasgeneralinstructions';
				break;
				case 'iasreservationissues'	:	$this->formhead		=	'IAS Reservation Issues';
												$this->pagetitle	=	'IAS Reservation Issues';
												$this->current		=	'iasreservationissues';
				break;
				case 'iasphcategory'		:	$this->formhead		=	'IAS PH Category';
												$this->pagetitle	=	'IAS PH Category';
												$this->current		=	'iasphcategory';
				break;
				case 'iasserviceallocation'	:	$this->formhead		=	'IAS Service Allocation';
												$this->pagetitle	=	'IAS Service Allocation';
												$this->current		=	'iasserviceallocation';
				break;
				case 'iasmedicalexamofcandidates':	$this->formhead	=	'IAS Medical Exam of Candidates';
													$this->pagetitle	=	'IAS Medical Exam of Candidates';
													$this->current		=	'iasmedicalexamofcandidates';
				break;
				case 'whyhimalai'			:	$this->formhead		=	'Why Himalai';
												$this->pagetitle	=	'Why Himalai';
												$this->current		=	'whyhimalai';
				break;
				case 'iasexam'				:	$this->formhead		=	'IAS Exam';
												$this->pagetitle	=	'IAS Exam';
												$this->current		=	'iasexam';
				break;
				case 'kasexam'				:	$this->formhead		=	'KAS Exam';
												$this->pagetitle	=	'KAS Exam';
												$this->current		=	'kasexam';
				break;
				case 'kaseligibility'		:	$this->formhead		=	'KAS Eligibility';
												$this->pagetitle	=	'KAS Eligibility';
												$this->current		=	'kaseligibility';
				break;
				case 'stagesofkasexam'		:	$this->formhead		=	'Stages of KAS Exam';
												$this->pagetitle	=	'Stages of KAS Exam';
												$this->current		=	'stagesofkasexam';
				break;
				case 'icseeligibility'		:	$this->formhead		=	'ICSE Eligibility';
												$this->pagetitle	=	'ICSE Eligibility';
												$this->current		=	'icseeligibility';
				break;
				case 'icsestagesandpattern'	:	$this->formhead		=	'ICSE Stages and Pattern';
												$this->pagetitle	=	'ICSE Stages and Pattern';
												$this->current		=	'icsestagesandpattern';
				break;
				case 'icsesyllabus'			:	$this->formhead		=	'ICSE Syllabus';
												$this->pagetitle	=	'ICSE Syllabus';
												$this->current		=	'icsesyllabus';
				break;
		        default:
												$this->formhead		=	$type;
												$this->pagetitle	=	$type;
												$this->current		=	$type;
		        break;

			}
			$object   =   $cms->getCmscontentByType($type);
			include_once _VIEWS_ROOT.'partial/admin_header.php';
			include_once _VIEWS_ROOT.'cmscontents/cmscontent.php';
			include_once _VIEWS_ROOT.'partial/admin_footer.php';
		}
	}

	public function save()
	{
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= $val;	}

			if (!empty($type)) {
				if (!in_array($type, array('home','contact','aboutus','directions','services','facilities')))
				{	$err	=	'Invalid request.';	}
			}
			$err	=   NULL;
			if ($err==NULL && empty($title))
			{	$err    =   'Please enter  title.';	}
			if ($err==NULL && empty($description))
			{	$err    =   'Please enter description.';	}

			if ($err==NULL)
			{
				$object					=   new stdClass();
				$object->title  		=   $title;
				$object->subtitle  		=   $subtitle;
				$object->type			=   $type;
				$object->description	=  	$description;


				if ($err==NULL)
				{
					$cms	=	new Cmscontent();
					if ($cms->updateCmscontent($object))
					{
						if(!empty($other))
						{
							$object->type			=   'others';
							$object->description	=  	$other;
							$cms->updateCmscontent($object);
						}
						exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'cmscontents/index?type='.strtolower($type))));
					}
				}
				$err    =   ($err==NULL)?'Unable to update infrastructure import.':$err;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
	}
}
?>
