<?php
class Enquiries extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}
    	
    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'contact';
    	$this->pagetitle=	'enquiries';
    	 
    	$ajaxSource	=	_URL.'enquiries/getenquiries';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'enquiries/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function getenquiries()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}
		
		if (!empty($status))
		{
			$sTable			=	'enquiries';
			$sIndexColumn	=	'enquiry_id';
			$aColumns 		= 	array(	'enquiry_id','first_name','phone','email','interest','created_on','updated_on','enabled','enquiry_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}

	public function view()
	{
		$this->islogged();
		$this->current	=	'contact';
    	$this->pagetitle=	'view enquiry';
		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}
	
		if (!empty($enquiryId))
		{
			$enquiry		=	new Enquiry();
			$obj_enquiry	=	$enquiry->getEnquiryById($enquiryId);
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'enquiries/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}

	public function sendmail()
	{
		$this->islogged();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= $val;	}
			
				$err        =   NULL;
			switch($action)	
			{
				case 'enquiry'	:   if ($err==NULL && empty($response))
								{	$err    =   'Please enter description.';	}
								
								if ($err==NULL)
								{
									$respond			=   str_replace('<br />', PHP_EOL, $response);
									
									$mailer		=	new Mailer();
									$mailBody	=	file_get_contents(_EMAIL_TEMPLATE.'enquiry_reply.inc');
									$mailBody	=	str_replace('{name}',$name,$mailBody);
									$mailBody	=	str_replace('{response}',$respond,$mailBody);
									$mailBody	=	str_replace('{footer}',_TITLE,$mailBody);
									$mailer->SendHTMLEMail($email, _ENQUIRY_RESPOND, $mailBody);
									exit(json_encode(array('status'=>true, 'message'=>'Email send successfully', 'redirect'=>true, 'url'=>_URL.'enquiries/view?enquiryId='.$enquiryId)));	;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
	}

	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{	
			$enquiry	=	new Enquiry();
			if ($enquiry->deleteEnquiry($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Enquiry '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.'))); 
	}
} 
?>