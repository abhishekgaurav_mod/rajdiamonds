<?php
class Galleries extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
		$gallery		=	new Gallery();
		
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}
				
    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'galleries';
		
		$arr_gallery	 =	$gallery->getImages(0,8);
		$this->pagetitle =	'Gallery Images';
		
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'galleries/index.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function add()
	{
		$this->islogged();
		$gallery		=	new Gallery();
		
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}
			
			$err        =   NULL;
			switch($action)	
			{
				case 'add'	:  	if ($err==NULL && (empty($_FILES['imagepath'])))
								{	$err	=	'Please upload Image.';		}
								
								if ($err==NULL)
								{
									$obj_image   =   (Object)'';
									$obj_image->title	=	$title;
									
									if (!empty($_FILES['imagepath'])){
										if (empty($_FILES['imagepath']['error']))
										{
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['imagepath']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['imagepath']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';		}
												
											$fileName	=	$images->uploadimage($_FILES['imagepath'], Common::slugify($title).'-'.time().uniqid(), _GALLERY_ORGS);											
											chmod(_GALLERY_ORGS.$fileName,0777);
											if ($fileName!=NULL)
											{
												/*$thumbsizes	=   unserialize(_GALLERY_THUMB_SIZE);
												if (!empty($thumbsizes)){
													foreach ($thumbsizes as $size){
														$images->createthumnail(_GALLERY_ORGS, $fileName, _GALLERY_THUMBS_ORGS, 'ti_'.$size.'_'.$fileName, $size);
													}
												}*/
												$obj_image->imagepath	=	$fileName;
											}
											else
											{	$err	=	'Error: uploading Newsevent picture.';	}
										}
									}
									if ($err==NULL)
									{	
										$imageId     =   $gallery->addImage($obj_image);
										if (!empty($imageId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'galleries/index')));	}
									}
									$err    =   ($err==NULL)?'Unable to add Images.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     		=   'add';
			
			$this->current		=	'galleries';
			$this->pagetitle 	=	'Gallery Images';
			$this->formhead		=	'image';
		}
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'galleries/add.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}
		if (!empty($id))
		{	
			$gallery	=	new Gallery();
			$obj_image = $gallery->getImageById($id);
			if ($gallery->deleteImage($id,$status)) {
				unlink(_GALLERY_ORGS.$obj_image->imagepath);
				exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Image '.(($status=='N')?"disabled":"enabled").' sccuessfully.', 'url'=>_URL.'galleries/index')));
			}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.'))); 
	}
	public function paginate()
	{
		$this->islogged();
		$gallery	=	new Gallery();
		
		foreach($_GET as $key=>$val)
		{	$$key	= $val;	}
	
		$err	=	NULL;
		if(!empty($type))
		{
			switch($type)
			{
				case 'image' : 	
								$count			=	$gallery->getImagesCount();
								$start			=	0;
								$end			=	_IMAGE_LIMIT;
								if ($count>0)
								{
									$max_page	=	ceil(($count)/_IMAGE_LIMIT);
									$page		=	($page<0)?0:$page;
									$start		=	($page-1)*_IMAGE_LIMIT;
															
									$next		=	($page<$max_page)?'<a href="javascript:void(0);" onclick="javascript:getimages('.($page+1).')">Next</a>':'Next';
									$prev		=	($page>1)?'<a href="javascript:void(0);" onclick="javascript:getimages('.($page-1).')">Previous</a>':'Previous';
												
									$arr_images	=	$gallery->getImages($start,$end);
									$rows		=	NULL;
									if (!empty($arr_images))
									{
										foreach($arr_images as $key => $obj_image)
										{
											$rows	.= '<div class="col-md-55">
												            <div class="thumbnail">
												              	<div class="image view view-first">
													                <img style="width: 100%; display: block;" src="'._GALLERY_ORG_URL.$obj_image->imagepath.'" alt="Gallery Image '.($key+1).'" />
													                <div class="mask">
													                  	<p>Image '.($key+1).'</p>
													                  	<div class="tools tools-bottom">
														                    <a href="'._GALLERY_ORG_URL.$obj_image->imagepath.'" rel="imagebox"><i class="fa fa-link"></i></a>
														                    <a href="#" onclick="javascript:deleters(\'Gallery Image\', \''.$obj_image->image_id.'\', \'N\', \'Delete\');"><i class="fa fa-times"></i></a>
													                  	</div>
													                </div>
												              	</div>
												              	<div class="caption">
												                	<p>'.$obj_image->title.'</p>
												              	</div>
												            </div>
											          	</div>';
										}
									}
									echo '	<div class="backpaginate">
												<ul>
													<li>'.$prev.'</li>
													<li>'.$next.'</li>
												</ul>
											</div>
											<div class="backimages">'.$rows.'</div>';
								}
								else
								{	echo '<div class="backimages textalignCenter">No Images Added.</div>';	}
								exit();
					break;
					
				default: exit('');
			}
		}
	}
}
?>