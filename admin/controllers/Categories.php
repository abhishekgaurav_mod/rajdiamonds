<?php
class Categories extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
	    foreach($_GET as $key=>$val)
    	{	$$key   = $val;	}
    	
    	$status     =   (isset($status))?(($status=='n')?strtoupper($status):'Y'):'Y';
    	$this->current	=	'categories';
    	$this->pagetitle=	'Categories';
    	 
    	$ajaxSource	=	_URL.'categories/getcategories';
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'categories/index.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function getcategories()
	{
		$this->islogged();
		foreach($_POST as $key=>$val)
		{	$$key   = $val;	}
		
		if (!empty($status))
		{
			$sTable			=	'categories';
			$sIndexColumn	=	'category_id';
			$aColumns 		= 	array('category_id','category_name','featured_status','category_order','created_on','updated_on','enabled','category_id');
			$condition		=	((isset($status))?" enabled = '".$status."'":'');
			$datatable		=	new DataTable($sTable, $aColumns, $sIndexColumn, $condition);
			$datatable->output();
		}
	}
	
	public function view()
	{
		$this->islogged();
		$this->current	=	'categories';
		$this->pagetitle=	'Category';
		foreach($_GET as $key=>$val)
		{	$$key   = $val;	}
	
		if (!empty($id))
		{
			$category			=	new Category();		
			$obj_category		=	$category->getCategoryById($id);
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'categories/view.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function add()
	{
		$this->islogged();
		$category		=	new Category();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{
				if(gettype($val)=='string')
				{	$$key	= trim(strip_tags($val));	}	
				else
				{	$$key	= $val;		}
			}
			
			$err        =   NULL;
			switch($action)	
			{
				case 'add'	:  	if ($err==NULL && empty($title))
								{	$err    =   'Please enter category name.';	}
								if ($err==NULL && $category->checkCategorySlug(Common::slugify($title)) )
								{ 	$err    =   'Category already in use.'; 	}
								if ($err==NULL && empty($featured_status))
								{	$err    =   'Please select featured status.';	}
								if ($err==NULL && (empty($_FILES['category_thumb'])))
								{	$err	=	'Please upload category thumbnail.'; }
								
								if ($err==NULL)
								{
									$obj_category   =   (Object)'';
									$obj_category->category_name  		=   $title;
									$obj_category->category_description =   $category_description;
									$obj_category->featured_status  	=   $featured_status;
									$obj_category->category_order		=   $category_order;
                  					$obj_category->category_slug        =   Common::slugify($title);
									$obj_category->category_thumb 		=   null;
									$obj_category->desktop_banner 		=   null;
									$obj_category->tab_banner 			=   null;
									$obj_category->mobile_banner 		=   null;

									if (!empty($_FILES['category_thumb'])){
										if (empty($_FILES['category_thumb']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['category_thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['category_thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$category_thumbName	=	$images->uploadimage($_FILES['category_thumb'], $fileName, _CATEGORY_ORGS.'thumb/');
											}
											if ($category_thumbName!=NULL)
											{	$obj_category->category_thumb	=	$category_thumbName;	}
											else
											{	$err	=	'Error: uploading category thumb.';		}
										}
									}

									if (!empty($_FILES['desktop_banner'])){
										if (empty($_FILES['desktop_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['desktop_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['desktop_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$desktop_bannerName	=	$images->uploadimage($_FILES['desktop_banner'], $fileName, _CATEGORY_ORGS.'desktop-banner/');
											}
											if ($desktop_bannerName!=NULL)
											{	$obj_category->desktop_banner	=	$desktop_bannerName;	}
											else
											{	$err	=	'Error: uploading desktop banner.';		}
										}
									}

									if (!empty($_FILES['tab_banner'])){
										if (empty($_FILES['tab_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['tab_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['tab_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$tab_bannerName	=	$images->uploadimage($_FILES['tab_banner'], $fileName, _CATEGORY_ORGS.'tab-banner/');
											}
											if ($tab_bannerName!=NULL)
											{	$obj_category->tab_banner	=	$tab_bannerName;	}
											else
											{	$err	=	'Error: uploading tablet banner.';		}
										}
									}

									if (!empty($_FILES['mobile_banner'])){
										if (empty($_FILES['mobile_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['mobile_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['mobile_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$mobile_bannerName	=	$images->uploadimage($_FILES['mobile_banner'], $fileName, _CATEGORY_ORGS.'mobile-banner/');
											}
											if ($mobile_bannerName!=NULL)
											{	$obj_category->mobile_banner	=	$mobile_bannerName;	}
											else
											{	$err	=	'Error: uploading mobile banner.';		}
										}
									}
									
									if ($err==NULL)
									{	
										$categoryId     =   $category->addCategory($obj_category);
										if (!empty($categoryId))
										{	exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'categories/view?id='.$categoryId)));	}
									}
									$err    =   ($err==NULL)?'Unable to add category.':$err;
								}
				break;
				case 'edit'	:	$obj_category  =   (Object)'';
								if ($err==NULL && empty($title))
								{	$err    =   'Please enter category name.';	}
								if ($err==NULL && $category->checkOtherSimilarCategorySlug($categoryId, Common::slugify($title)) )
								{ 	$err    =   'Category already in use.'; 	}
								if ($err==NULL && empty($featured_status))
								{	$err    =   'Please select featured status.';	}

								if (!empty($categoryId)) {
									$obj_categoryfile   =   $category->getCategoryfilesById($categoryId);
								}
								
								if ($err==NULL)
                                {
									$obj_category   					=   (Object)'';
                                	$obj_category->category_id  		=   $categoryId;
									$obj_category->category_name  		=   $title;
									$obj_category->category_description =   $category_description;
									$obj_category->featured_status  	=   $featured_status;
									$obj_category->category_order		=   $category_order;
                  					$obj_category->category_slug   		=   Common::slugify($title);
									$obj_category->category_thumb 		=   $category_thumbId;
									$obj_category->desktop_banner 		=   $desktop_bannerId;
									$obj_category->tab_banner 			=   $tab_bannerId;
									$obj_category->mobile_banner 		=   $mobile_bannerId;

									if (!empty($_FILES['category_thumb'])){
										if (empty($_FILES['category_thumb']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['category_thumb']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['category_thumb']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$category_thumbName	=	$images->uploadimage($_FILES['category_thumb'], $fileName, _CATEGORY_ORGS.'thumb/');
											}
											if ($category_thumbName!=NULL) {
												$obj_category->category_thumb	=	$category_thumbName;
												if(file_exists(_CATEGORY_ORGS.'thumb/'.$obj_categoryfile->category_thumb)){
													unlink(_CATEGORY_ORGS.'thumb/'.$obj_categoryfile->category_thumb);
												}
											}
											else
											{	$err	=	'Error: uploading category thumb.';		}
										}
									}

									if (!empty($_FILES['desktop_banner'])){
										if (empty($_FILES['desktop_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['desktop_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['desktop_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$desktop_bannerName	=	$images->uploadimage($_FILES['desktop_banner'], $fileName, _CATEGORY_ORGS.'desktop-banner/');
											}
											if ($desktop_bannerName!=NULL) {
												$obj_category->desktop_banner	=	$desktop_bannerName;
												if(file_exists(_CATEGORY_ORGS.'desktop-banner/'.$obj_categoryfile->desktop_banner)){
													unlink(_CATEGORY_ORGS.'desktop-banner/'.$obj_categoryfile->desktop_banner);
												}
											}
											else
											{	$err	=	'Error: uploading desktop banner.';		}
										}
									}

									if (!empty($_FILES['tab_banner'])){
										if (empty($_FILES['tab_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['tab_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['tab_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$tab_bannerName	=	$images->uploadimage($_FILES['tab_banner'], $fileName, _CATEGORY_ORGS.'tab-banner/');
											}
											if ($tab_bannerName!=NULL) {
												$obj_category->tab_banner	=	$tab_bannerName;
												if(file_exists(_CATEGORY_ORGS.'tab-banner/'.$obj_categoryfile->tab_banner)){
													unlink(_CATEGORY_ORGS.'tab-banner/'.$obj_categoryfile->tab_banner);
												}
											}
											else
											{	$err	=	'Error: uploading tablet banner.';		}
										}
									}

									if (!empty($_FILES['mobile_banner'])){
										if (empty($_FILES['mobile_banner']['error'])) {
											$images		=   new Images();
											$file_error =   $images->uplaodImageError($_FILES['mobile_banner']['error']);
											if ($file_error!=NULL)
											{	$err    =   $file_error;	}
											$file_type_error    =   $images->validateImageType($_FILES['mobile_banner']['type'],unserialize(_ALLWOED_IMAGE_TYPES));
											if ($file_type_error!=NULL)
											{	$err	=   'Invalid File format.';	}
											if ($err==NULL) {
												$fileName = Common::slugify($title).'-'.uniqid();
												$mobile_bannerName	=	$images->uploadimage($_FILES['mobile_banner'], $fileName, _CATEGORY_ORGS.'mobile-banner/');
											}
											if ($mobile_bannerName!=NULL) {
												$obj_category->mobile_banner	=	$mobile_bannerName;
												if(file_exists(_CATEGORY_ORGS.'mobile-banner/'.$obj_categoryfile->mobile_banner)){
													unlink(_CATEGORY_ORGS.'mobile-banner/'.$obj_categoryfile->mobile_banner);
												}
											}
											else
											{	$err	=	'Error: uploading mobile banner.';		}
										}
									}
									
                                	if ($err==NULL)
                                	{ 
	                                    if ($category->updateCategory($obj_category))
	                                    {   exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'categories/view?id='.$categoryId)));	}
                                	}
                                	$err    =   ($err==NULL)?'Unable to update category.':$err;
								}
				break;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			foreach($_GET as $key=>$val)
			{	$$key   = $val;		}
			$action     =   (isset($action))?(($action=='edit')?$action:'add'):'add';
			$this->current		=	'categories';
			$this->pagetitle	=	'Category';
			$this->formhead		=	'Category';
			if (!empty($id))
			{	$obj_category   =   $category->getCategoryById($id);	}
			else
			{   $action     =   'add';	}
		}
		include_once _VIEWS_ROOT.'partial/admin_header.php';
		include_once _VIEWS_ROOT.'categories/add.php';
		include_once _VIEWS_ROOT.'partial/admin_footer.php';
	}
	
	public function delete()
	{
		$this->islogged(true);
		foreach($_POST as $key=>$val)
		{	$$key	= $val;		}

		if (!empty($id))
		{	
			$category	=	new Category();
			if ($category->deleteCategory($id,$status))
			{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Category '.(($status=='N')?"disabled":"enabled").' successfully.')));	}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while updating.'))); 
	}
} 
?>