<?php
class DataTable extends DBSource
{
	var $index			= array();
    var $sTable			= NULL;  
    var $aColumns       = NULL;  
    var $sIndexColumn   = NULL;
    var $method			= 'GET';
    var $data			= NULL;
    var $sLimit			= NULL; 
	var $sOrder			= NULL;
	var $sWhere			= NULL;
	var $rResult		= NULL;
	var $iFilteredTotal	= NULL;
	var $iTotal			= NULL;
	var $output			= NULL;
	var $condition		= NULL;
	var $outType		= 'json';
	
    public function __destruct()
	{	parent::__destruct();	}
    
    public function __construct($sTable, $aColumns, $sIndexColumn, $condition, $outType='json'){
    	parent::__construct();	
    	$this->sTable		=	$sTable;
    	$this->aColumns		=	$aColumns;
    	$this->sIndexColumn	=	$sIndexColumn;
    	$this->method		=	$_SERVER['REQUEST_METHOD'];
    	$this->data			=	($this->method=='GET')?$_GET:$_POST;
    	$this->condition	=	$condition;
    	$this->outType		=	$outType;
    	$this->setData();
    }

    public function __set($name, $value)
    {	$this->index[$name] = $value;	}
    
    public function setData()
    {
    	foreach($this->data as $key=>$val)
    	{	$this->__set($key, $val);	}
    	$this->data		=	(object) $this->index;
    	$this->index	=	array();
    }

    public function output()
    {
    	$this->setLimit();
    	$this->setOrder();
    	$this->setfilter();
    	$this->getdata();
    	$this->setFilteredDataLength();
    	$this->setDatalength();
    	$this->setOutput();
    	if ($this->outType=='json')
    	{	echo json_encode($this->output);	}
    	else
    	{	return $this->output;	}
    }
    
    public function setOutput()
    {
    	$this->output = array(	"sEcho" => intval($this->data->sEcho),
    			"iTotalRecords" => $this->iTotal,
    			"iTotalDisplayRecords" => $this->iFilteredTotal,
    			"aaData" => array());
    	while ( $aRow = $this->db_fetch_array( $this->rResult ))
    	{
    		$row = array();
    		for ( $i=0 ; $i<count($this->aColumns) ; $i++ )
    		{	$row[] = $aRow[ $this->aColumns[$i] ];	}
    		$this->output['aaData'][] = $row;
    	}
    }
    
    /* SQL queries Get data to display */
    public function getdata()
    {
    	$sQuery			=	"	SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $this->aColumns))."
    							FROM $this->sTable $this->sWhere $this->sOrder $this->sLimit";
    	$this->rResult	=	$this->db_query($sQuery);
    }
    
    /* Data set length after filtering */
    public function setFilteredDataLength()
    {
    	$sQuery					=	"SELECT FOUND_ROWS()";
    	$rResultFilterTotal		=	$this->db_query($sQuery);
    	$aResultFilterTotal		=	$this->db_fetch_array($rResultFilterTotal);
    	if(!empty($aResultFilterTotal))
    	{	$this->iFilteredTotal= 	$aResultFilterTotal[0];	}
    }
    
    /* Total data set length */
    public function setDatalength()
    {
    	$sQuery				=	"SELECT COUNT(".$this->sIndexColumn.") FROM $this->sTable";
    	$rResultTotal 		= 	$this->db_query($sQuery);
    	$aResultTotal 		= 	$this->db_fetch_array($rResultTotal);
    	if (!empty($aResultTotal))
    	{	$this->iTotal 	= 	$aResultTotal[0];	}
    }
    
    public function setLimit()
    {
    	if (isset($this->data->iDisplayStart) && $this->data->iDisplayLength!='-1')
    	{	$this->sLimit = "LIMIT ".$this->mysqlEscapeString($this->data->iDisplayStart).", ".$this->mysqlEscapeString($this->data->iDisplayLength);	}
    }

    public function setOrder()
    {
    	
    	if ( isset($this->data->iSortCol_0))
		{
			$this->sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval($this->data->iSortingCols); $i++ )
			{
				$iSortCol_	=	'iSortCol_'.$i;
				$sSortDir_	=	'sSortDir_'.$i;
				$bSortable_	=	'bSortable_'.intval($this->data->$iSortCol_);
				
				if ( $this->data->$bSortable_ == "true" )
				{	$this->sOrder .= $this->aColumns[ intval($this->data->$iSortCol_) ]." ".$this->mysqlEscapeString($this->data->$sSortDir_) .", ";	}
			}
			$this->sOrder = substr_replace( $this->sOrder, "", -2 );
			if ( $this->sOrder == "ORDER BY" )
			{	$this->sOrder = "";	}
		}
	}
	
	public function setfilter()
	{
		$this->sWhere = "";
		if ( $this->data->sSearch != "" )
		{
			$this->sWhere = "WHERE (";
			for ( $i=0 ; $i<count($this->aColumns) ; $i++ )
			{	$this->sWhere .= $this->aColumns[$i]." LIKE '%".$this->mysqlEscapeString($this->data->sSearch)."%' OR ";	}
			$this->sWhere = substr_replace( $this->sWhere, "", -3 );
			$this->sWhere .= ')';
		}
		
		/* Individual column filtering */
		for ( $i=0 ; $i<count($this->aColumns) ; $i++ )
		{
			$bSearchable_	=	'bSearchable_'.$i;
			$sSearch_		=	'sSearch_'.$i;
			if ($this->data->$bSearchable_ == "true" && $this->data->$sSearch_ != '' )
			{
				if ( $this->sWhere == "" )
				{	$this->sWhere = "WHERE ";	}
				else
				{	$this->sWhere .= " AND ";	}
				$this->sWhere .= $this->aColumns[$i]." LIKE '%".$this->mysqlEscapeString($this->data->$sSearch_)."%' ";
			}
		}

		if ($this->condition!='')
		{
			if ($this->sWhere=='')
			{	$this->sWhere	.=	" WHERE $this->condition";	}
			else
			{	$this->sWhere	.=	" AND $this->condition";	}
		}
	}
}
?>