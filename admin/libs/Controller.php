<?php
class Controller {
	var $current	=	NULL;
	var $pagetitle	=	NULL;
	var $formhead	=	NULL;
	public function __construct(){
		$this->current		=	NULL;
		$this->pagetitle	=	NULL;
		$this->formhead		=	NULL;
		$this->common		=	new Common();
	}
	public function __destruct(){
		$db	=	new DBSource();
		$db->closeConnection();
	}
	
	public function call($function) 
	{	$this->$function();	}
	
	public function error_404()
	{	
		include_once _PARTIAL_ROOT.'admin_header.php';
		include_once _VIEWS_ROOT.'error_404.php';
		include_once _PARTIAL_ROOT.'admin_footer.php';
	}
	
	public function islogged($json=false)
	{
		if (!isset($_SESSION['adminId']))
		{
			if (!$json)
			{	header('Location:'._URL.'admins/signin');	}
			exit(json_encode(array('status'=>false, 'redirect'=>true, 'url'=>_URL.'admins/signin')));
		}
	}
	
	public function notlogged($json=false)
	{
		if (isset($_SESSION['adminId'])) {
			if (!$json)
			{	header('Location:'._URL);	}
			exit(json_encode(array('status'=>false, 'redirect'=>true, 'url'=>_URL)));
		}
	}
	
	public function isadmin($json=false)
	{
		if (!$_SESSION['isadmin']) {
			if (!$json)
			{	header('Location:'._URL);	}
			exit(json_encode(array('status'=>false, 'redirect'=>true, 'url'=>_URL)));
		}
	}
}
?>