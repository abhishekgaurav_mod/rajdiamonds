<?php 
class Facebook {
	public function getAccessToken($apiKey, $secret){
		$fbURL	=	'https://graph.facebook.com/oauth/access_token?client_id='.$apiKey.'&client_secret='.$secret.'&grant_type=client_credentials';
		$result	=	$this->fbCall($fbURL);
		if(!($result===false))
		{	return str_replace('access_token=', '', $result);	}
		return NULL;
	}

	public function getFacebookPosts($fbURL){
		$result	=	$this->fbCall($fbURL);
		if(!($result===false))
		{	return json_decode($result);	}
		return NULL;
	}
	
	private function fbCall($fbURL)
	{
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $fbURL);
		curl_setopt($ch,CURLOPT_POST, false);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}
}
?>