<?php 
class Twitter {
	public function getAccessToken($consumerKey, $consumerSecret){
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, 'https://api.twitter.com/oauth2/token');
		curl_setopt($ch,CURLOPT_POST, true);
		
		$data = array();
		$data['grant_type'] = "client_credentials";
		curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch,CURLOPT_USERPWD, $consumerKey . ':' . $consumerSecret);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		
		if(!($result===false))
		{	return (object) json_decode($result);	}
		return 0;
	}

	public function searchTwitter($access_token, $hashtag, $sinceId=NULL){
		$apiurl	=	'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name='.urlencode($hashtag).'&count=100';
		if ($sinceId!=NULL)
		{	$apiurl .=	'&since_id='.$sinceId;	}

		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $apiurl);
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Authorization: Bearer ' . $access_token));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);

		if(!($result===false))
		{	return $result;	}
		return 0;
	}

	public function getTweet($access_token, $tweetId){
		$apiurl	=	'https://api.twitter.com/1.1/statuses/show.json?id='.$tweetId;
	
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $apiurl);
		curl_setopt($ch,CURLOPT_HTTPHEADER,array('Authorization: Bearer ' . $access_token));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
	
		if(!($result===false))
		{	return $result;	}
		return 0;
	}
}
?>