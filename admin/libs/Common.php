<?php
class Common {
    function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	        $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';

	    return $ipaddress;
    }

    function encrypt($string, $key) {
    	$result = '';
    	for($i=0; $i<strlen($string); $i++) {
    		$char = substr($string, $i, 1);
    		$keychar = substr($key, ($i % strlen($key))-1, 1);
    		$char = chr(ord($char)+ord($keychar));
    		$result.=$char;
    	}
    	return base64_encode($result);
    }

    function decrypt($string, $key) {
    	$result = '';
    	$string = base64_decode($string);
    	for($i=0; $i<strlen($string); $i++) {
    		$char = substr($string, $i, 1);
    		$keychar = substr($key, ($i % strlen($key))-1, 1);
    		$char = chr(ord($char)-ord($keychar));
    		$result.=$char;
    	}
    	return $result;
    }
    function slugify($text) {
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = preg_replace('~[^-\w]+~', '', $text);
        $text = trim($text, '-');
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }

    function pickerDate($date)
    {
    	if (strpos($date, '-') === false)
    	{	return false;	}
    	$item = explode('-', $date);
    	if (count($item)!=3)
    	{	return false;	}
    	return date('d-m-Y', strtotime($item[2].'-'.$item[1].'-'. $item[0]));
    }

    function comparedates($date1, $date2)
    {	return ($date1 == $date2 || $date1 < $date2)?true:false;	}

	function truncate($text, $length) {
		$length = abs((int)$length);
        if(strlen($text) > $length) {
        	$text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
		}
        return($text);
     }

     function createsalt(){
     	return str_shuffle(uniqid().time());
     }


     function randomPassword() {
     	$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
     	$pass = array();
     	$alphaLength = strlen($alphabet) - 1;
     	for ($i = 0; $i < 8; $i++) {
     		$n = rand(0, $alphaLength);
     		$pass[] = $alphabet[$n];
     	}
     	return implode($pass);
     }

     function youtubevideoid($link)	{
     	if ($link!='')
     	{
     		$new	=	explode('watch?v=',$link);
     		if (!empty($new[1]))
     		{	$link	=	$new[1];	}
     		else
     		{
     			$new	=	explode('embed/',$link);
     			$link	=	(!empty($new[1]))?$new[1]:$link;
     		}
     	}
     	return $link;
     }
}
?>
