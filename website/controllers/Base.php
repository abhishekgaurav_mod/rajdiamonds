<?php
class Base extends Controller {
	var $current=	false;
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function __call($method, $args) {
		try{
			$helper = new Helpers();
			$page_code =  Common::urlToPageCode($method);
			$helper->setPage($page_code);
			exit();
		}catch (Exception $e){ $this->error_log($e); }
	}

	public function index() {
		$this->current	=	false;

		$slider		=	new Slider();
		$category	=	new Category();
		$product	=	new Product();

		$arr_sliders			= $slider->getSliders();
		$arr_featuredCategory 	= $category->getFeaturedCategories();

		if (!empty($arr_featuredCategory)){
			foreach ($arr_featuredCategory as $obj_featuredCategory) {
				$arr_product					=	$product->getHomeProductsByCategoryId($obj_featuredCategory->category_id);
				$obj_featuredCategory->products	=	$arr_product;
			}
		}

		$this->metatitle 		= 'Home';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';

		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'index.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function aboutus() {
		$this->current	=	'about-us';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('aboutus');

		$this->metatitle 		= 'About Us';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';

		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/aboutus.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function careandstorage() {
		$this->current	=	'care-and-storage';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('careandstorage');

		$this->metatitle 		= 'Care And Storage';
		$this->metakeyword 		= 'Diamond Care,Jewellery Storage';
		$this->metadescription 	= 'Keep the fire in your diamond alive. Taking care of your diamonds is made easy. Just visit Raj Diamonds every six months, and have your diamond mountings and settings checked free of cost.';

		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/careandstorage.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function thefourcs() {
		$this->current	=	'the-four-cs';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('thefourcs');

		$this->metatitle 		= 'The Four Cs';
		$this->metakeyword 		= 'Diamond Shapes,Cut,Colour,Clarity,Carat';
		$this->metadescription 	= 'Rarer than rare, every Raj Diamonds diamond is meticulously selected for its exceptional characteristics and promise above and beyond the 4Cs of Cut, Colour, Clarity and Carat.';

		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/thefourcs.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function ourlegacy() {
		$this->current	=	'our-legacy';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('ourlegacy');

		$this->metatitle 		= 'Our Legacy';
		$this->metakeyword 		= '';
		$this->metadescription 	= 'The illustrious history of Raj Diamonds begins when renowned connoisseur of fine jewellery, Vijaykumar Surana built the first showroom in Bengaluru in 2010. The first branch was located in the traditional heart of Old Bangalore at Jayanagar.';

		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/ourlegacy.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function ourexpertise() {
		$this->current	=	'our-expertise';
		$cms			=	new Cmscontent();
		$cmsContent		=	$cms->getCmscontentByType('ourexpertise');

		$this->metatitle 		= 'Our Expertise';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';

		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'pages/ourexpertise.php';
		include _PARTIAL_ROOT.'_footer.php';
	}

	public function dynamicpage() {
		$this->current	=	false;

		$uri = $_SERVER[REQUEST_URI];
		$uri = str_replace('.php', '', $uri);
		$uri = trim($uri, '/');		
		$uri = explode('?', $uri);
		$uri = $uri[0];
		$nav = explode('/', $uri);

		if (!empty($_GET)) {
			foreach($_GET as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}
		}

		$account		=	new Account();
		$category		=	new Category();
		$product		=	new Product();
		$tag			=	new Tag();
		
		$curr_user	=	$account->getUserById($_SESSION['userId']);
		$arr_favtId = array();
		if (!empty($curr_user->favourite)) {
			$arr_favtId = explode(',', $curr_user->favourite);
		}

		$arr_category 	= $category->getAllCategories();
		$arr_tag 		= $tag->getAllTags();


		/*CATEGORY OR TAG PAGE*/
		if (!empty($nav[0]) && empty($nav[1])) {
			$p 		= ((empty($p) || $p < 0)?'1':$p);
			$qty	= 6;
			$lLimit = ($p-1)*$qty ;
			$uLimit = $qty;

			$obj_category 	= $category->getCategoryBySlug($nav[0]);
			$obj_tag 		= $tag->getTagBySlug($nav[0]);

			if (!empty($obj_category)) {
				$this->metatitle 		= $obj_category->category_name;
				$this->metakeyword 		= '';
				$this->metadescription 	= trim(strip_tags(preg_replace( "/\r|\n/", "", $obj_category->category_description)));

				$arr_AllProduct	=	$product->getProductsByCategoryId($obj_category->category_id);
				$arr_product	=	$product->getLimitProductsByCategoryId($obj_category->category_id, $lLimit, $uLimit);

				$totalPage = ceil(count($arr_AllProduct)/$qty);
				if ($p > $totalPage) {
					header('Location: '._URL.$nav[0].'?p='.$totalPage);
				}
				if (!empty($arr_product)) {
					include _PARTIAL_ROOT.'_header.php';
					include _VIEWS_ROOT.'products/category.php';
				} else {
					include _PARTIAL_ROOT.'_header.php';
					include _VIEWS_ROOT.'error_404.php';
				}
			}
			else if (!empty($obj_tag)) {
				$this->metatitle 		= $obj_tag->tag_name;
				$this->metakeyword 		= '';
				$this->metadescription 	= trim(strip_tags(preg_replace( "/\r|\n/", "", $obj_tag->tag_description)));

				$arr_AllProduct	=	$product->getProductsByTagId($obj_tag->tag_id);
				$arr_product	=	$product->getLimitProductsByTagId($obj_tag->tag_id, $lLimit, $uLimit);

				$totalPage = ceil(count($arr_AllProduct)/$qty);
				if ($p > $totalPage) {
					header('Location: '._URL.$nav[0].'?p='.$totalPage);
				}
				if (!empty($arr_product)) {
					include _PARTIAL_ROOT.'_header.php';
					include _VIEWS_ROOT.'products/tag.php';
				} else {
					include _PARTIAL_ROOT.'_header.php';
					include _VIEWS_ROOT.'error_404.php';
				}
			}
			else {
				include _PARTIAL_ROOT.'_header.php';
				include _VIEWS_ROOT.'error_404.php';
			}
		}
		/*PRODUCT PAGE*/
		else if (!empty($nav[0]) && !empty($nav[1]) && empty($nav[2])) {			
			$obj_category 	= $category->getCategoryBySlug($nav[0]);
			$obj_tag 		= $tag->getTagBySlug($nav[0]);
			$obj_product 	= $product->getProductsByProductSlug($nav[1]);

			$arr_tagProduct 		= $product->getRelatedProductsByTagSlug($nav[0]);
			$arr_categoryProduct 	= $product->getRelatedProductsByCategorySlug($nav[0]);

			$this->metatitle 		= $obj_product->product_name;
			$this->metakeyword 		= '';
			$this->metadescription 	= trim(strip_tags(preg_replace( "/\r|\n/", "", $obj_product->description)));

			if (!empty($arr_tagProduct)) {
				$arr_relatedProduct = $arr_tagProduct;
			} else if (!empty($arr_categoryProduct)) {
				$arr_relatedProduct = $arr_categoryProduct;
			}

			if (!empty($obj_product) && (!empty($obj_category) || !empty($obj_tag))) {
				$arr_tag 		= $tag->getTagsByProductId($obj_product->product_id);

				include _PARTIAL_ROOT.'_header.php';
				include _VIEWS_ROOT.'products/product-detail.php';
			} else {
				include _PARTIAL_ROOT.'_header.php';
				include _VIEWS_ROOT.'error_404.php';
			}
		}

		else {
			include _PARTIAL_ROOT.'_header.php';
			include _VIEWS_ROOT.'error_404.php';
		}

		include _PARTIAL_ROOT.'_footer.php';
	}

	public function error_404()
	{
		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'error_404.php';
		include _PARTIAL_ROOT.'_footer.php';
		exit;
	}
}
?>
