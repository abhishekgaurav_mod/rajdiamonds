<?php
class Contact extends Controller {

	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function index()
	{

		$this->current	=	'contact';
		$enquiry 		=	new Enquiry();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}

			$err        =   NULL;
			if ($err==NULL && empty($name))
			{	$err    =   'Please enter name.';	}
			if ($err==NULL && empty($email))
			{	$err    =   'Please enter email.';	}
			if ($err==NULL && !filter_var($email, FILTER_VALIDATE_EMAIL))
			{	$err    =   'Please enter valid email.';	}
			if ($err==NULL && empty($phone))
			{	$err    =   'Please enter contact number.';	}
			if ($err==NULL && !preg_match("/^[0-9+ -]+$/", $phone))
	        {  $err    =   'Please enter valid contact number.';        }
	       if  ($err==NULL && !(strlen($phone) >= 10 && strlen($phone) < 14 ))
	        {  $err    =   'Contact number should be min 10 digits.';        }
			if ($err==NULL && empty($message))
			{	$err    =   'Please enter message.';	}

			if ($err==NULL)
			{
				$obj_enquiry   =   (Object)'';
				$obj_enquiry->name  		=   $name;
				$obj_enquiry->phone  	    =   $phone;
				$obj_enquiry->email  		=   $email;
				$obj_enquiry->product_code  =   $product_code;
				$obj_enquiry->subject  		=   $subject;
				$obj_enquiry->message  		= 	$message;

				if ($err==NULL)
				{
					$enquiryId     =   $enquiry->addEnquiry($obj_enquiry);
					if (!empty($enquiryId))
					{
						$mailer		=	new Mailer();

						$sendTo			=	'mg@rajdiamonds.com';
						/*$sendTo				=	'abhishek@motherofdesign.in';*/
						$mailSubject		=	'Raj Diamonds - Received New Enquiry';
						$contactBody        =	file_get_contents(_EMAIL_TEMPLATE.'mail_to_admin.inc');
						$contactBody        =   str_replace('{name}',$obj_enquiry->name,$contactBody);
						$contactBody        =   str_replace('{phone}',$obj_enquiry->phone,$contactBody);
						$contactBody        =   str_replace('{email}',$obj_enquiry->email,$contactBody);
						$contactBody        =   str_replace('{product_code}',$obj_enquiry->product_code,$contactBody);
						$contactBody        =   str_replace('{subject}',$obj_enquiry->subject,$contactBody);
						$contactBody        =   str_replace('{message}',$obj_enquiry->message,$contactBody);

						$mailer->SendHTMLEMail($sendTo, $mailSubject, $contactBody);

						echo json_encode(array('status'=>true,'prompt'=>true, 'htmlrefresh'=>false, 'message'=>'Thank you for contacting us.....', 'redirect'=>true, 'url'=>_URL));
						exit();
					}
				}
				$err    =   ($err==NULL)?'Unable to Send Enquiry.':$err;
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}

		$this->metatitle 		= 'Contact Us';
		$this->metakeyword 		= 'Branch Locations,Showrooms,Phone,Email,Address,Enquiry';
		$this->metadescription 	= '';

		include _PARTIAL_ROOT.'_header.php';
		include _VIEWS_ROOT.'contact/index.php';
		include _PARTIAL_ROOT.'_footer.php';
	}


}
?>
