<?php
class Accounts extends Controller {
	
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}
			
	public function index()
	{
		$this->islogged();
    	
    	$this->current	=	'account';
    	$this->pagetitle=	'account';

		$account	=	new Account();
		$obj_user	=	$account->getUserById($_SESSION['userId']);

		$this->metatitle 		= 'My Account';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
    	
		include_once _PARTIAL_ROOT.'_header.php';
		include_once _VIEWS_ROOT.'accounts/index.php';
		include_once _PARTIAL_ROOT.'_footer.php';
	}
			
	public function favourite()
	{
		$this->islogged();
    	
    	$this->current	=	'account';
    	$this->pagetitle=	'account';

		$account	=	new Account();
		$product	=	new Product();
		$obj_user	= $account->getUserById($_SESSION['userId']);
		$arr_favtId = array();
		if (!empty($obj_user->favourite)) {
			$arr_favtId = explode(',', $obj_user->favourite);
		}
		$arr_favtProduct = array();
		if (!empty($arr_favtId)) {
			foreach ($arr_favtId as $key => $product_id) {
				$obj_product = $product->getProductsByProductId($product_id);
				array_push($arr_favtProduct, $obj_product);
			}
		}

		$this->metatitle 		= 'My Favourite';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';
    	
		include_once _PARTIAL_ROOT.'_header.php';
		include_once _VIEWS_ROOT.'accounts/favourite.php';
		include_once _PARTIAL_ROOT.'_footer.php';
	}
			
	public function addfavourite()
	{
		try {
			$this->islogged();
			if (!empty($_POST)) {
				$account    = new Account();
				foreach ($_POST as $key=>$val)
				{  $$key = $val;  }

				$err = NULL;
				if ($err==NULL && empty($product_id))
				{ $err    =   'Invalid Request.'; }

				if($err==NULL){
					$obj_user	= $account->getUserById($_SESSION['userId']);
					$arr_favtId = array();
					if (!empty($obj_user->favourite)) {
						$arr_favtId = explode(',', $obj_user->favourite);
					}
					if(in_array($product_id, $arr_favtId)){
						exit(json_encode(array('status'=>false, 'message'=>'Error: Already added to favourite.')));
					}

					array_push($arr_favtId, $product_id);
					$string_favtId = implode(',', $arr_favtId);
					$account->updateFavourite($_SESSION['userId'], $string_favtId);

					exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Added to favourite list.')));
				} else {
					exit(json_encode(array('status'=>false, 'message'=>'Error: not added to favourite.')));
				}
			}
		}catch (Exception $e){ $this->error_log($e); }
	}

	
	public function profile()
	{
		$this->islogged();
		$account		=	new Account();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}
			
			$err        =   NULL;
				$obj_user  =   (Object)'';
				if ($err==NULL && !filter_var($email, FILTER_VALIDATE_EMAIL))
				{	$err    =   'Please enter valid email.';	}
				if ($err==NULL && ($password!=''))
				{
					if ($err==NULL && empty($retypepassword))
					{	$err    =   'Please repeat password.';	}
					if ($err==NULL && $password!=$retypepassword)
					{	$err    =   'Password mismatch.';	}
					else
					{   
						$obj_user->password   =    $password;
						$obj_user->salt_string	=   str_shuffle(uniqid().time());
					}
				}
				if ($err==NULL && empty($name))
				{	$err    =   'Please enter name.';	}
				
                if ($err==NULL) {
                	$obj    =   $account->getUserByEmail($email);
                    if (!empty($obj)) {
                    	if ($obj->user_id!=$_SESSION['userId'])
                        {   $err    =   'Email already in use.';    }
					}        
				}
				if ($err==NULL) {
                	$obj_user->user_id  =   $_SESSION['userId'];
                	$obj_user->name		=   $name;
                	$obj_user->email  	=   $email;
                	$obj_user->phone  	=   $phone;
                	$obj_user->address  =   $address;
                	
                	if ($err==NULL) {
                        if ($account->updateUser($obj_user)) {
                        	if ($userId==$_SESSION['userId']) {
                        		$_SESSION['name']	=	$obj_user->name;
                        		$_SESSION['email']	=	$obj_user->email;
                        	}
                            exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'accounts/')));
                        }
                	}
                	$err    =   ($err==NULL)?'Error: Unable to update profile.':$err;
				}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		else
		{
			$this->current		=	'account';
    		$this->pagetitle	=	'account';
    		$this->formhead		=	'account';
			if (!empty($_SESSION['userId']))
			{	$obj_user   =   $account->getUserById($_SESSION['userId']);	}
		}

		$this->metatitle 		= 'My Profile';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';

		include_once _PARTIAL_ROOT.'_header.php';
		include_once _VIEWS_ROOT.'accounts/profile.php';
		include_once _PARTIAL_ROOT.'_footer.php';
	}
	
	public function register() {
		$this->notlogged();
		$account		=	new Account();
		if (!empty($_POST)) {
			foreach($_POST as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}
			
			$err =   NULL;
				if ($err==NULL && empty($name))
				{	$err    =   'Please enter name.';	}
				if ($err==NULL && !filter_var($email, FILTER_VALIDATE_EMAIL))
				{	$err    =   'Please enter valid email.';	}
				if ($err==NULL && empty($password))
				{	$err    =   'Please enter user password.';	}
				if ($err==NULL && empty($retypepassword))
				{	$err    =   'Please confirm user password.';	}
				if ($err==NULL && ($password!=$retypepassword))
				{	$err    =   'Password mismatch.';	}
				if ($err==NULL && $account->checkUserEmail($email))
				{	$err    =   'Email already in use.';	}
				
				if ($err==NULL) {
					$obj_user   =   (Object)'';
					$obj_user->name			=   $name;
					$obj_user->email  	 	=   $email;
					$obj_user->phone		=   $phone;
					$obj_user->salt_string	=   str_shuffle(uniqid().time());
					$obj_user->password		=   $password;
					$obj_user->address		=   $address;
					if ($err==NULL) {
						$userId     =   $account->addUser($obj_user);
						if (!empty($userId)) {

							$obj_user	=	$account->getUserById($userId);
		    				$_SESSION['userId']	=	$userId;
	        				$_SESSION['name']	=	$obj_user->name;
	        				$_SESSION['email']	=	$obj_user->email;
	        				$_SESSION['phone']	=	$obj_user->phone;
							exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'accounts')));
						}
					}
					$err    =   ($err==NULL)?'Error: Unable to Register.':$err;
				}
				
				
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		} else {
			$this->current		=	'Register';
    		$this->pagetitle	=	'Register';

			$category		=	new Category();
			$tag			=	new Tag();

			$arr_category 	= $category->getAllCategories();
			$arr_tag 		= $tag->getAllTags();

			$this->metatitle 		= 'Register';
			$this->metakeyword 		= '';
			$this->metadescription 	= '';

			include_once _PARTIAL_ROOT.'_header.php';
			include_once _VIEWS_ROOT.'accounts/register.php';
			include_once _PARTIAL_ROOT.'_footer.php';
		}
	}
		
	public function login() {
		$this->notlogged();
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= trim(strip_tags($val));	}
			
			$err		=	NULL;
			if (empty($useremail))
			{	$err		=	'Please enter email.';	}
			if ($err==NULL && !filter_var($useremail, FILTER_VALIDATE_EMAIL))
            {   $err    =   'Please enter valid email.';        }
			if ($err==NULL && empty($userpassword))
			{	$err		=	'Please enter password.';	}
			if ($err==NULL)
			{
				$account		=	new Account();
				$common		=	new Common();
					
				$obj_user	=	$account->getUserByEmail($useremail);
				if (!empty($obj_user))
				{
					$obj_user	=	$account->authenticateUser($useremail,$obj_user->salt_string.$userpassword);
        			if (!empty($obj_user))
        			{
        				$_SESSION['userId']	=	$obj_user->user_id;
        				$_SESSION['name']	=	$obj_user->name;
        				$_SESSION['email']	=	$obj_user->email;
	        			$_SESSION['phone']	=	$obj_user->phone;
        				
        				exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL)));
        			}
				}
				$err    =   'Incorrect username or password.';
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}
		$category		=	new Category();
		$tag			=	new Tag();

		$arr_category 	= $category->getAllCategories();
		$arr_tag 		= $tag->getAllTags();

		$this->metatitle 		= 'Login';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';

		include_once _PARTIAL_ROOT.'_header.php';
		include_once _VIEWS_ROOT.'accounts/login.php';
		include_once _PARTIAL_ROOT.'_footer.php';
	}
		
	public function fblogin() {
		$this->notlogged();
		if (!empty($_POST)) {

			$account		=	new Account();

			$obj_user   =   (Object)'';
			$obj_user->name				=   $_POST['name'];
			$obj_user->email  	 		=   $_POST['email'];
			$obj_user->first_name  		=   $_POST['first_name'];
			$obj_user->middle_name  	=   $_POST['middle_name'];
			$obj_user->last_name  		=   $_POST['last_name'];
			$obj_user->gender  			=   $_POST['gender'];
			$obj_user->age_range  		=   $_POST['age_range'];
			$obj_user->social_link  	=   $_POST['social_link'];
			$obj_user->socialmedia_id  	=   $_POST['socialmedia_id'];
			$obj_user->login_type  		=   'FB';
			$obj_user->salt_string		=   str_shuffle(uniqid().time());

			$userSocialMedia     =   $account->getUserBySocialMediaId($obj_user->socialmedia_id,$obj_user->login_type);
			if (empty($userSocialMedia)) {
				$userId     =   $account->addSocialMediaUser($obj_user);
			} else {
				$userId = $userSocialMedia->user_id;
			}
			
			if (!empty($userId)) {
				$_SESSION['userId']	=	$userId;
				$_SESSION['name']	=	$_POST['name'];
				$_SESSION['email']	=	$_POST['email'];
				$_SESSION['phone']	=	'';
				exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL)));
			} else {
				exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'accounts/login')));
			}

		} else {
			exit(json_encode(array('status'=>true, 'redirect'=>true, 'url'=>_URL.'accounts/login')));
		}
	}
	
	public function logout()
	{
		session_destroy();
    	header('Location: '._URL.'accounts/login');
	}
	
	public function changepassword()
	{
		$this->islogged();
		$this->current		=	'Change Password';
		$this->pagetitle	=	'Change Password';
		$this->formhead		=	'Change Password';
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= 	trim(strip_tags($val));	}
			
			$err	=	NULL;
			if (empty($newpassword))
			{	$err	=	'Please enter your new password.';	}
			if ($err==NULL && ($newpassword!=$confirmpassword))
			{	$err	=	'Password mis-match.';	}

			if ($err==NULL)
			{
				$account		=	new Account();
				if ($account->changePassword(str_shuffle(uniqid().time()), $confirmpassword, $_SESSION['userId']))
				{	exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Password updated successfully!!!')));	}
				else
				{	$err	=	'Error: while updating password.';	}
			}
			exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>$err)));
		}

		$this->metatitle 		= 'Change Password';
		$this->metakeyword 		= '';
		$this->metadescription 	= '';

		include_once _PARTIAL_ROOT.'_header.php';
		include_once _VIEWS_ROOT.'accounts/change_password.php';
		include_once _PARTIAL_ROOT.'_footer.php';
	}
	
	public function sendmail(){
		$this->islogged(true);
		if (!empty($_POST))
		{
			foreach($_POST as $key=>$val)
			{	$$key	= $val;	}
			
			if (!empty($_SESSION['userId'])){
				$common		=	new Common();
				$account		=	new Account();
				$password	=	$common->randomPassword();

				$obj_user	=	$account->getUserById($_SESSION['userId']);
				if (!empty($obj_user)){
					if ($account->changePassword(str_shuffle(uniqid().time()), $password, $obj_user->user_id))
					{
						$mailer		=	new Mailer();
						$mailBody	=	file_get_contents(_EMAIL_TEMPLATE.'create_user.inc');
						$mailBody	=	str_replace('{name}',$obj_user->name,$mailBody);
						$mailBody	=	str_replace('{email}',$obj_user->email,$mailBody);
						$mailBody	=	str_replace('{password}',$password,$mailBody);
						$mailBody	=	str_replace('{target}',_URL,$mailBody);
						$mailBody	=	str_replace('{footer}',_TITLE,$mailBody);
						$mailer->SendHTMLEMail($obj_user->email, _CREATE_ACCOUNT, $mailBody);
						exit(json_encode(array('status'=>true, 'prompt'=>true, 'message'=>'Login details being sent to '.$name.' on there registered email address.')));
					}
				}
			}
		}
		exit(json_encode(array('status'=>false, 'prompt'=>true, 'message'=>'Error: while sending login details!!!')));
	}
} 
?>