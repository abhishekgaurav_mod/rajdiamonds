<?php
class Boot
{
	var $url		=	NULL;
	var $target		=	NULL;
	var $controller	=	NULL;
	var $method		=	NULL;
	var $mysqlconnect	=	false;
	var $dynamicfunction		=	false;

	public function __construct() {
		$this->url			=	'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		$this->controller	=	'Base';
		$this->method		=	'index';
    }

    public function loadController()
    {
    	try
    	{
	    	if ($this->url==_URL)
	    	{
				$this->controller	=	'Base';
				$this->method		=	'index';
	    	}
	    	else
	    	{
	    		try
	    		{
	    			$this->controller	=	'Base';
	    			$this->url	=	trim($this->url,'?/');
	    		    if (strpos($this->url,'?'))
	     			{
	     				$str		=	explode('?',$this->url);
	     				$this->url	=	(!empty($str))?$str[0]:$this->url;
	     			}
	     			$this->target =	trim(substr($this->url, strlen(_URL)),'/');
	    		    if (strpos($this->target,'/'))
	     			{
	     				$str	=	explode('/',$this->target);
						$this->controller	=	str_replace('-', '', $str[0]);
						$this->method		=	str_replace('-', '', $str[1]);
					} else {
	    				$this->controller	=	(strlen($this->target) > 0 )?str_replace('-', '', $this->target):'Base';
					}
					$this->controller		=	ucfirst(strtolower($this->controller));

					if (!method_exists($this->controller,$this->method))
					{
						$this->controller	=	'Base';
						$this->method		=	$this->target;
						$this->dynamicfunction	=	true;
					}
	    		}
	    	    catch (Exception $e)
	        	{   exit('Caught exception: '.$e->getMessage());    }
	    	}
	    	$instance = new $this->controller;
	    	if($this->dynamicfunction){
	    		$params	=	explode('/',$this->target);
    			$instance->{$this->method}($params);
	    	}else{
    			$instance->call($this->method);
	    	}
    	}
    	catch (Exception $e)
    	{	exit('Caught exception: '.$e->getMessage());	}
	}
}
?>
