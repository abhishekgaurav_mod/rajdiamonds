<?php
require("class.phpmailer.php");  
class Mailer extends PHPMailer {  
    var $From           = _FROM_EMAIL;  
    var $FromName       = _EMAIL_FROM_NAME;  
    var $Host           = _SMTP_SERVER;  
    var $Mailer         = "smtp";
    var $WordWrap       = 50; 
    var $Username      	= _EMAIL_LOGIN;          
    var $Password   	= _EMAIL_PASSWORD;	
    var $SMTPAuth   	= true;                  		
    var $SMTPSecure 	= _EMAIL_SMTPSECURE; 
    var $Port       	= _EMAIL_PORT; 
    
    function SendHTMLEMail($address, $subject, $htmlBody) {
        $this->IsSMTP();
        $this->IsHTML(true);

        $this->AltBody    = "To view the message, please use an HTML compatible email viewer!";
        $this->Subject    = $subject;
        $this->MsgHTML($htmlBody);

        $this->AddReplyTo(_EMAIL_REPLY_TO, _EMAIL_FROM_NAME);
        $this->AddAddress($address);
        $this->Send();
		$this->ClearAddresses();
		$this->ClearReplyTos();	
    }
}
?>
