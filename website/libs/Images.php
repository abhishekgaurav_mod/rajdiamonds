<?php 
class Images {
	public function __construct(){}
	public function __destruct(){}

	public function uplaodImageError($file_error)
	{
		$error	=	NULL;
		if(!empty($file_error))
		{
			switch($file_error)
			{
	
				case '1': 	$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
				case '2':	$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
				case '3':	$error = 'The uploaded file was only partially uploaded';
				break;
				case '4':	$error = 'No file was uploaded.';
				break;
				case '6':	$error = 'Missing a temporary folder';
				break;
				case '7':	$error = 'Failed to write file to disk';
				break;
				case '8':	$error = 'File upload stopped by extension';
				break;
				case '999':
				default:	$error = 'No error code avaiable';
			}
		}
		return $error;
	}
	
	public function validateImageType($file_type, $allowed_ext)
	{	return (!in_array($file_type, $allowed_ext))?true:false;	}
	
	public function uploadimage($file, $fileName, $upload_dir)
	{
		$file_ext 	= 	strtolower(substr($file['name'],strrpos($file['name'],'.')));
		$extension	= 	$fileName.$file_ext;
		return (move_uploaded_file($file['tmp_name'], $upload_dir.$extension))?$fileName.$file_ext:NULL;
	}
	
	public function transparentpng($org_dir, $org_name, $target_dir, $target_name)
	{
		$image = new Imagick($org_dir.$org_name);
		$image->newImage($image->getImageWidth(), $image->getImageHeight());
		$image->setImageFormat('png');
		$file_ext 	= 	strtolower(substr($target_name,strrpos($target_name,'.')));
		$target_name	=	str_replace($file_ext, '.png', $target_name);
		$image->writeImage($target_dir.$target_name);
		$image->destroy();
		return $target_name;
	}
	
	public function converttojpg($org_dir, $org_name, $target_dir, $target_name)
	{
		$image = new Imagick($org_dir.$org_name);
		$image->compositeimage($image, Imagick::COMPOSITE_OVER, 0, 0);
		$image->setImageFormat('jpg');
		$file_ext 	= 	strtolower(substr($target_name,strrpos($target_name,'.')));
		$target_name	=	str_replace($file_ext, '.jpg', $target_name);
		$image->writeImage($target_dir.$target_name);
		$image->destroy();
		return $target_name;
	}
	
	public function copyimage($org_dir, $org_name, $thumb_dir, $thumb_name)
	{
		return @copy($org_dir.$org_name, $thumb_dir.$thumb_name);
	}
	
	public function createthumnail($org_dir, $org_name, $thumb_dir, $thumb_name, $maxsize)
	{
		ini_set("memory_limit","20M");
		$image = new Imagick($org_dir.$org_name);
	
		$imagewidth		=	$image->getImageWidth();
		$imageheight	=	$image->getImageHeight();
		$format 		= 	$image->getImageFormat();
	
		if ($imagewidth>$maxsize){
			if ($format == 'GIF')
			{
				$image = $image->coalesceImages();
				do {
					$image->resizeImage($maxsize, 0, Imagick::FILTER_BOX, 1);
				} while ($image->nextImage());
				$image = $image->deconstructImages();
				$image->writeImages($thumb_dir.$thumb_name, true);
				$image->clear();
				$image->destroy();
			}
			else
			{
				$image->resizeImage($maxsize,0,Imagick::FILTER_LANCZOS,1);
				$image->setImageCompression(Imagick::COMPRESSION_JPEG);
				$image->setImageCompressionQuality(75);
				$image->stripImage();
				$image->writeImage($thumb_dir.$thumb_name);
				$image->destroy();
			}
		}
		else {
			$image->destroy();
			@copy($org_dir.$org_name, $thumb_dir.$thumb_name);
		}
	}
}
?>