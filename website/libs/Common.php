<?php
class Common {
    function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	        $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';

	    return $ipaddress;
    }

    function encrypt($string, $key) {
    	$result = '';
    	for($i=0; $i<strlen($string); $i++) {
    		$char = substr($string, $i, 1);
    		$keychar = substr($key, ($i % strlen($key))-1, 1);
    		$char = chr(ord($char)+ord($keychar));
    		$result.=$char;
    	}
    	return base64_encode($result);
    }

    function decrypt($string, $key) {
    	$result = '';
    	$string = base64_decode($string);
    	for($i=0; $i<strlen($string); $i++) {
    		$char = substr($string, $i, 1);
    		$keychar = substr($key, ($i % strlen($key))-1, 1);
    		$char = chr(ord($char)-ord($keychar));
    		$result.=$char;
    	}
    	return $result;
    }

	function truncate($text, $length) {
		$length = abs((int)$length);
        if(strlen($text) > $length) {
        	$text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
		}
        return($text);
    }

    function toAscii($str, $replace=array(), $delimiter='-') {
    	if( !empty($replace) )
    	{	$str = str_replace((array)$replace, ' ', $str);	}

     	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
     	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
     	$clean = strtolower(trim($clean, '-'));
     	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

    function formatSizeUnits($bytes)
    {
    	if ($bytes >= 1073741824)
    	{	$bytes = number_format($bytes / 1073741824, 2) . ' GB';	}
    	elseif ($bytes >= 1048576)
    	{	$bytes = number_format($bytes / 1048576, 2) . ' MB';	}
    	elseif ($bytes >= 1024)
    	{	$bytes = number_format($bytes / 1024, 2) . ' KB';	}
    	elseif ($bytes > 1)
    	{	$bytes = $bytes . ' bytes';	}
    	elseif ($bytes == 1)
    	{	$bytes = $bytes . ' byte';	}
    	else
    	{	$bytes = '0 bytes';	}
    	return $bytes;
    }

     function createsalt(){
        return str_shuffle(uniqid().time());
     }


     function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
     }

    function urlToPageCode($params){
        return '|'.str_replace('-', '_',strtoupper(Common::clean(str_replace('/','|', $params)))).'|';
    }

    function clean($str) {
        $str = @trim($str);
        $str = strip_tags($str);
        if(get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        }

        return $str;
    }
}
?>
