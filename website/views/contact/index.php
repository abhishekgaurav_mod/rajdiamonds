<div class="wrap-content">
   <div class="container">          
   <?php include _PARTIAL_ROOT.'_breadcrumb.php'; ?>
   </div>

   <div id="mapCanvas"> </div>

   <div class="container">
      <div class="row addressMap">
         <div class="col-md-6">

            <section class="b-contact">
               <h2 class="ui-title-block-2">Branch Locations:</h2>
               <div class="b-info">
                  <div class="b-contact__decription">
                     <div class="row">
                        <div class="col-md-4">
                           <div class="b-info__name"><i class="icon fa fa-map-signs"></i> MG Road:</div>
                        </div>
                        <div class="col-md-8">
                           <div class="b-info__info">#52, The Grandiose, MG Road,<br>Bengaluru - 560 001</div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4">
                           <div class="b-info__name"><i class="icon fa fa-map-marker"></i> Landmark:</div>
                        </div>
                        <div class="col-md-8">
                           <div class="b-info__info">Opp. to Metro Station</div>
                           <div class="b-info__info"><a href="https://www.google.co.in/maps/place/RAJ+DIAMONDS/@12.9743596,77.5987753,15z/data=!4m5!3m4!1s0x3bae16834632d7af:0xaead3c28cb10843c!8m2!3d12.975034!4d77.607471" class="link" target="_blank">View on Google Map</a></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4">
                           <div class="b-info__name"><i class="icon fa fa-phone"></i> Phone:</div>
                        </div>
                        <div class="col-md-8">
                           <div class="b-info__info"><a href="tel:08025582550">080 25582550</a> / <a href="tel:08025592550">080 25592550</a></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4">
                           <div class="b-info__name"><i class="icon fa fa-envelope"></i> Email:</div>
                        </div>
                        <div class="col-md-8">
                           <div class="b-info__info"><a href="mailto:mg@rajdiamonds.com">mg@rajdiamonds.com</a></div>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6 col-xs-6">
                        <div class="b-info__name"><i class="icon fa fa-user"></i> Social Media:</div>
                     </div>
                     <div class="col-md-6 col-xs-6">
                        <div class="b-info__info">
                           <ul class="social-net list-inline">
                              <li class="social-net__item"><a href="https://www.facebook.com/rajdiamondsmg" target="_blank" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
                              <li class="social-net__item"><a href="https://www.instagram.com/rajdiamonds/" target="_blank" class="social-net__link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <div class="col-md-1">

         </div>
         <div class="col-md-5">
            <section class="b-contact">
               <div class="degree360">
                  <h2 class="ui-title-block-2">Take a Virtual Tour of our Showrooms:</h2>
                  <a href="https://www.google.co.in/maps/uv?hl=en&pb=!1s0x3bae16834632d7af%3A0xaead3c28cb10843c!2m22!2m2!1i80!2i80!3m1!2i20!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e5!2m2!1m1!1e4!2m2!1m1!1e6!3m1!7e115!4shttps%3A%2F%2Fpicasaweb.google.com%2Flh%2Fsredir%3Funame%3D114789977497780492722%26id%3D6336380746666424802%26target%3DPHOTO!5sraj%20diamonds%20mg%20road%20bangalore%20-%20Google%20Search&imagekey=!1e10!2sAF1QipOn6wcBel23B0fgxfGSLhWy0xmL4vjuGYjuNi5j&sa=X&ved=0ahUKEwjMpN217dbVAhXBvI8KHZ8LBucQoioIgQEwDg" target="_blank"><img src="<?=_IMAGES?>mg-road.jpg" alt="MG Road"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> </a>
               </div>
            </section>
         </div>
      </div>

      <div class="row addressMap">
         <div class="col-md-6">

            <section class="b-contact">
               <!-- <h2 class="ui-title-block-2">Branch Locations:</h2> -->
               <div class="b-info">
                  <div class="b-contact__decription">
                     <div class="row">
                        <div class="col-md-4">
                           <div class="b-info__name"><i class="icon fa fa-map-signs"></i> Jayanagar:</div>
                        </div>
                        <div class="col-md-8">
                           <div class="b-info__info">#701, 11th Main, 33rd Cross, 4th Block, Jayanagar, Bengaluru - 560 011</div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4">
                           <div class="b-info__name"><i class="icon fa fa-map-marker"></i> Landmark:</div>
                        </div>
                        <div class="col-md-8">
                           <div class="b-info__info">Jeweller Street</div>
                           <div class="b-info__info"><a href="https://www.google.co.in/maps/place/Raj+Diamonds/@12.926056,77.5809725,16z/data=!4m5!3m4!1s0x3bae15a28e906d7f:0x4f9a7858a8b97f5a!8m2!3d12.9262547!4d77.5856181" class="link" target="_blank">View on Google Map</a></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4">
                           <div class="b-info__name"><i class="icon fa fa-phone"></i> Phone:</div>
                        </div>
                        <div class="col-md-8">
                           <div class="b-info__info"><a href="tel:08022445570">080 22445570</a> / <a href="tel:08022445571">080 22445571</a></div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4">
                           <div class="b-info__name"><i class="icon fa fa-envelope"></i> Email:</div>
                        </div>
                        <div class="col-md-8">
                           <div class="b-info__info"><a href="mailto:nitin@rajdiamonds.com">nitin@rajdiamonds.com</a></div>
                        </div>
                     </div>
                  </div>

                  <div class="row">
                     <div class="col-md-6 col-xs-6">
                        <div class="b-info__name"><i class="icon fa fa-user"></i> Social Media:</div>
                     </div>
                     <div class="col-md-6 col-xs-6">
                        <div class="b-info__info">
                           <ul class="social-net list-inline">
                              <li class="social-net__item"><a href="https://www.facebook.com/rajdiamondsmg" target="_blank" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
                              <li class="social-net__item"><a href="https://www.instagram.com/rajdiamonds/" target="_blank" class="social-net__link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <div class="col-md-1">

         </div>
         <div class="col-md-5">
            <section class="b-contact">
               <div class="degree360">
                  <!-- <h2 class="ui-title-block-2">Take a Virtual Tour of our Jayanagar Showroom:</h2> -->
                  <a href="https://www.google.co.in/maps/uv?hl=en&pb=!1s0x3bae15a28e906d7f%3A0x4f9a7858a8b97f5a!2m22!2m2!1i80!2i80!3m1!2i20!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e5!2m2!1m1!1e4!2m2!1m1!1e6!3m1!7e115!4shttps%3A%2F%2Fpicasaweb.google.com%2Flh%2Fsredir%3Funame%3D115662752527291933992%26id%3D6383112668920248514%26target%3DPHOTO!5sraj%20diamonds%20jayanagar%20-%20Google%20Search&imagekey=!1e10!2sAF1QipPRC0RHbfzojdfzQP7JbMTiiAZoEl7-PO44bdbA&sa=X&ved=0ahUKEwi3uemU7dbVAhXMKY8KHWduDegQoioIdzAK" target="_blank"><img src="<?=_IMAGES?>jaynagar.jpg" alt="Jaynagar"><i class="fa fa-circle-o-notch" aria-hidden="true"></i> </a>
               </div>
            </section>
         </div>
      </div>



      <div class="row">
         <div class="col-md-6">
            <section class="section-type-1">
               <h2 class="ui-title-block-2">Send us an Email:</h2>
               <div class="forms">
                  <form id="frmenquiry" name="frmenquiry" action="<?=_URL?>contact" method="post" class="form-contacts ui-form">
                     <div class="row">
                        <div class="col-xs-12">
                           <input type="text" name="name" value="<?=(!empty($_SESSION['name'])?$_SESSION['name']:'')?>" placeholder="Your Name*" class="form-control">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <input type="email" name="email" value="<?=(!empty($_SESSION['email'])?$_SESSION['email']:'')?>" placeholder="Email*" class="form-control">
                           <input type="text" name="product_code" value="<?=$obj_product->product_code?>" placeholder="Product Code" class="form-control">
                        </div>
                        <div class="col-md-6">
                           <input type="tel" name="phone" value="<?=(!empty($_SESSION['phone'])?$_SESSION['phone']:'')?>" placeholder="Phone" class="form-control">
                           <input type="text" name="subject" placeholder="Subject" class="form-control last-block_mrg-btn_0">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                           <textarea name="message" rows="3" placeholder="Enter your message*" class="form-control"></textarea>
                           <button type="submit" class="btn btn-primary btn-lg btn-effect">Send Message</button>
                        </div>
                     </div>
                  </form>
               </div>
         </section>
         </div>
         <div class="col-md-6 col-md-offset-1">

         </div>
      </div>

   </div>
</div>


<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyA7IZt-36CgqSGDFK8pChUdQXFyKIhpMBY&sensor=true" type="text/javascript"></script>
<script type="text/javascript">
   var map;
   var geocoder;
   var marker;
   var people = new Array();
   var latlng;
   var infowindow;
   $(document).ready(function() {
      ViewCustInGoogleMap();
   });
   function ViewCustInGoogleMap() {
      var mapOptions = {
         center: new google.maps.LatLng(12.964779, 77.594908),
         zoom: 13,
         mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(document.getElementById("mapCanvas"), mapOptions);

      var data = '[{ "DisplayText": "<div class=\'markerTitle\'>Raj Diamonds, MG Road</div>", "ADDRESS": "<div class=\'markerDetail\'>#52, The Grandiose, MG Road, Bengaluru - 560 001</div>", "LatitudeLongitude": "12.9749247,77.6072112", "MarkerId": "Customer" },{ "DisplayText": "<div class=\'markerTitle\'>Raj Diamonds, Jayanagar</div>", "ADDRESS": "<div class=\'markerDetail\'>#701, 11th Main, 33rd Cross, 4th Block, Jayanagar, Bengaluru - 560 011</div>", "LatitudeLongitude": "12.9506414,77.5788632", "MarkerId": "Customer"}]';
      people = JSON.parse(data);
      for (var i = 0; i < people.length; i++) {
         setMarker(people[i]);
      }
   }

   function setMarker(people) {
      geocoder = new google.maps.Geocoder();
      infowindow = new google.maps.InfoWindow();
      if ((people["LatitudeLongitude"] == null) || (people["LatitudeLongitude"] == 'null') || (people["LatitudeLongitude"] == '')) {
         geocoder.geocode({ 'address': people["Address"] }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
               latlng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
               marker = new google.maps.Marker({
                  position: latlng,
                  map: map,
                  draggable: false,
                  html: people["DisplayText"]+'<br>'+people["ADDRESS"],
                  icon: "images/marker/" + people["MarkerId"] + ".png"
               });
               google.maps.event.addListener(marker, 'click', function(event) {
                  infowindow.setContent(this.html);
                  infowindow.setPosition(event.latLng);
                  infowindow.open(map, this);
               });
            }
            else {
               alert(people["DisplayText"] + " -- " + people["Address"] + ". This address couldn't be found");
            }
         });
      }
      else {
         var latlngStr = people["LatitudeLongitude"].split(",");
         var lat = parseFloat(latlngStr[0]);
         var lng = parseFloat(latlngStr[1]);
         latlng = new google.maps.LatLng(lat, lng);
         marker = new google.maps.Marker({
            position: latlng,
            map: map,
            draggable: false,               // cant drag it
            html: people["DisplayText"]+people["ADDRESS"],
            icon: "<?=_IMAGES?>logo-30x30.png"       // Give ur own image
         });
         google.maps.event.addListener(marker, 'click', function(event) {
            infowindow.setContent(this.html);
            infowindow.setPosition(event.latLng);
            infowindow.open(map, this);
         });
      }
   }
 </script>