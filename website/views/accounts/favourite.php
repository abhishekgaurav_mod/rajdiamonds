<div class="wrap-content">
	<div class="b-title-page b-title-page_w_bg"> </div>
	<div class="container">
		<div class="row">
			<nav class="col-md-3 woocommerce-MyAccount-navigation">
				<ul>
					<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active">
						<a href="<?=_URL?>accounts">Dashboard</a>
					</li>
					<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders">
						<a href="<?=_URL?>accounts/favourite">My Favourite</a>
					</li>
					<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account">
						<a href="<?=_URL?>accounts/profile">Account Details</a>
					</li>
					<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout">
						<a href="<?=_URL?>accounts/logout">Logout</a>
					</li>
				</ul>
			</nav>
			<div class="col-md-9 woocommerce-MyAccount-content">
				<div class="l-main-content productListing">
					<div class="b-goods-catalog">
					 <div id="errorMsg" ></div>
					 <?php if(!empty($arr_favtProduct)){
					          foreach ($arr_favtProduct as $key => $product){ ?>
					             <section class="b-goods b-goods_mod-a b-goods_3-col">
					                <div class="b-goods__inner">
					                   <a href="<?=_URL?><?=$product->category_slug?>/<?=$product->product_slug?>" class="b-goods__img">
					                      <img src="<?=_PRODUCT_ORG_URL?><?=$product->product_thumb?>" alt="<?=$product->product_name?>" class="img-responsive"/>
					                   </a>

                                       	<div class="b-goods__wrap">
                                          	<a href="<?=_URL?><?=$product->category_slug?>/<?=$product->product_slug?>">
												<div class="b-goods__category"><?=$product->category_name?></div>
												<h3 class="b-goods__name"><?=$product->product_name?></h3>
												<div class="b-goods__description">Praesent quis vestibulum risus. Suspendisse non malesuada risus, ut venenatis nisi. Quisque aliquam justo in est tempor malesuada ac eu sem.</div>
												<div class="b-goods__price"><?=$product->product_code?></div>
                                          	</a>

                                            <!-- <a href="javascript:void(0);" class="addfavourite <?=in_array($product->product_id, $arr_favtId)?'markedFavt':''?> b-goods-links__item" data-productid="<?=$product->product_id?>"><i class="icon fa fa-heart-o"></i></a> -->
                                        </div>
					                </div>
					             </section>
					    <?php }
					    } else { ?>
					    	<h3>Currently you have empty favourite list. </h3>
					    <?php } ?>

					</div>
               </div>
			</div>
		</div>
	</div>
</div>