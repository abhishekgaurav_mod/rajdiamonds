<div class="wrap-content">
	<div class="b-title-page b-title-page_w_bg"> </div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 ">
				<div class="woocommerce">
					<nav class="woocommerce-MyAccount-navigation">
						<ul>
							<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active">
								<a href="<?=_URL?>accounts">Dashboard</a>
							</li>
							<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders">
								<a href="<?=_URL?>accounts/favourite">My Favourite</a>
							</li>
							<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account">
								<a href="<?=_URL?>accounts/profile">Account Details</a>
							</li>
							<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout">
								<a href="<?=_URL?>accounts/logout">Logout</a>
							</li>
						</ul>
					</nav>
					<div class="woocommerce-MyAccount-content">
						
						<div class="woocommerce">
							<h2>Update Profile</h2>
							<div class="forms">
								<form name="frmprofile" id="frmprofile" action="<?=_URL?>accounts/profile" method="post" enctype="multipart/form-data" class="login">
						            <div class="col2-set" id="customer_details">
						                <div class="col-12">
						                  	<div class="woocommerce-billing-fields">
							                    <p class="form-row form-row form-row-wide ">
							                      	<label>Name <abbr class="required" title="required">*</abbr></label>
							                      	<input type="text" name="name" value="<?=$obj_user->name?>" class="input-text form-control" name="billing_first_name" placeholder="Name" >
							                    </p>
							                    <p class="form-row form-row form-row-first" >
							                      	<label>Email Id <abbr class="required" title="required">*</abbr></label>
							                      	<input type="email" name="email" value="<?=$obj_user->email?>" class="input-text form-control" placeholder="Email Id">
							                    </p>
							                    <p class="form-row form-row form-row-last" >
							                      	<label>Phone </label>
							                      	<input type="tel" name="phone" value="<?=$obj_user->phone?>" class="input-text form-control" placeholder="Phone">
							                    </p>
							                    <div class="clear"></div>
							                    <?php if ($obj_user->login_type == 'E') {?>
								                    <p class="form-row form-row form-row-first" >
								                      	<label>Password <abbr class="required" title="required">*</abbr></label>
								                      	<input type="password" name="password" class="input-text form-control" placeholder="Password">
								                    </p>
								                    <p class="form-row form-row form-row-last" >
								                      	<label>Repeat Password <abbr class="required" title="required">*</abbr></label>
								                      	<input type="password" name="retypepassword" class="input-text form-control" placeholder="Repeat Password">
								                    </p>
								                    <div class="clear"></div>
							                    <?php } ?>
							                    <p class="form-row form-row form-row-wide" >
							                      	<label>Address</label>
							                      	<input type="text" name="address" value="<?=$obj_user->address?>" class="input-text form-control" placeholder="Address">
							                    </p>
												<p class="form-row field">
													<button type="submit" name="login" value="Login" class="btn btn-primary btn-effect" >Update</button>
												</p>				                
						                  	</div>
						                </div>
						            </div>
						        </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>