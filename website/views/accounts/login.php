<div class="wrap-content">
	<div class="b-title-page b-title-page_w_bg"> </div>

	<div class="container">
		<div class="row">
			<div class="col-md-9 ">
				<div class="woocommerce">
					<h2>Login</h2>
					<div class="forms">
						<form name="frmlogin" id="frmlogin" action="<?=_URL?>accounts/login" method="post" class="login">
							<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
								<label for="username">Email address <span class="required">*</span></label>
								<input type="text" name="useremail" id="username" placeholder="Email id" class="woocommerce-Input form-control">
							</p>
							<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
								<label for="password">Password <span class="required">*</span></label>
								<input type="password" name="userpassword" id="password" placeholder="Password" class="woocommerce-Input woocommerce-Input--text input-text form-control" >
							</p>

							<div class="forms">
								<div class="col-md-6 ">
									<p class="form-row field">
										<button type="submit" name="login" value="Login" class="btn btn-primary btn-effect" >Login</button>
									</p>
								</div>
								<div class="col-md-6 ">
									<div class="form-row field">
										<div class="loginAsFb">
											<div class="fb-login-button" data-max-rows="1" data-scope="email" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
										</div>
										<div class="continueAsFb"><img src="" class="fbProfilePic"><span class="box"><i class="fa fa-facebook-official" aria-hidden="true"></i> <span>Continue with Facebook</span></span></div>
										<div id="status"> </div>
									</div>
								</div>
							</div>
							<!-- <p class="woocommerce-LostPassword lost_password">
								<a href="lost-password.html" class="pink">Forgot password?</a>
							</p> -->
							<p class="form-row field">
								<div class="woocommerce-info">Don't have an account ? <a href="<?=_URL?>accounts/register" class="pink">Click here</a> to register.</div>
							</p>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-4">
			  	<aside class="l-sidebar sidebar_right">
			        <?php if (!empty($arr_category)) { ?>
			                <section class="widget">
			                  	<h2>Categories</h2>
			                  	<div class="ui-decor-1"></div>
			                  	<div class="widget-content">
				                    <ul class="widget-list list">
				                        <?php   foreach ($arr_category as $key => $category) { ?>
			                      				<li class="widget-list__item">
			                      					<a href="<?=_URL?><?=$category->category_slug?>" class="widget-list__link"><!-- <span class="badge pull-right">98</span> --> <?=$category->category_name?></a>
			                      				</li>
				                         <?php  } ?>
				                    </ul>
			                  	</div>
			                </section>
			        <?php } ?>

			        <?php if (!empty($arr_category)) { ?>
			                <section class="widget">
								<h2>Tags</h2>
								<div class="ui-decor-1"></div>
								<div class="widget-content">
				                    <ul class="list-tags list-unstyled">
				                        <?php   foreach ($arr_tag as $key => $tag) { ?>
													<li class="list-tags__item"><a href="<?=_URL?><?=$tag->tag_slug?>" class="list-tags__link"><?=$tag->tag_name?></a></li>
				                         <?php  } ?>
				                    </ul>
								</div>
			                </section>
			        <?php } ?>
			  	</aside>              
			</div>
		</div>
	</div>
</div>

<script>


  window.fbAsyncInit = function() {
	  FB.init({
	    appId      : '156282174926267',
	    channelUrl : 'http://www.***.com/', // Channel File
	    status     : true, // check login status
	    cookie     : true,  // enable cookies to allow the server to access the session
	    xfbml      : true,  // parse social plugins on this page
	    version    : 'v2.8' // use graph api version 2.8
	  });

	FB.Event.subscribe('auth.login', function(){
	    window.location.href = _URL+'accounts/login';
	});

	  FB.getLoginStatus(function(response) {
	    statusChangeCallback(response);
	  });

  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  function statusChangeCallback(response) {

	    if (response.status === 'connected') {
	      // Logged into your app and Facebook.
	      testAPI();
	      $('.loginAsFb').hide();
	    } else {
	      // The person is not logged into your app or we are unable to tell.
	      /*document.getElementById('status').innerHTML = 'Please log ' +
	        'into this app.';*/
	        $('.continueAsFb').hide();
	    }
	 
  }

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  function testAPI() {
    FB.api('/me?fields=name,first_name,middle_name,last_name,email,link,gender,picture,birthday,about,hometown,languages,location,age_range,relationship_status,religion,cover', function(response) {
      /*document.getElementById('status').innerHTML =
        'Thanks for logging in !<br> Name = ' + response.name + '<br>'+
        'ID = '+response.id + '<br>'+
        'email = '+response.email + '<br>';*/

	$(".fbProfilePic").attr("src", response.picture.data.url);
	$('.continueAsFb').click(function(){
		
        $.post( _URL+"accounts/fblogin",
	    {
	        name: response.name,
	        email: response.email,
	        first_name: response.first_name,
	        middle_name: response.middle_name,
	        last_name: response.last_name,
	        gender: response.gender,
	        age_range: response.age_range.min,
	        social_link: response.link,
	        socialmedia_id: response.id
	    },
	    function(data, status){
	        console.log("Status: " + status);
	        window.location.href = _URL;
	    });
	});

    });

  }
</script>