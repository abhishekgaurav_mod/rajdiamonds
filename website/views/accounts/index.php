<div class="wrap-content">
	<div class="pageBanner commonBanner desktop">
		<img src="<?=_IMAGES?>raj-banner.jpg">
	</div>
	<div class="pageBanner commonBanner mobile">
		<img src="<?=_IMAGES?>raj-banner.jpg">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12 ">
				<div class="woocommerce">
					<nav class="woocommerce-MyAccount-navigation">
						<ul>
							<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--dashboard is-active">
								<a href="<?=_URL?>accounts">Dashboard</a>
							</li>
							<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--orders">
								<a href="<?=_URL?>accounts/favourite">My Favourite</a>
							</li>
							<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--edit-account">
								<a href="<?=_URL?>accounts/profile">Account Details</a>
							</li>
							<li class="woocommerce-MyAccount-navigation-link woocommerce-MyAccount-navigation-link--customer-logout">
								<a href="<?=_URL?>accounts/logout">Logout</a>
							</li>
						</ul>
					</nav>
					<div class="woocommerce-MyAccount-content">
						<p> Dear <strong><?=$obj_user->name?></strong></p><br>
						<p>Thank you for signing up with Raj Diamonds.</p><br>
						<p>
							Welcome to your account dashboard - access your <a href="<?=_URL?>accounts/favourite" class="pink"><u>Favourite Jewelleries</u></a>, update your <a href="<?=_URL?>accounts/profile" class="pink"><u>Profile details</u></a> and edit your <a href="<?=_URL?>accounts/profile" class="pink"><u>Password</u></a>.
						</p><br>
						<p>(Not <?=$obj_user->name?>? <a href="<?=_URL?>accounts/logout" class="pink"><u>Logout</u></a>)</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>