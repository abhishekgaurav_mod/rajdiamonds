<div class="right_col" role="main">
  	<div class="">
	    <div class="page-title">
	      	<div class="title_left">
	        	<h3><?=ucfirst($this->pagetitle)?></h3>
	      	</div>
	    </div>
	    <div class="clearfix"></div>	    

	    <div class="row">
	      	<div class="col-md-6 col-xs-12">
		        <div class="x_panel">
					<div class="x_content">
						<div class="forms addslider">

							<form name="frmaddslider" id="frmaddslider" action="<?=_URL?>accounts/changepassword" method="post">
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="newpassword">New Password</label>
									<input name="newpassword" type="password" id="newpassword" placeholder="New password" maxlength="20" class="form-control has-feedback-left" />
                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>
								<div class="col-md-12 col-sm-12 col-xs-12 form-group has-feedback">
									<label for="confirmpassword">Confrim Password</label>
									<input name="confirmpassword" type="password" id="confirmpassword" placeholder="Confrim password" maxlength="20" class="form-control has-feedback-left" />
                        			<span class="fa fa-edit form-control-feedback left" aria-hidden="true"></span>
								</div>

								<div class="clearfix"></div>
		                      	<div class="ln_solid"></div>
		                      	<div class="form-group">
									<div class="field col-md-9 col-sm-9 col-xs-12">
										<span class="loading">Please wait...</span>
										<button type="submit" name="save" value="Save" class=" inline btn btn-success" >Save</button>
									</div>
		                      	</div>
							</form>

						</div>		            
		          	</div>
		        </div>
	      	</div>

	    </div>
  	</div>
</div>
<div class="clearfix"></div>
