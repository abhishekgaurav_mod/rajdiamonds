<div class="wrap-content">
	<div class="b-title-page b-title-page_w_bg"> </div>

	<div class="container">
		<div class="row">
			<div class="col-md-9 ">
				<div class="woocommerce">
					<h2>Register</h2>
					<div class="forms">
						<form name="frmregister" id="frmregister" action="<?=_URL?>accounts/register" method="post" enctype="multipart/form-data" class="login">
				            <div class="col2-set" id="customer_details">
				                <div class="col-12">
				                  	<div class="woocommerce-billing-fields">
					                    <p class="form-row form-row form-row-wide ">
					                      	<label>Name <abbr class="required" title="required">*</abbr></label>
					                      	<input type="text" name="name" class="input-text form-control" name="billing_first_name" placeholder="Name" >
					                    </p>
					                    <p class="form-row form-row form-row-first" >
					                      	<label>Email Id <abbr class="required" title="required">*</abbr></label>
					                      	<input type="email" name="email" class="input-text form-control" placeholder="Email Id">
					                    </p>
					                    <p class="form-row form-row form-row-last" >
					                      	<label>Phone </label>
					                      	<input type="tel" name="phone" class="input-text form-control" placeholder="Phone">
					                    </p>
					                    <div class="clear"></div>
					                    <p class="form-row form-row form-row-first" >
					                      	<label>Password <abbr class="required" title="required">*</abbr></label>
					                      	<input type="password" name="password" class="input-text form-control" placeholder="Password">
					                    </p>
					                    <p class="form-row form-row form-row-last" >
					                      	<label>Repeat Password <abbr class="required" title="required">*</abbr></label>
					                      	<input type="password" name="retypepassword" class="input-text form-control" placeholder="Repeat Password">
					                    </p>
					                    <div class="clear"></div>
					                    <p class="form-row form-row form-row-wide" >
					                      	<label>Address</label>
					                      	<input type="text" name="address" class="input-text form-control" placeholder="Address">
					                    </p>
										<p class="form-row field">
											<button type="submit" name="login" value="Login" class="btn btn-primary btn-effect" >Register</button>
										</p>				                
				                  	</div>
				                </div>
				            </div>
				        </form>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-4">
			  	<aside class="l-sidebar sidebar_right">
			        <?php if (!empty($arr_category)) { ?>
			                <section class="widget">
			                  	<h2>Categories</h2>
			                  	<div class="ui-decor-1"></div>
			                  	<div class="widget-content">
				                    <ul class="widget-list list">
				                        <?php   foreach ($arr_category as $key => $category) { ?>
			                      				<li class="widget-list__item">
			                      					<a href="<?=_URL?><?=$category->category_slug?>" class="widget-list__link"><!-- <span class="badge pull-right">98</span> --> <?=$category->category_name?></a>
			                      				</li>
				                         <?php  } ?>
				                    </ul>
			                  	</div>
			                </section>
			        <?php } ?>

			        <?php if (!empty($arr_category)) { ?>
			                <section class="widget">
			                  <h2>Tags</h2>
			                  <div class="ui-decor-1"></div>
			                  <div class="widget-content">
			                    <ul class="list-tags list-unstyled">
			                        <?php   foreach ($arr_tag as $key => $tag) { ?>
												<li class="list-tags__item"><a href="<?=_URL?><?=$tag->tag_slug?>" class="list-tags__link"><?=$tag->tag_name?></a></li>
			                         <?php  } ?>
			                    </ul>
			                  </div>
			                </section>
			        <?php } ?>
			  	</aside>              
			</div>
		</div>
	</div>
</div>
