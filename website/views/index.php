<div class="homeBanner desktop">
<?php if(!empty($arr_sliders)) { ?>
   <ul class="homeBannerSlider">
      <?php foreach ($arr_sliders as $key => $slider) { ?>
         <li><img src="<?=_SLIDER_ORG_URL.'desktop-banner/'.$slider->desktop_banner?>" alt="Slider <?=($key+1)?>" /></li>
      <?php } ?>
   </ul>
<?php } ?>
</div>
<div class="homeBanner tablet480">
<?php if(!empty($arr_sliders)) { ?>
   <ul class="homeBannerSlider">
      <?php foreach ($arr_sliders as $key => $slider) { ?>
         <li><img src="<?=_SLIDER_ORG_URL.'tab-banner/'.$slider->tab_banner?>" alt="Slider <?=($key+1)?>" /></li>
      <?php } ?>
   </ul>
<?php } ?>
</div>
<div class="homeBanner deviceMobile">
<?php if(!empty($arr_sliders)) { ?>
   <ul class="homeBannerSlider">
      <?php foreach ($arr_sliders as $key => $slider) { ?>
         <li><img src="<?=_SLIDER_ORG_URL.'mobile-banner/'.$slider->mobile_banner?>" alt="Slider <?=($key+1)?>" /></li>
      <?php } ?>
   </ul>
<?php } ?>
</div>

<?php
   if(!empty($arr_featuredCategory)) { 
      $arrCount = count($arr_featuredCategory);
      foreach ($arr_featuredCategory as $key => $featuredCategory) { ?>
      <section class="section-area">
         <div class="block-table block-table_padd_10 <?=($key % 2 == 0)?'':'block-table_revers'?>">
            <div class="block-table__cell col-md-6 <?=($key % 2 == 0)?'col-md-push-6':''?>">
               <a href="<?=$featuredCategory->category_slug?>">
                  <div class="block-table__inner scrollreveal homeCategoryThumb" style="background-image: url('<?=_CATEGORY_ORG_URL?>thumb/<?=$featuredCategory->category_thumb?>');"></div>
               </a>
            </div>
            <div class="block-table__cell col-md-6 <?=($key % 2 == 0)?'col-md-pull-6':''?> text-center">
               <div class="block-table__inner">
                  <div class="main-flex-box">
                     <div class="flex-box-content">
                        <h2 class="ui-title-block ui-title-block_mod-a">
                           <span class="text-gradient"><span class=""><?=$featuredCategory->category_name?></span> </span>
                        </h2>
                        <div class="ui-subtitle-block"><?=$featuredCategory->category_description?></div>
                        <div data-min480="2" data-min768="3" data-min992="2" data-min1200="2" data-pagination="true" data-navigation="false" data-auto-play="4000" data-stop-on-hover="true" class="goods-carousel owl-carousel owl-theme enable-owl-carousel js-zoom-gallery ">

                           <?php if (!empty($featuredCategory->products)){
                                    foreach ($featuredCategory->products as $obj_product) { ?>
                                       <section class="b-goods">
                                          <div class="b-goods__inner">
                                             <a href="<?=_URL?><?=$featuredCategory->category_slug?>/<?=$obj_product->product_slug?>" class="b-goods__img">
                                                <img src="<?=_PRODUCT_ORG_URL?><?=$obj_product->product_thumb?>" alt="<?=$obj_product->product_name?>" class="img-responsive" />
                                             </a>
                                             <a href="<?=_URL?><?=$featuredCategory->category_slug?>/<?=$obj_product->product_slug?>">
                                                <!-- <div class="b-goods__category"><?=$featuredCategory->category_name?></div> -->
                                                <h3 class="b-goods__name"><?=$obj_product->product_name?></h3>
                                                <div class="b-goods__price"><?=$obj_product->product_code?></div>
                                             </a>
                                          </div>
                                       </section>
                           <?php    }
                                 }
                           ?>

                        </div>
                        <a href="<?=$featuredCategory->category_slug?>" class="b-goods-carousel__btn-link btn-link">view all</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- <?php if (($key+1) != $arrCount) { ?>
         <div class="crownBorder"><span class="crown"></span></div>
      <?php } ?> -->
<?php } 
   }
?>