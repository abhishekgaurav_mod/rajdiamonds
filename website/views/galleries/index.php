<div id="page-title">

	<div class="title-bar dark bg-black padded-vertical-60">
        <div class="bg-image bg-image-3"></div>
        <div class="overlay overlay-black editable-alpha" data-alpha="70"></div> 
        <div class="container">	
            <span class="sep sep-primary sep-no-top"></span>
            <h2 class="text-uppercase margin-bottom-0"><strong>Galleries</strong></h2>
            <!-- <span class="text-xs text-muted text-uppercase">Lorem Ipsum is a dummy text for dummy content</span> -->
        </div>
    </div>

   <?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

</div>

<div id="content">
	<div class="container">
    	<div class="row padded-top-40">
    		<?php if (!empty($arr_gallery)) { ?>
					<ul class="gallery">
		          	<?php foreach ($arr_gallery as $key => $image) { ?>
							<li class="item"><img src="<?=_GALLERY_ORG_URL.$image->imagepath?>" alt="<?=$image->title?>" class="img-responsive" /></li>
            		<?php } ?>
					</ul>
					<div id="galleryPager">
			          	<?php foreach ($arr_gallery as $key => $image) { ?>
							<a data-slide-index="<?=$key?>" href=""><img src="<?=_GALLERY_ORG_URL.$image->imagepath?>" alt="thumb-<?=$image->title?>" /></a>
	            		<?php } ?>
					</div>
            <?php 	} ?>
        </div>
    </div>  

    <?php  include _STATIC_BLOCK.'_getQuestionCenter.php'; ?>

</div>
