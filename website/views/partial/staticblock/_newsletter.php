<div class="b-form-newsletter">
	<div class="forms">
		<form id="frmregister" name="frmregister" action="<?=_URL?>newsletters" method="POST" class="b-form-newsletter__form">
			<div class="b-form-newsletter__title">Newsletter</div>
			<input type="text" value="<?=(!empty($_SESSION['email'])?$_SESSION['email']:'')?>" placeholder="Enter your email" class="form-control"/>
			<button type="submit" class="btn btn-primary btn-effect">Subscribe</button>
		</form>
	</div>
</div>