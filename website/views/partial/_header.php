<?php include _PARTIAL_ROOT.'_metaHeader.php'; ?>
<div>
	<div class="header-search open-search">
	  	<div class="container">
		    <div class="row">
		      	<div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
			        <div class="navbar-search">
			          	<form class="search-global">
				            <input type="text" placeholder="Type to search" autocomplete="off" name="s" value="" class="search-global__input"/>
				            <button class="search-global__btn"><i class="icon fa fa-search"></i></button>
				            <div class="search-global__note">Begin typing your search above and press return to search.</div>
			          	</form>
			        </div>
		      	</div>
		    </div>
	  	</div>
	  	<button type="button" class="search-close close"><i class="fa fa-times"></i></button>
	</div>

	<div class="mobile">
	    <?php include _PARTIAL_ROOT.'_menuMobile.php'; ?>
	</div>   
	<div class="desktop">
	    <?php include _PARTIAL_ROOT.'_menuDesktop.php'; ?>
	</div>
</div>

<div class="wrap-content">