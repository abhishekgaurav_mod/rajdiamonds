<!-- Scripts -->
<script type="text/javascript" src="<?=_JS?>custom/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?=_JS?>custom/jquery.form.js"></script>
<script type="text/javascript" src="<?=_JS?>custom/notify.min.js"></script>


<script type="text/javascript" src="<?=_WEBROOT?>libs/jquery-migrate-1.2.1.js"></script>
<!-- Bootstrap-->
<script type="text/javascript" src="<?=_WEBROOT?>libs/bootstrap/bootstrap.min.js"></script>
<!-- User customization-->
<script type="text/javascript" src="<?=_JS?>custom.js"></script>
<!-- Other slider-->
<script type="text/javascript" src="<?=_PLUGINS?>owl-carousel/owl.carousel.min.js"></script>
<!-- Pop-up window-->
<script type="text/javascript" src="<?=_PLUGINS?>magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- Headers scripts-->
<script type="text/javascript" src="<?=_PLUGINS?>headers/slidebar.js"></script>
<script type="text/javascript" src="<?=_PLUGINS?>headers/header.js"></script>
<!-- Select customization-->
<script type="text/javascript" src="<?=_PLUGINS?>bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- Mail scripts-->
<!-- <script type="text/javascript" src="<?=_PLUGINS?>jqBootstrapValidation.js"></script>
<script type="text/javascript" src="<?=_PLUGINS?>contact_me.js"></script> -->
<!-- Filter and sorting images-->
<script type="text/javascript" src="<?=_PLUGINS?>isotope/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?=_PLUGINS?>isotope/imagesLoaded.js"></script>
<!-- Shuffle-->
<script type="text/javascript" src="<?=_PLUGINS?>letters/jquery.shuffleLetters.js"></script>
<!-- Progress numbers-->
<script type="text/javascript" src="<?=_PLUGINS?>rendro-easy-pie-chart/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="<?=_PLUGINS?>rendro-easy-pie-chart/waypoints.min.js"></script>
<!-- Animations-->
<script type="text/javascript" src="<?=_PLUGINS?>scrollreveal/scrollreveal.min.js"></script>
<!-- Main slider-->
<script type="text/javascript" src="<?=_PLUGINS?>slider-pro/jquery.sliderPro.js"></script>
    

<script type="text/javascript" src="<?=_PLUGINS?>leanModal/jquery.leanModal.min.js"></script>

<!-- Custom Script -->
<script type="text/javascript" src="<?=_JS?>custom/custom-script.js"></script>

</body>

</html>