<div class="desktop desktopHeader">
    <div class="header transparentHeader navbar-fixed-top header-navbar-center flex flexJustifyBetween flexAlignItemsCenter flexWrap opaqueHeader">
      
        <div class="mainLogo">
            <a href="<?=_URL?>">
                <div class="mainLogoBox">
                    <img src="<?=_IMAGES?>raj-diamonds-logo-light.svg" alt="Raj Diamonds" class="lightLogo" />
                    <img src="<?=_IMAGES?>raj-diamonds-logo-dark.svg" alt="Raj Diamonds" class="darkLogo" />
                </div>
            </a>
        </div>
        <div class="flex flexJustifyBetween flexAlignItemsCenter flexWrap">
            <ul class="nav nav-main text-uppercase dropdown-dark">
                <li class="has-dropdown">
                    <a href="#">Collections</a>
                    <div class="nav-main-dropdown">
                        <ul>
                            <li><a href="<?=_URL?>necklaces"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Necklaces</a></li>
                            <li><a href="<?=_URL?>rings"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Rings</a></li>
                            <li><a href="<?=_URL?>earrings"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Earrings</a></li>
                            <li><a href="<?=_URL?>pendants"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Pendants</a></li>
                            <li><a href="<?=_URL?>bangles-and-bracelets"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Bangles &amp; Bracelets</a></li>
                            <li><a href="<?=_URL?>ancient-splendour"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Ancient splendour</a></li>
                            <li><a href="<?=_URL?>diamond-accessories"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Diamond Accessories</a></li>
                            <!-- <li><a href="<?=_URL?>diya-collections"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Diya Collections</a></li> -->
                        </ul>
                    </div>
                </li>
                <li class="has-dropdown">
                    <a href="#">Occasions</a>
                    <div class="nav-main-dropdown">
                        <ul>
                            <li><a href="<?=_URL?>artisan-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Artisan Jewellery</a></li>
                            <li><a href="<?=_URL?>bridal-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Bridal jewellery</a></li>
                            <li><a href="<?=_URL?>cocktail-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Cocktail jewellery</a></li>
                            <li><a href="<?=_URL?>daily-wear-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Daily wear jewellery</a></li>
                            <li><a href="<?=_URL?>traditional-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Traditional jewellery</a></li>
                            <!-- <li><a href="<?=_URL?>tiny-trinkets"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Tiny Trinkets</a></li> -->
                        </ul>
                    </div>
                </li>
                <li class="has-dropdown">
                    <a href="#">Diamonds</a>
                    <div class="nav-main-dropdown">
                        <ul>
                            <!-- <li class="has-dropdown">
                                <a href="#"><i class="icon-before fa fa-star pink"></i> &nbsp; BUYING TIPS</a>
                                <ul>
                                    <li><a href="<?=_URL?>"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; DIAMOND SHAPES</a></li>
                                    <li><a href="<?=_URL?>"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; CARAT</a></li>
                                    <li><a href="<?=_URL?>"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; COLOR</a></li>
                                    <li><a href="<?=_URL?>"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; CLARITY</a></li>
                                    <li><a href="<?=_URL?>"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; CUT</a></li>
                                </ul>
                            </li> -->
                            <li><a href="<?=_URL?>the-four-cs"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; The Four Cs</a></li>
                            <li><a href="<?=_URL?>care-and-storage"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Care & Storage</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="<?=_URL?>our-legacy">About</a></li>
                <li><a href="<?=_URL?>contact-us">Contact Us</a></li>
            </ul>
            <div class="menuOption">
                <div class="flex ">
                    <!-- <div class="item">
                        <a href="#" class="btn_header_search"><i class="fa fa-search" aria-hidden="true"></i></a>
                    </div> -->
                    <div class="item">
                        <a href="<?=_URL?>accounts/favourite"><i class="icon fa fa-heart-o normal"></i><i class="fa fa-heart hover" aria-hidden="true"></i> </a>
                    </div>
                    <?php if (!empty($_SESSION['userId'])) { ?>
                        <div class="item">
                            <a href="<?=_URL?>accounts"><i class="fa fa-user-o" aria-hidden="true"></i> <?=explode(' ', trim($_SESSION['name']))[0]?></a>
                        </div>
                    <?php } else { ?>
                    <div class="item">
                        <a href="<?=_URL?>accounts/login"><i class="fa fa-user-o" aria-hidden="true"> Login</i></a>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php if ($currentUrl != _URL) { ?>
            <div class="falseHeaderPadding"></div>
    <?php } ?>
</div>
