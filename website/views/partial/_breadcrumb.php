<div class="row">
   <div class="col-xs-12">
      <ol class="breadcrumb">
         <li><a href="<?=_URL?>">Home</a></li>
         <?php $navLink = _URL;
               foreach ($nav as $key=>$slug) {
                  $navLink = $navLink.$slug.'/';
                  if ($key == 1) { ?>
                     <li class="<?=($slug == end($nav))?'active':''?>"><a href="<?=$navLink?>"><?=(substr(strstr(ucwords(str_replace('-', ' ', $slug))," "), 1))?></a></li>
         <?php    } else { ?>
                     <li class="<?=($slug == end($nav))?'active':''?>"><a href="<?=$navLink?>"><?=(ucwords(str_replace('-', ' ', $slug)))?></a></li>
         <?php    }
               }?>
      </ol>
   </div>
</div>
