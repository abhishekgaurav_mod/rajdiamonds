	<!-- <?php  include _STATIC_BLOCK.'_newsletter.php'; ?> -->
</div>

<footer class="footer footer-type-1">
	<div class="footer__main">
		<div class="">
			<div class="col-md-6 col-md-push-6">
				<div class="row">
					<div class="col-md-4">
						<section class="footer-section">
							<!-- <h3 class="footer-section__title">About</h3>
							<ul class="footer__list list-unstyled">
								<li><a href="about.html">About Us</a></li>
								<li><a href="home.html">Careers</a></li>
								<li><a href="home.html">International</a></li>
							</ul> -->
						</section>
					</div>
					<div class="col-md-4">
						<section class="footer-section">
							<ul class="footer__list list-unstyled">
								<li><a href="https://www.facebook.com/rajdiamondsmg" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a></li>
								<!-- <li><a href="<?=_URL?>">Terms &amp; Conditions</a></li> -->
							</ul>
						</section>
					</div>
					<div class="col-md-4">
						<section class="footer-section">
							<ul class="footer__list list-unstyled">
								<li><a href="https://www.instagram.com/rajdiamonds/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
								<!-- <li><a href="<?=_URL?>">Privacy Policy</a></li> -->
							</ul>
						</section>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-md-pull-6">
				<div class="footerLogo">
		            <a href="<?=_URL?>">
		                <img src="<?=_IMAGES?>raj-diamonds-logo-light.svg" alt="Raj Diamonds" class="darkLogo" />
		            </a>
		        </div>
				<!-- end social-list-->
				<div class="copyright">&copy; 2017 Raj Diamonds &reg; All Right Reserved</div>
			</div>
		</div>
	</div>
</footer>


<?php include _PARTIAL_ROOT.'_footerScript.php'; ?>