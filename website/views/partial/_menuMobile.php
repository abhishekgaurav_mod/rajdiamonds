<div class="mobileExpand" data-off-canvas="mobile-slidebar left overlay">
    <a href="<?=_URL?>" class="mainLogoMobile">
        <img src="<?=_IMAGES?>raj-diamonds-logo-light.svg" alt="Raj Diamonds" class="darkLogo" />
    </a>
    <ul class="nav navbar-nav">
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Collections<b class="caret"></b> </a>
            <ul role="menu" class="dropdown-menu">
                <li><a href="<?=_URL?>necklaces"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Necklaces</a></li>
                <li><a href="<?=_URL?>rings"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Rings</a></li>
                <li><a href="<?=_URL?>earrings"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Earrings</a></li>
                <li><a href="<?=_URL?>pendants"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Pendants</a></li>
                <li><a href="<?=_URL?>bangles-and-bracelets"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Bangles &amp; Bracelets</a></li>
                <li><a href="<?=_URL?>ancient-splendour"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Ancient splendour</a></li>
                <li><a href="<?=_URL?>diamond-accessories"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Diamond Accessories</a></li>
                <!-- <li><a href="<?=_URL?>diya-collections"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Diya Collections</a></li> -->
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Occasions<b class="caret"></b> </a>
            <ul role="menu" class="dropdown-menu">
                <li><a href="<?=_URL?>artisan-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Artisan Jewellery</a></li>
                <li><a href="<?=_URL?>bridal-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Bridal jewellery</a></li>
                <li><a href="<?=_URL?>cocktail-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Cocktail jewellery</a></li>
                <li><a href="<?=_URL?>daily-wear-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Daily wear jewellery</a></li>
                <li><a href="<?=_URL?>traditional-jewellery"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Traditional jewellery</a></li>
                <!-- <li><a href="<?=_URL?>tiny-trinkets"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Tiny Trinkets</a></li> -->
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Diamonds<b class="caret"></b> </a>
            <ul role="menu" class="dropdown-menu">
                <!-- <li>
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">BUYING TIPS<b class="caret"></b> </a>                    
                        <ul role="menu" class="dropdown-menu">
                            <li><a href="<?=_URL?>" tabindex="-1">DIAMOND SHAPES</a></li>
                            <li><a href="<?=_URL?>" tabindex="-1">CARAT</a></li>
                            <li><a href="<?=_URL?>" tabindex="-1">COLOR</a></li>
                            <li><a href="<?=_URL?>" tabindex="-1">CLARITY</a></li>
                            <li><a href="<?=_URL?>" tabindex="-1">CUT</a></li>
                        </ul>
                </li> -->
                <li><a href="<?=_URL?>the-four-cs"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; The Four Cs</a></li>
                <li><a href="<?=_URL?>care-and-storage"><i class="fa fa-diamond pink" aria-hidden="true"></i> &nbsp; Care & Storage</a></li>
            </ul>
        </li>
        <li><a href="<?=_URL?>our-legacy">About</a></li>
        <li><a href="<?=_URL?>contact-us">Contact Us</a></li>
        <?php if (!empty($_SESSION['userId'])) { ?>
            <li><a href="<?=_URL?>accounts"><i class="fa fa-user-o" aria-hidden="true"></i> &nbsp; <?=explode(' ', trim($_SESSION['name']))[0]?></a></li>
        <?php } else { ?>
            <li><a href="<?=_URL?>accounts/login"><i class="fa fa-user-o" aria-hidden="true"> &nbsp; Login</i></a></li>
        <?php } ?>
    </ul>
</div>

<div>
    <header class="header header-logo-black ">
      
        <div class="flex flexJustifyBetween flexAlignItemsCenter">
            <div>
                <a href="<?=_URL?>" class="mainLogoMobile">
                    <img src="<?=_IMAGES?>raj-diamonds-logo-light.svg" alt="Raj Diamonds" class="darkLogo" />
                </a>
            </div>
            <div>
                <button class="js-toggle-mobile-slidebar toggle-menu-button">
                    <i class="toggle-menu-button-icon"><span></span><span></span><span></span><span></span><span></span><span></span></i>
                </button>
            </div>
        </div>
    </header>
</div>