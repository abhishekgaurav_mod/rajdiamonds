<?php
$currentUrl = "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
$uri = str_replace(_URL, '', $currentUrl);
$uri = str_replace('.php', '', $uri);
$uri = trim($uri, '/');
$uri = explode('?', $uri);
$uri = $uri[0];
$nav = explode('/', $uri);
?>
<!DOCTYPE html>
<html lang="en">
<head>

<!-- Title -->
<title><?=ucwords(!empty($this->metatitle)?$this->metatitle:_TITLE)?> - Raj Diamonds</title>

<!-- Meta -->
<meta charset="utf-8"/>
<meta http-equiv="x-ua-compatible" content="ie=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta content="telephone=no" name="format-detection"/>
<meta name="HandheldFriendly" content="true"/>

<meta name="keywords" content="<?=(!empty($this->metakeyword)?$this->metakeyword.',Raj Diamonds':'Raj Diamonds')?>">
<meta name="description" content="<?=(!empty($this->metadescription)?$this->metadescription:_TITLE)?>">

<!-- Favicons -->
<link rel="shortcut icon" href="<?=_IMAGES?>favicon.png">
<link rel="apple-touch-icon" href="<?=_IMAGES?>favicon_60x60.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?=_IMAGES?>favicon_76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?=_IMAGES?>favicon_120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?=_IMAGES?>favicon_152x152.png">

<!-- Google Web Fonts -->
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Quattrocento:400,700" rel="stylesheet">

<!-- Stylesheets -->
<link rel="stylesheet" type="text/css" href="<?=_CSS?>master.css" />

<link rel="stylesheet" type="text/css" href="<?=_PLUGINS?>leanModal/leanModal.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>jquery.bxslider.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>notify.css" />
<link rel="stylesheet" type="text/css" href="<?=_CSS?>custom-style.css" />

<script type="text/javascript" src="<?=_JS?>custom/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
	var _URL			=	'<?=_URL?>';
	var _CURRENT_URL	=	window.location.href;
</script>
</head>
<body class="sticky-nav">
	<!-- FB SHARE CODE -->
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<!-- FB SHARE CODE -->