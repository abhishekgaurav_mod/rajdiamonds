<div class="wrap-content productPage">
   <div class="container">          
      <?php include _PARTIAL_ROOT.'_breadcrumb.php'; ?>
   </div>
   <div class="goods-card">
      <div class="container">
         <div id="errorMsg" ></div>
         
         <div class="row">
            <div class="b-goods-carousel">
               <div class="col-md-6">
                  <a href="<?=_PRODUCT_ORG_URL?><?=$obj_product->product_thumb?>" class="js-zoom-images">
                     <img src="<?=_PRODUCT_ORG_URL?><?=$obj_product->product_thumb?>" alt="<?=$obj_product->product_name?>" class="img-responsive" />
                  </a>
               </div>

               <div class="col-lg-5 col-lg-offset-1 col-md-5">
                  <div class="productDetails">
                     <h3 class="productName"><?=$obj_product->product_name?></h3>
                     <!-- <div class="productCode"><?=$obj_product->product_code?> Code</div> -->

                     <table class="productAttr">
                        <tr>
                           <td><span class="labal">Product Code</span></td>
                           <td>: <?=$obj_product->product_code?></td>
                        </tr>
                        <tr>
                           <td><span class="labal">Bar Code</span></td>
                           <td>: <?=!empty($obj_product->bar_code)?$obj_product->bar_code:'N/A'?></td>
                        </tr>
                        <tr>
                           <td><span class="labal">Category</span></td>
                           <td>: <a href="<?=_URL?><?=$obj_product->category_slug?>"><?=$obj_product->category_name?></a></td>
                        </tr>
                        <tr>
                           <td><span class="labal">Tags</span></td>
                           <td>:
                                 <span>
                                    <?php if(!empty($arr_tag)){ 
                                             foreach ($arr_tag as $key => $tag) { ?>
                                                <a href="<?=_URL?><?=$tag->tag_slug?>" class="tagListing"><?=$tag->tag_name?></a></span>
                                    <?php    }
                                          } else { ?>
                                             N/A
                                    <?php } ?>      
                                 </span>
                           </td>
                        </tr>
                     </table>
                     <div class="flexResponsive flexAlignItemsCenter">
                        <div class="favtBtn">

                           <?php if (!empty($_SESSION['userId'])) { ?>
                                 <?php if (in_array($obj_product->product_id, $arr_favtId)) { ?>
                                    <a href="javascript:void(0);" class="addfavourite pink" data-productid="<?=$obj_product->product_id?>"><i class="icon fa fa-heart-o"></i> &nbsp; Added to favourite</a>
                                 <?php } else { ?>
                                    <a href="javascript:void(0);" class="addfavourite" data-productid="<?=$obj_product->product_id?>"><i class="icon fa fa-heart-o normal"></i><i class="fa fa-heart hover" aria-hidden="true"></i> &nbsp; Add to favourite</a>
                                 <?php } ?>
                           <?php } else { ?>
                                    <a href="<?=_URL?>accounts/login"><i class="icon fa fa-heart-o normal"></i><i class="fa fa-heart hover" aria-hidden="true"></i> &nbsp; Add to favourite</a>
                           <?php } ?>
                           
                        </div>
                        <div class="flex shareBtn">

                           <div data-href="<?=($currenturl)?>" data-layout="icon" data-mobile-iframe="true">
                              <a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=($currenturl)?>&amp;src=sdkpreparse" title="Share on facebook"><i class="fa fa-facebook"></i></a>
                           </div>

                           <a href="https://twitter.com/share?
                              url=<?=($currenturl)?>&
                              via=RajDiamonds&
                              related=twitterapi%2Ctwitter&
                              hashtags=<?=ucfirst($nav[0])?>&
                              text=Currently reading"
                              target="_blank" title="Share on twitter"><i class="icon fa fa-twitter"></i></a>

                           <a href="http://pinterest.com/pin/create/bookmarklet/?media=<?=_IMAGES?>raj-diamonds-logo-light.svg&url=<?=($currenturl)?>&is_video=false&description=<?=ucwords(!empty($metatitle)?$metatitle:SHARE_TITLE)?>" target="_blank" title="Share on pinterest"><i class="icon fa fa-pinterest"></i></a>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="container">
      <div class="row">
         <div class="col-md-6">
            <div class="prodDesc">
               <div class="heading">Description</div>
               <div class="desc"><?=$obj_product->description?></div>
            </div>
         </div>
         <div class="col-lg-5 col-lg-offset-1 col-md-5">
            <section>
               <h2 class="ui-title-block-2">Enquire for pricing :</h2>
               <div class="forms">
                  <form id="frmenquiry" name="frmenquiry" action="<?=_URL?>contact" method="post" class="form-contacts ui-form">
                     <div class="row">
                        <div class="col-xs-12">
                           <input type="text" name="name" value="<?=(!empty($_SESSION['name'])?$_SESSION['name']:'')?>" placeholder="Your Name*" class="form-control">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6">
                           <input type="email" name="email" value="<?=(!empty($_SESSION['email'])?$_SESSION['email']:'')?>" placeholder="Email*" class="form-control">
                           <input type="text" name="product_code" value="<?=$obj_product->product_code?>" placeholder="Product Code" class="form-control">
                        </div>
                        <div class="col-md-6">
                           <input type="tel" name="phone" value="<?=(!empty($_SESSION['phone'])?$_SESSION['phone']:'')?>" placeholder="Phone" class="form-control">
                           <input type="text" name="subject" placeholder="Subject" class="form-control last-block_mrg-btn_0">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                           <textarea name="message" rows="3" placeholder="Enter your message*" class="form-control"></textarea>
                           <button type="submit" class="btn btn-primary btn-lg btn-effect">Send Message</button>
                        </div>
                     </div>
                  </form>
               </div>
            </section>
         </div>
      </div>
   </div>
<?php if(!empty($arr_relatedProduct)){ ?>
   <div class="container">
      <div class="relatedProduct">
         <div class="heading">Related Products</div>
         <div class="js-scroll-content">
            <div class="row">
            <?php foreach($arr_relatedProduct as $key => $relatedProduct){ ?>
                     <div class="col-md-3">
                        <section class="b-goods b-goods_mod-b">
                           <div class="b-goods__inner">
                              <a href="<?=_URL?><?=$relatedProduct->group_slug?>/<?=$relatedProduct->product_slug?>" class="b-goods__img">
                                 <img src="<?=_PRODUCT_ORG_URL?><?=$relatedProduct->product_thumb?>" alt="<?=$relatedProduct->product_name?>" class="img-responsive"/>
                              </a>
                              <a href="<?=_URL?><?=$relatedProduct->group_slug?>/<?=$relatedProduct->product_slug?>">
                                 <!-- <div class="b-goods__category"><?=$relatedProduct->group_name?></div> -->
                                 <h3 class="b-goods__name"><?=$relatedProduct->product_name?></h3>
                                 <div class="b-goods__price"><?=$relatedProduct->product_code?></div>
                              </a>
                           </div>
                        </section>
                     </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
   <?php } ?>
</div>
