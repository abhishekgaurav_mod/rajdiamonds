<div class="wrap-content">
   <div class="pageBanner desktop">
      <img src="<?=_CATEGORY_ORG_URL?>desktop-banner/<?=$obj_category->desktop_banner?>">
   </div>
   <div class="pageBanner tablet480">
      <img src="<?=_CATEGORY_ORG_URL?>tab-banner/<?=$obj_category->tab_banner?>">
   </div>
   <div class="pageBanner deviceMobile">
      <img src="<?=_CATEGORY_ORG_URL?>mobile-banner/<?=$obj_category->mobile_banner?>">
   </div>
   <!-- <div class="b-title-page b-title-page_w_bg">
      <div class="container">
         <div class="row">
            <div class="col-xs-12">
               <h1 class="b-title-page__title"><?=$obj_category->category_name?></h1>
               <div class="b-title-page__list color-primary"><?=$obj_category->category_description?></div>
            </div>
         </div>
      </div>
   </div> -->
        
   <div class="container">
      <?php include _PARTIAL_ROOT.'_breadcrumb.php'; ?>

      <div class="row">
         <div class="col-xs-12">
            <div class="b-goods-headers b-goods-headers_mod-a">
               <!-- <button type="button" data-toggle="collapse" data-target="#wrap-filter" class="b-filter__btn btn btn-default btn-effect"><span class="badge"></span><i class="icon fa fa-bars"></i> Filters</button> -->
               <div class="b-goods-headers__view"><i class="b-goods-headers__view-item active fa fa-th js-view-col"></i><i class="b-goods-headers__view-item fa fa-list js-view-list"></i></div>         
               <!-- <div id="wrap-filter" class="b-filter collapse">
                  <form class="form-filter">
                     <div class="row">
                        <div class="col-md-6">
                           <section class="b-filter__section">
                              <h3 class="b-filter__title">Categories</h3>
                              <select data-width="100%" class="selectpicker" onchange="location=this.options[this.selectedIndex].value;">
                                 <?php if (!empty($arr_category)) {
                                          foreach ($arr_category as $key => $category) { ?>
                                             <option value="<?=_URL?><?=$category->category_slug?>"><?=$category->category_name?></option>
                                 <?php    }
                                       } ?>
                              </select>
                           </section>
                        </div>
                        <div class="col-md-6">
                           <section class="b-filter__section">
                              <h3 class="b-filter__title">Jewellery Type</h3>
                              <select data-width="100%" class="selectpicker" onchange="location=this.options[this.selectedIndex].value;">
                              <?php if (!empty($arr_tag)) {
                                       foreach ($arr_tag as $key => $tag) { ?>
                                          <option value="<?=_URL?><?=$tag->tag_slug?>"><?=$tag->tag_name?></option>
                              <?php    }
                                    } ?>
                              </select>
                           </section>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                           <div class="b-goods-number-products pull-left">Showing 1 to 10 of 243 products</div>
                        </div>
                     </div>
                  </form>
               </div> -->

               <div class="l-main-content productListing">
                  <div class="b-goods-catalog">
                     <div id="errorMsg" ></div>
                     <?php if(!empty($arr_product)){
                              foreach ($arr_product as $key => $product){ ?>
                                 <section class="b-goods b-goods_mod-a b-goods_3-col">
                                    <div class="b-goods__inner">
                                       <a href="<?=_URL?><?=$obj_category->category_slug?>/<?=$product->product_slug?>" class="b-goods__img">
                                          <img src="<?=_PRODUCT_ORG_URL?><?=$product->product_thumb?>" alt="<?=$obj_category->category_name?>" class="img-responsive"/>
                                       </a>

                                       <div class="b-goods__wrap">
                                          <a href="<?=_URL?><?=$obj_category->category_slug?>/<?=$product->product_slug?>">
                                             <!-- <div class="b-goods__category"><?=$obj_category->category_name?></div> -->
                                             <h3 class="b-goods__name"><?=$product->product_name?></h3>
                                             <div class="b-goods__description"><?=((strlen($product->description)>200)?substr(strip_tags($product->description), 0,200).'. . .':strip_tags($product->description))?></div>
                                             <div class="b-goods__price"><?=$product->product_code?></div>
                                          </a>
                                          <div class="b-goods-links">
                                             <?php if (!empty($_SESSION['userId'])) { ?>
                                                <a href="javascript:void(0);" class="addfavourite <?=in_array($product->product_id, $arr_favtId)?'markedFavt':''?> b-goods-links__item" data-productid="<?=$product->product_id?>"><i class="icon fa fa-heart-o normal"></i><i class="fa fa-heart hover" aria-hidden="true"></i> </a>
                                            <?php } else { ?>
                                                <a href="<?=_URL?>accounts/login" class="b-goods-links__item"><i class="icon fa fa-heart-o normal"></i><i class="fa fa-heart hover" aria-hidden="true"></i> </a>
                                            <?php } ?>
                                          </div>
                                       </div>
                                    </div>
                                 </section>
                        <?php }
                           } ?>
                  </div>
                  <?php if ($totalPage > 1) { ?>
                        <div class="flex flexJustifyEnd flexAlignItemsCenter pagination">
                              <?php for($page = 1; $page <= $totalPage; $page++ ) { ?>
                                 <div class="<?=($p == $page)?'active':''?>"><a href="<?=_URL.$nav[0]?>?p=<?=$page?>" class="btn btn-primary btn-lg btn-effect"><?=$page?></a></div>
                              <?php } ?>
                        </div>
                  <?php } ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>