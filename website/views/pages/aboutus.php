<div class="b-title-page b-title-page_w_bg">
	<div class="container">
		<div class="row">
			<!-- <div class="col-xs-12">
				<h1 class="b-title-page__title shuffle"><?=$cmsContent->title?></h1>
				<div class="pageCaptionSubTitle"><?=$cmsContent->subtitle?></div>
			</div> -->
		</div>
	</div>
</div>

<div class="container">
<?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="cms pageContent textaligncenter">
				<?php
				if (!empty($cmsContent)) {
				$page_body = str_replace('{_IMAGES}', _IMAGES, $cmsContent->description);
				$page_body = str_replace('{_URL}', _URL, $page_body);
				$page_body = str_replace('{_MEDIA_URL}', _MEDIA_URL, $page_body);

				echo $page_body;
				}
				?>
			</div>
		</div>
	</div>
</div>