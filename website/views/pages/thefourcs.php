<div class="wrap-content">
   <div class="pageBanner desktop">
      <img src="<?=_IMAGES?>page-banner/desktop/the-4c.jpg">
   </div>
   <div class="pageBanner tablet480">
      <img src="<?=_IMAGES?>page-banner/tab/the-4c.jpg">
   </div>
   <div class="pageBanner deviceMobile">
      <img src="<?=_IMAGES?>page-banner/mobile/the-4c.jpg">
   </div>

	<div class="container">
	<?php  include _PARTIAL_ROOT.'_breadcrumb.php'; ?>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="cms pageContent">
					<?php
					if (!empty($cmsContent)) {
					$page_body = str_replace('{_IMAGES}', _IMAGES, $cmsContent->description);
					$page_body = str_replace('{_URL}', _URL, $page_body);
					$page_body = str_replace('{_MEDIA_URL}', _MEDIA_URL, $page_body);

					echo $page_body;
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>