<?php
class Category extends DBSource
{
    public function __construct()
    {   parent::__construct();  }

    public function __destruct()
    {   parent::__destruct();   }

    public function getAllCategories() {
        try {
            $sql = "SELECT * FROM categories
                    WHERE enabled = 'Y' ORDER BY category_name ASC";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getCategoryBySlug($category_slug) {
        try {
            $sql = "SELECT * FROM categories
                    WHERE category_slug = '".$this->mysqlEscapeString($category_slug)."' 
                    AND enabled = 'Y' LIMIT 1 ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                $row    =   $this->db_fetch_object($res);
                $this->db_free_results($res);
                return $row;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getFeaturedCategories() {
        try {
            $sql = "SELECT * FROM categories
                    WHERE enabled = 'Y' AND featured_status = 'Y' ORDER BY category_order ASC";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

}
?>
