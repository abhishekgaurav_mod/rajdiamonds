<?php 
class Cmscontent extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

    public function getCmscontentByType($type)
    {
    	try
    	{
    		$sql = "SELECT * FROM cms_contents
    				WHERE type = '".$this->mysqlEscapeString($type)."' 
    				AND enabled = 'Y' ";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
}
?>
