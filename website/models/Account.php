<?php 
class Account extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addUser($obj_user)
	{
		try
		{
			$sql = 'INSERT INTO users
					(	
            name
						, email
            , phone
						, password
						, salt_string
						, created_on
						, updated_on
					)
					VALUES
					(	
            "'.$this->mysqlEscapeString($obj_user->name).'"
            , "'.$this->mysqlEscapeString($obj_user->email).'"
						, "'.$this->mysqlEscapeString($obj_user->phone).'"
						, MD5("'.$this->mysqlEscapeString($obj_user->salt_string.$obj_user->password).'")
						, "'.$this->mysqlEscapeString($obj_user->salt_string).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

  public function addSocialMediaUser($obj_user)
  {
    try
    {
      $sql = 'INSERT INTO users
          ( 
            name
            , socialmedia_id
            , login_type
            , email
            , first_name
            , middle_name
            , last_name
            , gender
            , age_range
            , social_link
            , salt_string
            , created_on
            , updated_on
          )
          VALUES
          ( 
            "'.$this->mysqlEscapeString($obj_user->name).'"
            , "'.$this->mysqlEscapeString($obj_user->socialmedia_id).'"
            , "'.$this->mysqlEscapeString($obj_user->login_type).'"
            , "'.$this->mysqlEscapeString($obj_user->email).'"
            , "'.$this->mysqlEscapeString($obj_user->first_name).'"
            , "'.$this->mysqlEscapeString($obj_user->middle_name).'"
            , "'.$this->mysqlEscapeString($obj_user->last_name).'"
            , "'.$this->mysqlEscapeString($obj_user->gender).'"
            , "'.$this->mysqlEscapeString($obj_user->age_range).'"
            , "'.$this->mysqlEscapeString($obj_user->social_link).'"
            , "'.$this->mysqlEscapeString($obj_user->salt_string).'"
            , CURRENT_TIMESTAMP
            , CURRENT_TIMESTAMP);';
        $this->db_query($sql);
      return $this->mysqlInsertId();
      }
      catch(Exception $e)
      { throw $e; }
  }

	public function updateUser ($obj_user)
	{
		try
		{
   		$sql	="	UPDATE users SET
                name			  =	'".$this->mysqlEscapeString($obj_user->name)."' 
                , email			=	'".$this->mysqlEscapeString($obj_user->email)."'
                , phone     = '".$this->mysqlEscapeString($obj_user->phone)."'
                , address   = '".$this->mysqlEscapeString($obj_user->address)."'
                , updated_on	=	CURRENT_TIMESTAMP "; 
   			if (!empty($obj_user->password)) {
          $sql .= "	, salt_string	=	'".$this->mysqlEscapeString($obj_user->salt_string)."'
							, password	=	MD5('".$this->mysqlEscapeString($obj_user->salt_string.$obj_user->password)."') ";
        }		   					
   					
			$sql	.= " WHERE user_id	=	'".$this->mysqlEscapeString($obj_user->user_id)."'";
			return ($this->db_query($sql))?true:false;
		}
   		catch(Exception $e)
   		{	throw $e;	}
	}
  
  public function updateFavourite ($user_id, $favourite)
  {
      try {
          $sql    ="  UPDATE users SET
                      favourite       =   '".$this->mysqlEscapeString($favourite)."' 
                      WHERE user_id =   '".$this->mysqlEscapeString($user_id)."'";
          return ($this->db_query($sql))?true:false;
      }
      catch(Exception $e)
      {   throw $e;   }
  }
	
	
	public function authenticateUser ($useremail, $userpassword)
	{
		try
		{
   			$sql='	SELECT * FROM users  
   					WHERE email 	= \''.$this->mysqlEscapeString($useremail).'\' 
   					AND password	= MD5(\''.$this->mysqlEscapeString($userpassword).'\')';
   				
   			$res=$this->db_query($sql);
   			if($this->db_num_rows($res)==0)
   			{
   				$this->db_free_results($res);
				return 0;
   			}
			else
			{
				$row	=	$this->db_fetch_object($res);
				$this->db_free_results($res);
				return $row;
			}
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}
	
    public function getUserById($userId)
    {
    	try
    	{
    		$sql = "SELECT * FROM users
    				    WHERE user_id = '".$this->mysqlEscapeString($userId)."' ";
    	
    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0) {
    			$this->db_free_results($res);
    			return 0;
    		} else {
    			$row    =   $this->db_fetch_object($res);
    			$this->db_free_results($res);
    			return $row;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
  
    public function getUserBySocialMediaId($socialmedia_id,$login_type)
    {
      try
      {
        $sql = "SELECT * FROM users
                WHERE socialmedia_id = '".$this->mysqlEscapeString($socialmedia_id)."' ";
      
        $res=$this->db_query($sql);
        if($this->db_num_rows($res)==0) {
          $this->db_free_results($res);
          return 0;
        } else {
          $row    =   $this->db_fetch_object($res);
          $this->db_free_results($res);
          return $row;
        }
      }
      catch(Exception $e)
      { throw $e; }
    }
	
    public function changePassword($salt, $password, $userId)
	{
		try
		{
			$sql = 'UPDATE users 
					SET password = MD5(\''.$this->mysqlEscapeString($salt).$this->mysqlEscapeString($password).'\')
						, salt_string =	\''.$this->mysqlEscapeString($salt).'\'
						, updated_on		=	CURRENT_TIMESTAMP
					WHERE user_id = '.$userId.';';
   			return ($this->db_query($sql))?true:false;
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}
	
    public function getUserByEmail($email)
    {
        try
        {
            $sql='	SELECT 	user_id
            				, email
            				, salt_string
            				, name
                    , phone
            		FROM users WHERE email = "'.$this->mysqlEscapeString($email).'"';
                
            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0)
            {
                $this->db_free_results($res);
                return 0;
            }
            else
            {
                $row    =   $this->db_fetch_object($res);
                $this->db_free_results($res);
                return $row;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }
	
	public function checkUserEmail($email)
    {
        try
        {
            $sql="SELECT user_id, email FROM users WHERE email = '".$this->mysqlEscapeString($email)."'";
            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0)
            {
                $this->db_free_results($res);
                return false;
            }
            else
            {
                $this->db_free_results($res);
                return true;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }
}
?>
