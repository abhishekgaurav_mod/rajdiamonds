<?php 
class Newsletter extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addSubscriber ($object) {
		try {
			$sql = 'INSERT INTO subscribers
					(	
						email
						, created_on
                        , updated_on
					)
					VALUES
					(	
						"'.$this->mysqlEscapeString($object->email).'"
                        , CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

    public function unsubscribe ($id) {
        try {
            $sql    ="  UPDATE subscribers SET
                        subscribed      =   'N',
                        updated_on  	=   CURRENT_TIMESTAMP
                        WHERE subscriber_id  =   '".$this->mysqlEscapeString($id)."'";
            return ($this->db_query($sql))?true:false;
        }
        catch(Exception $e)
        {   throw $e;   }
    }
	
}
?>
