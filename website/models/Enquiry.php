<?php 
class Enquiry extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

	public function addEnquiry($obj_enquiry)
	{
		try
		{
			$sql = 'INSERT INTO enquiries
					(	
						name
						, phone
						, email
						, product_code
						, subject
						, message
						, created_on
						, updated_on
					)
					VALUES
					(	"'.$this->mysqlEscapeString($obj_enquiry->name).'"
						, "'.$this->mysqlEscapeString($obj_enquiry->phone).'"
						, "'.$this->mysqlEscapeString($obj_enquiry->email).'"
						, "'.$this->mysqlEscapeString($obj_enquiry->product_code).'"
						, "'.$this->mysqlEscapeString($obj_enquiry->subject).'"
						, "'.$this->mysqlEscapeString($obj_enquiry->message).'"
						, CURRENT_TIMESTAMP
						, CURRENT_TIMESTAMP);';
   			$this->db_query($sql);
			return $this->mysqlInsertId();
   		}
   		catch(Exception $e)
   		{	throw $e;	}
	}

}
?>
