<?php 
class Slider extends DBSource 
{
	public function __construct()
	{	parent::__construct();	}

    public function __destruct()
	{	parent::__destruct();	}

    public function getSliders()
    {
    	try
    	{
    		$sql = "SELECT * FROM sliders WHERE enabled = 'Y' ORDER BY slider_id ASC ";

    		$res=$this->db_query($sql);
    		if($this->db_num_rows($res)==0)
    		{
    			$this->db_free_results($res);
    			return 0;
    		}
    		else
    		{
    			for($i=0;$row=$this->db_fetch_object($res);$i++)
				{	$arr[$i] = $row; 	}
				$this->db_free_results($res);
				return $arr;
    		}
    	}
    	catch(Exception $e)
    	{	throw $e;	}
    }
	
}
?>
