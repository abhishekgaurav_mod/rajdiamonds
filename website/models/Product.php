<?php
class Product extends DBSource {
    public function __construct()
    {   parent::__construct();  }

    public function __destruct()
    {   parent::__destruct();   }

    public function getProductsByProductSlug($product_slug)
    {
        try {
            $sql = "SELECT  p.* 
                            , c.category_name
                            , c.category_slug
                            FROM products AS p
                            LEFT JOIN categories AS c ON c.category_id = p.category_id
                            WHERE p.product_slug  = '".$this->mysqlEscapeString($product_slug)."' 
                            AND p.enabled = 'Y' AND c.enabled = 'Y' LIMIT 1 ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                $row    =   $this->db_fetch_object($res);
                $this->db_free_results($res);
                return $row;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getProductsByProductId($product_id)
    {
        try {
            $sql = "SELECT  p.* 
                            , c.category_name
                            , c.category_slug
                            FROM products AS p
                            LEFT JOIN categories AS c ON c.category_id = p.category_id
                            WHERE p.product_id  = '".$this->mysqlEscapeString($product_id)."' 
                            AND p.enabled = 'Y' AND c.enabled = 'Y' LIMIT 1 ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                $row    =   $this->db_fetch_object($res);
                $this->db_free_results($res);
                return $row;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getProductsByCategoryId($category_id) {
        try {
            $sql = "SELECT * FROM products
                    WHERE enabled = 'Y' AND category_id = '".$this->mysqlEscapeString($category_id)."' ORDER BY product_order ASC";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getLimitProductsByCategoryId($category_id, $lLimit, $uLimit) {
        try {
            $sql = "SELECT * FROM products
                    WHERE enabled = 'Y' AND category_id = '".$this->mysqlEscapeString($category_id)."' ORDER BY product_order ASC  LIMIT ".$lLimit.",".$uLimit." ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getRelatedProductsByTagSlug($tag_slug) {
        try {
            $sql = "SELECT p.*, t.tag_name AS group_name, t.tag_slug AS group_slug
                    FROM products AS p
                    INNER JOIN product_tag_mapping AS ptm ON ptm.product_id = p.product_id 
                    INNER JOIN tags AS t ON t.tag_id = ptm.tag_id 
                    WHERE t.enabled = 'Y' AND p.enabled = 'Y' AND t.tag_slug = '".$this->mysqlEscapeString($tag_slug)."' ORDER BY RAND() LIMIT 4 ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getRelatedProductsByCategorySlug($category_slug) {
        try {
            $sql = "SELECT p.*, c.category_name AS group_name, c.category_slug AS group_slug
                    FROM products AS p
                    INNER JOIN categories AS c ON c.category_id = p.category_id 
                    WHERE c.enabled = 'Y' AND p.enabled = 'Y' AND c.category_slug = '".$this->mysqlEscapeString($category_slug)."' ORDER BY RAND() LIMIT 4 ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getHomeProductsByCategoryId($category_id) {
        try {
            $sql = "SELECT * FROM products
                    WHERE enabled = 'Y' AND front_visible = 'Y' AND category_id = '".$this->mysqlEscapeString($category_id)."' ORDER BY RAND() LIMIT 6";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getProductsByTagId($tag_id) {
        try {
            $sql = "SELECT  p.*
                    FROM products AS p
                    LEFT JOIN product_tag_mapping AS ptm ON ptm.product_id = p.product_id
                    WHERE p.enabled = 'Y' AND ptm.tag_id  = '".$this->mysqlEscapeString($tag_id)."' ORDER BY p.product_order ASC ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }    

    public function getLimitProductsByTagId($tag_id, $lLimit, $uLimit) {
        try {
            $sql = "SELECT  p.*
                    FROM products AS p
                    LEFT JOIN product_tag_mapping AS ptm ON ptm.product_id = p.product_id
                    WHERE p.enabled = 'Y' AND ptm.tag_id  = '".$this->mysqlEscapeString($tag_id)."' ORDER BY p.product_order ASC LIMIT ".$lLimit.",".$uLimit." ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

}
?>
