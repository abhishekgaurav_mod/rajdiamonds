<?php
class Tag extends DBSource
{
    public function __construct()
    {   parent::__construct();  }

    public function __destruct()
    {   parent::__destruct();   }

    public function getAllTags() {
        try {
            $sql = "SELECT * FROM tags
                    WHERE enabled = 'Y' ORDER BY tag_name ASC";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i] = $row;    }
                $this->db_free_results($res);
                return $arr;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getTagBySlug($tag_slug) {
        try {
            $sql = "SELECT * FROM tags
                    WHERE tag_slug = '".$this->mysqlEscapeString($tag_slug)."' 
                    AND enabled = 'Y' LIMIT 1 ";

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                $row    =   $this->db_fetch_object($res);
                $this->db_free_results($res);
                return $row;
            }
        }
        catch(Exception $e)
        {   throw $e;   }
    }

    public function getTagsByProductId($product_id) {
        try {
            $sql = "SELECT t.tag_name, t.tag_slug 
                    FROM product_tag_mapping AS ptm
                    INNER JOIN tags AS t ON ptm.tag_id = t.tag_id
                    WHERE t.enabled = 'Y' AND ptm.product_id = ".$this->mysqlEscapeString($product_id);

            $res=$this->db_query($sql);
            if($this->db_num_rows($res)==0) {
                $this->db_free_results($res);
                return 0;
            } else {
                $arr    =   array();
                for($i=0;$row=$this->db_fetch_object($res);$i++)
                {   $arr[$i]    =   $row;   }
                $this->db_free_results($res);
                return $arr;
            }
        }catch (Exception $e){ $this->db_error($e); }
    }

}
?>
