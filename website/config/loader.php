<?php
	require 'config.php';
	require 'settings.php';
	setlocale(LC_ALL, 'en_US.UTF8');
	
	date_default_timezone_set(_TIME_ZONE);
	error_reporting(E_ALL & ~E_STRICT);
	session_start();
	
	function __autoload($_class)
	{
		$str_class          =   _CONTROLLERS_ROOT.$_class . '.php';
		$str_model_class    =   _MODEL_ROOT.$_class . '.php';
		$str_lib_class    	=   _LIBS_ROOT.$_class . '.php';
		
		if (file_exists($str_class))
		{	$str_class  =   $str_class;	}
		if (file_exists($str_model_class))
		{	$str_class  =   $str_model_class;	}
		if (file_exists($str_lib_class))
		{	$str_class  =   $str_lib_class;	}
		if(is_file($str_class))
		{	include_once($str_class);	}
	}
?>