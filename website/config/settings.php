<?php
	$functional	=	array(	'_TITLE' => 'Raj Diamonds',
							'_URL_SLUG_LENGTH'=>100);

	$smtpsetting=	array(	'_SMTP_SERVER' => 'smtp.gmail.com',
							'_EMAIL_LOGIN' => 'connect@motherofdesign.in',
							'_EMAIL_PASSWORD' => 'MOD@2017',
							'_EMAIL_FROM_NAME' => 'Raj Diamonds',
							'_FROM_EMAIL' => 'connect@motherofdesign.in',
							'_EMAIL_REPLY_TO' => 'connect@motherofdesign.in',
							'_EMAIL_SMTPSECURE' => 'ssl', // ssl/tls/nothing
							'_EMAIL_PORT' => '465');

	setdefined(array_merge($database, $userdefind, $functional, $smtpsetting));

	// ROOT  Settings
	define('_CONTROLLERS_ROOT', _ROOT.'controllers/');
	define('_LIBS_ROOT', _ROOT.'libs/');
	define('_MODEL_ROOT', _ROOT.'models/');
	define('_VIEWS_ROOT', _ROOT."views/");
	define('_PARTIAL_ROOT',_VIEWS_ROOT."partial/");
	define('_STATIC_BLOCK',_PARTIAL_ROOT."staticblock/");
	define('_WEBROOT_ROOT',_ROOT."webroot/");
	define('_EMAIL_TEMPLATE',_VIEWS_ROOT."email_templates/");

	// URL Settings
	define('_WEBROOT',_URL."webroot/");
	define('_IMAGES',_WEBROOT."images/");
	define("_CSS",_WEBROOT."css/");
	define("_JS",_WEBROOT."js/");
	define("_DOCS",_WEBROOT."docs/");
	define("_PLUGINS",_WEBROOT."plugins/");

	define('_MEDIA_ROOT', _WEBROOT_ROOT.'media/');
	define('_MEDIA_URL', _URL.'webroot/media/');


	// GALLERY-IMAGES
    define('_GALLERY_ORG_URL',_MEDIA_URL.'galleries/');
    define('_GALLERY_THUMBS_ORG_URL',_MEDIA_URL.'galleries/thumb/');

    // SLIDERS-IMAGES
	define('_SLIDER_ORG_URL',_MEDIA_URL.'sliders/');

	// PRODUCT-IMAGE
    define('_PRODUCT_ORG_URL',_MEDIA_URL.'products/');

	// CATEGORY-IMAGE
    define('_CATEGORY_ORG_URL',_MEDIA_URL.'categories/');

	// TAG-IMAGE
    define('_TAG_ORG_URL',_MEDIA_URL.'tags/');

    

	function setdefined($defines){
		if (!empty($defines))
		{
			foreach ($defines as $key=>$val)
			{	define($key, $val);	}
		}
	}
?>
