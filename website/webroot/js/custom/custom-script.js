$(document).ready(function(){

	$('.homeBannerSlider').bxSlider({
      mode: 'fade',
      controls: false,
      pager: true,
      auto: true,
      speed: 1000,

    });

	if ($('#frmlogin').length>0)
	{ formsubmit('frmlogin','.forms','bottom left');	}
   if ($('#frmregister').length>0)
   { formsubmit('frmregister','.forms','bottom left');   }
   if ($('#frmprofile').length>0)
   { formsubmit('frmprofile','.forms','bottom left');   }
   if ($('#frmenquiry').length>0)
   { formsubmit('frmenquiry','.forms','bottom left'); }
   if ($('#frmnewsletter').length>0)
   { formsubmit('frmnewsletter','.forms','bottom left'); }

	$('a[rel*=leanModal]').leanModal({ top : '200px', closeButton: ".popupClose" });
});

/*STICK-HEADER-TO-TOP*/
if (_CURRENT_URL == _URL) {
   $(document).ready(function() {
         var loadPos = $('.desktopHeader .header').offset().top;
         if (loadPos > 200) {
            $('.desktopHeader .header').addClass('opaqueHeader');
         } else {
            $('.desktopHeader .header').removeClass('opaqueHeader');
         }
      $(document).scroll(function () {
         var y = $(this).scrollTop();
         if (y > 200) {
            $('.desktopHeader .header').addClass('opaqueHeader');
         } else {
            $('.desktopHeader .header').removeClass('opaqueHeader');
         }
      });
   });
}

$(document).on('click','.addfavourite',function(){
   var thisObj = $(this);
   var product_id = thisObj.data('productid');
   var params     = 'product_id='+product_id;
   $.ajax({
      url: _URL+'accounts/addfavourite',
      data: params,
      type: 'POST',
      dataType: 'json',
      success: function(data) {
         if(data.status){
            thisObj.addClass('markedFavt');
            $('#errorMsg').notify(data.message, {className:'success', position:'top left'});
            return false;
            //window.location.href=_URL+'accounts/favourite';
         }
         else{
            $('#errorMsg').notify(data.message, {className:'error', position:'top left'});
            return false;
         }
      },
      error: function() { alert('Error while exporting records.');  },
      complete: function() {    }
   });
});

/*STICK-HEADER-TO-TOP*/

var formsubmit  =   function(id,relative,pos){
   $('#'+id+' button[type="submit"]').removeAttr('disabled','disabled');
   $('#'+id).ajaxForm({
      forceSync: true,
      dataType: 'json',
      beforeSerialize:function($Form, options){
         //return CKupdate();
      },
      beforeSubmit: function(){
         $('#'+id+' .loading').show();
         $('#'+id+' button[type="submit"]').data('value', $('#'+id+' button[type="submit"]').html());
         $('#'+id+' button[type="submit"]').html(((typeof $('#'+id+' button[type="submit"]').data('loadtext') != 'undefined')?$('#'+id+' button[type="submit"]').data('loadtext'):'Loading')+'...');
         $('#'+id+' button[type="submit"]').attr('disabled','disabled');

      },
      success: function(responseText, statusText, xhr, $form){
      var responseText  =   jQuery.parseJSON(xhr.responseText);
      if(responseText.redirect)
      { setTimeout(function(){ window.location.href = responseText.url; }, 1500);  }
      if (responseText.refresh)
      {
        location.reload();
      }

      if(responseText.prompt)
      {
         var error = (responseText.status)?'success':'error';
         $('#'+id).notify(responseText.message, {className:error, position:pos});
         if(error == 'success'){
            setTimeout(function() {   //calls click event after a certain time
            //$("#lean_overlay").trigger("click");
            }, 2000);
         }

      }
      },
      complete: function(){
         $('#'+id+' button[type="submit"]').html($('#'+id+' button[type="submit"]').data('value'));
         $('#'+id+' button[type="submit"]').removeAttr('disabled','disabled');
         $('#'+id+' .loading').hide();
      }
  });
};